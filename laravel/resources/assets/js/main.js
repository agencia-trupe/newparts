import AjaxSetup from "./AjaxSetup";
import MobileToggle from "./MobileToggle";
import Produtos from "./Produtos";
import Orcamentos from "./Orcamentos";

AjaxSetup();
MobileToggle();
Produtos();
Orcamentos();

$(document).ready(function () {
  // HOME - BANNERS
  $(".banners").cycle({
    slides: ".banner",
    fx: "fade",
    speed: 800,
    pager: ".cycle-pager",
  });

  var urlPrevia = "";
  // var urlPrevia = "/previa-newparts";

  // HOME - AJAX PARA EXIBIR produtos ALEATÓRIAMENTE
  $.ajax({
    type: "GET",
    url: urlHomeProdutos,
    success: function (data, textStatus, jqXHR) {
      fillProdutos(data.produtos.slice(0, 8));

      var intervalId = setInterval(function () {
        $("#produtosVitrineHome").fadeOut(1000, "swing", function () {
          randomProdutos(data.produtos, 8);
        });
      }, 10000);
    },
    error: function (jqXHR, textStatus, errorThrown) {
      console.log(jqXHR, textStatus, errorThrown);
    },
  });

  var fillProdutos = function (produtos) {
    $("#produtosVitrineHome").html("").css({ display: "none", opacity: 0 });

    produtos.forEach((element) => {
      var html =
        "<a href='" +
        window.location.origin +
        urlPrevia +
        "/produtos/" + element.linha_slug + "/produto/" +
        element.slug +
        "' class='produto' title='" +
        element.titulo +
        "'><div class='imagens'><img src='" +
        window.location.origin +
        urlPrevia +
        "/assets/img/produtos/" +
        element.capa +
        "' class='img-capa' alt='"+element.titulo+"'><img src='" +
        window.location.origin +
        urlPrevia +
        "/assets/img/layout/marca-newparts-cinza.svg" +
        "' alt='' class='img-logo'></div><p class='cod-titulo'>" +
        element.codigo +
        " - " +
        element.titulo +
        "</p></a>";
      $("#produtosVitrineHome").append(html);
    });

    $("#produtosVitrineHome").show().animate({ opacity: 1 }, 1600, "swing");
  };

  var randomProdutos = function (arr, n) {
    var result = new Array(n);
    var len = arr.length;
    var taken = new Array(len);

    if (n > len)
      throw new RangeError("getRandom: more elements taken than available");

    while (n--) {
      var x = Math.floor(Math.random() * len);
      result[n] = arr[x in taken ? taken[x] : x];
      taken[x] = --len in taken ? taken[len] : len;
    }

    fillProdutos(result);
  };

  // HOME - AJAX PARA EXIBIR marcas ALEATÓRIAMENTE
  $.ajax({
    type: "GET",
    url: urlHomeMarcas,
    success: function (data, textStatus, jqXHR) {
      fillMarcas(data.marcas.slice(0, 6));

      var intervalId = setInterval(function () {
        $("#marcasHome").fadeOut(1000, "swing", function () {
          randomMarcas(data.marcas, 6);
        });
      }, 10000);
    },
    error: function (jqXHR, textStatus, errorThrown) {
      console.log(jqXHR, textStatus, errorThrown);
    },
  });

  var fillMarcas = function (marcas) {
    $("#marcasHome").html("").css({ display: "none", opacity: 0 });

    marcas.forEach((element) => {
      var html =
        "<a href='" +
        window.location.origin +
        urlPrevia +
        "/produtos/elevadores/marcas/" +
        element.slug +
        "' class='marca' title='" +
        element.nome +
        "'><img src='" +
        window.location.origin +
        urlPrevia +
        "/assets/img/marcas/" +
        element.imagem +
        "' class='img-marca' alt='"+element.nome+"'></a>";
      $("#marcasHome").append(html);
    });

    $("#marcasHome").show().animate({ opacity: 1 }, 1600, "swing");
  };

  var randomMarcas = function (arr, n) {
    var result = new Array(n);
    var len = arr.length;
    var taken = new Array(len);

    if (n > len)
      throw new RangeError("getRandom: more elements taken than available");

    while (n--) {
      var x = Math.floor(Math.random() * len);
      result[n] = arr[x in taken ? taken[x] : x];
      taken[x] = --len in taken ? taken[len] : len;
    }

    fillMarcas(result);
  };

  // LOCALSTORAGE - actives links
  $(
    ".submenu-produtos .link-categoria, .produtos .link-categoria, .produtos-categorias .categoria, .submenu-nav .categoria, .footer .sublink"
  ).click(function (e) {
    e.preventDefault();
    localStorage.clear();
    localStorage.setItem("categoriaId", $(this).attr("id"));
    window.location.href = $(this).attr("href");
  });

  // limpando localStorage
  $(".produtos .busca-rapida .btn-busca, select.select-marcas").click(
    function () {
      localStorage.clear();
    }
  );

  var categoriaId = localStorage.getItem("categoriaId");
  var letraId = localStorage.getItem("letraId");

  if (categoriaId) {
    $(".produtos .link-categoria#" + categoriaId).addClass("active");
    $(".produtos .lista-categorias .categoria#" + categoriaId).addClass("active");
  }

  if (letraId) {
    $(".submenu-nav .letra#" + letraId).addClass("active");
  }

  $(".submenu-nav .letra").click(function (e) {
    e.preventDefault();
    localStorage.clear();
    localStorage.setItem("letraId", $(this).attr("id"));
    window.location.href = $(this).attr("href");
  });

  $("nav .link-nav, .home .todos, .footer .sublink.todos").click(function (e) {
    e.preventDefault();
    localStorage.clear();
    window.location.href = $(this).attr("href");
  });

  // FANCYBOX EMPRESA-IMAGENS
  $(".fancybox").fancybox({
    padding: 0,
    prevEffect: "fade",
    nextEffect: "fade",
    closeBtn: false,
    openEffect: "elastic",
    openSpeed: "150",
    closeEffect: "elastic",
    closeSpeed: "150",
    helpers: {
      title: {
        type: "outside",
        position: "top",
      },
      overlay: {
        css: {
          background: "rgba(132, 134, 136, .88)",
        },
      },
    },
    fitToView: false,
    autoSize: false,
    beforeShow: function () {
      this.maxWidth = "90%";
      this.maxHeight = "90%";
    },
  });

  $(".empresa-imagem").click(function handler(e) {
    e.preventDefault();

    const id = $(this).data("empresa-id");

    if (id) {
      $(`a[rel=empresa-${id}]`)[0].click();
    }
  });

  // BTN VER MAIS - NOVIDADES
  var itensNovidades = $(".todas-novidades .novidade");
  var spliceItensNovidades = 6;
  if (itensNovidades.length <= spliceItensNovidades) {
    $(".btn-mais-novidades").hide();
  }

  var setDivNovidades = function (itens, spliceItensNovidades) {
    var spliceItens = itens.splice(0, spliceItensNovidades);
    $(".todas-novidades").append(spliceItens);
    $(spliceItens).show();

    itens.length <= 0
      ? $(".btn-mais-novidades").hide()
      : $(".btn-mais-novidades").show();

    $(".todas-novidades").show();
  };

  $(".btn-mais-novidades").click(function () {
    setDivNovidades(itensNovidades, spliceItensNovidades);
  });

  $(".todas-novidades .novidade").hide();
  setDivNovidades(itensNovidades, spliceItensNovidades);

  // BUSCA
  $(".link-busca").click(function (e) {
    e.preventDefault();

    if ($(window).width() <= 1250) {
      $("header nav").css("display", "none");
    }

    $(".busca-avancada").show();
    e.stopPropagation();
  });

  // BUSCA AVANÇADA - CLICK LETRA PARA EXIBIR LINHAS DA LETRA
  $(".busca-avancada .letra").click(function (e) {
    e.preventDefault();
    var letra = $(this).html();

    $.ajax({
      type: "GET",
      url: $(this).attr("href"),
      success: function (data, textStatus, jqXHR) {
        $(".categorias-por-letra").html("").show();
        console.log(data);

        data.categorias.forEach((categoria) => {
          var htmlLinkCategoria =
            "<a href='" +
            window.location.origin +
            urlPrevia +
            "/produtos/" + data.linha.slug + "/" + categoria.slug + "' class='categoria-sub' id='categoria" + categoria.id +
            "'>" + categoria.titulo + "</a>";
          $(".categorias-por-letra#letra-" + letra).append(htmlLinkCategoria);
        });

        var alturaDiv = $(".categorias-por-letra#letra-" + letra).height();

        if ($(window).width() <= 800) {
          $(".busca-avancada").css("height", "100%");
        } else {
          $(".busca-avancada").height(alturaDiv + 150);
        }

        $(".categorias-por-letra .categoria-sub").eq(0).css("margin-top", "10px");

        $(".busca-avancada .categoria, .busca-avancada .categoria-sub").click(function (
          e
        ) {
          e.preventDefault();
          localStorage.removeItem("letraId");
          localStorage.setItem("categoriaId", $(this).attr("id"));
          window.location.href = $(this).attr("href");
        });
      },
      error: function (jqXHR, textStatus, errorThrown) {
        console.log(jqXHR, textStatus, errorThrown);
      },
    });
  });

  // FECHAR POP-UP BUSCA AVANÇADA
  $(
    ".busca-avancada .categoria, .busca-avancada .categoria-sub, .busca-avancada .img-fechar"
  ).click(function (e) {
    $(".busca-avancada").hide();
    e.stopPropagation();
  });

  // MARCAS - removendo margin-right dos 4º elementos
  $("section.marcas .marca:nth-child(4n)").css("margin-right", "0");

  // PRODUTOS - LINHAS (proxima, anterior) + btn topo
  var categoriaAnterior = $(".produtos .lista-categorias .categoria.active").prev();
  var categoriaProximo = $(".produtos .lista-categorias .categoria.active").next();

  $(".produtos .btn-anterior .nome-categoria").html(categoriaAnterior.html());
  $(".produtos .btn-proximo .nome-categoria").html(categoriaProximo.html());

  var marcaSlugSelecionada = $("select.select-marcas option:selected").val();
  var marcaSlugFiltrada = $(".marca-filtrada .link-marca").attr("slug");

  // btn categoria anterior
  $(".produtos .btn-anterior").click(function (e) {
    e.preventDefault();
    localStorage.clear();
    localStorage.setItem("categoriaId", categoriaAnterior.attr("id"));

    var linha = $(this).attr('linha');

    if (marcaSlugSelecionada == "null" || marcaSlugSelecionada == "todos") {
      if (marcaSlugFiltrada != null) {
        window.location.href =
          window.location.origin +
          urlPrevia +
          "/produtos/" + linha + "/" + categoriaAnterior.attr("slug") + "/" + marcaSlugFiltrada;
      } else {
        window.location.href =
          window.location.origin +
          urlPrevia +
          "/produtos/"+linha+"/" +
          categoriaAnterior.attr("slug");
      }
    } else if (marcaSlugSelecionada == "todos") {
      window.location.href =
        window.location.origin +
        urlPrevia +
        "/produtos/"+linha+"/" +
        categoriaAnterior.attr("slug");
    } else {
      window.location.href =
        window.location.origin +
        urlPrevia +
        "/produtos/"+linha+"/" + categoriaAnterior.attr("slug") + "/" + marcaSlugSelecionada;
    }
  });

  // btn categoria proxima
  $(".produtos .btn-proximo").click(function (e) {
    e.preventDefault();
    localStorage.clear();
    localStorage.setItem("categoriaId", categoriaProximo.attr("id"));

    var linha = $(this).attr('linha');

    if (marcaSlugSelecionada == "null") {
      if (marcaSlugFiltrada != null) {
        window.location.href =
          window.location.origin +
          urlPrevia +
          "/produtos/"+linha+"/" + categoriaProximo.attr("slug") + "/" + marcaSlugFiltrada;
      } else {
        window.location.href =
          window.location.origin +
          urlPrevia +
          "/produtos/"+linha+"/" +
          categoriaProximo.attr("slug");
      }
    } else if (marcaSlugSelecionada == "todos") {
      window.location.href =
        window.location.origin +
        urlPrevia +
        "/produtos/"+linha+"/" +
        categoriaProximo.attr("slug");
    } else {
      window.location.href =
        window.location.origin +
        urlPrevia +
        "/produtos/"+linha+"/" + categoriaProximo.attr("slug") + "/" + marcaSlugSelecionada;
    }
  });

  // btn topo
  $(".produtos .btn-topo").click(function (e) {
    e.preventDefault();
    $("html, body").animate({ scrollTop: $("body").offset().top }, 1000);
  });

  // AVISO DE COOKIES
  $(".aviso-cookies").hide();

  if (window.location.href == routeHome) {
    $(".aviso-cookies").show();

    $(".aceitar-cookies").click(function () {
      var url = window.location.origin + "/aceite-de-cookies";

      $.ajax({
        type: "POST",
        url: url,
        success: function (data, textStatus, jqXHR) {
          $(".aviso-cookies").hide();
        },
        error: function (jqXHR, textStatus, errorThrown) {
          console.log(jqXHR, textStatus, errorThrown);
        },
      });
    });
  }

  // máscara telefone
  $(".input-telefone").mask("(00) 000000000");
  $(".input-cnpj").mask("00.000.000/0000-00");
});
