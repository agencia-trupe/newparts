export default function Produtos() {
  $(document).ready(function () {
    var urlPrevia = "";
    // var urlPrevia = "/previa-newparts";

    var linha = $(".submenu-nav .linha.active").attr('id');
    if(linha == undefined) {
      linha = 'elevadores';
    }

    var urlProdutosGet = window.location.origin + urlPrevia + "/produtos/" + linha + "/getProdutos";

    // PRODUTOS - AJAX PARA EXIBIR produtos ALEATÓRIAMENTE
    $.ajax({
      type: "GET",
      url: urlProdutosGet,
      success: function (data, textStatus, jqXHR) {
        console.log(data);
        fillProdutos(data.produtos.slice(0, 15));

        var intervalId = setInterval(function () {
          $(".resultados-produtos-ajax").fadeOut(1000, "swing", function () {
            randomProdutos(data.produtos, 15);
          });
        }, 10000);
      },
      error: function (jqXHR, textStatus, errorThrown) {
        console.log(jqXHR, textStatus, errorThrown);
      },
    });

    var fillProdutos = function (produtos) {
      $(".resultados-produtos-ajax")
        .html("")
        .css({ display: "none", opacity: 0 });

      produtos.forEach((element) => {
        var html =
          "<a href='" +
          window.location.origin +
          urlPrevia +
          "/produtos/" + element.linha_slug + "/produto/" +
          element.slug +
          "' class='link-produto'><div class='imagens'><img src='" +
          window.location.origin +
          urlPrevia +
          "/assets/img/produtos/" +
          element.capa +
          "' class='img-produto' alt='" +
          element.titulo +
          "'><img src='" +
          window.location.origin +
          urlPrevia +
          "/assets/img/layout/marca-newparts-cinza.svg" +
          "' alt='' class='img-logo'></div><div class='marca'><img src='" +
          window.location.origin +
          urlPrevia +
          "/assets/img/marcas/" +
          element.marca_imagem +
          "' class='img-marca' alt='"+element.marca_nome+"'></div><p class='codigo'>[ " +
          element.codigo +
          " ]</p><p class='titulo'>" +
          element.titulo +
          "</p></a>";

        $(".resultados-produtos-ajax").append(html);
      });

      $(".resultados-produtos-ajax")
        .show()
        .animate({ opacity: 1 }, 1600, "swing");
    };

    var randomProdutos = function (arr, n) {
      var result = new Array(n);
      var len = arr.length;
      var taken = new Array(len);

      if (n > len)
        throw new RangeError("getRandom: more elements taken than available");

      while (n--) {
        var x = Math.floor(Math.random() * len);
        result[n] = arr[x in taken ? taken[x] : x];
        taken[x] = --len in taken ? taken[len] : len;
      }

      fillProdutos(result);
    };

    // SUBMENU LINHAS DE PRODUTOS
    if (window.location.href.indexOf("produtos") == -1) {
      $(".submenu-nav").css("display", "none");
      $(".complemento-menu").css("display", "none");
    }

    var categoriaAtiva = $(".categoria-filtrada .link-categoria").attr("slug");
    if (categoriaAtiva) {
      localStorage.clear();
      localStorage.setItem(
        "categoriaId",
        $(".categoria-filtrada .link-categoria").attr("id")
      );
    }
    // PRODUTOS - SELECT MARCAS
    $("body").on("change", "select.select-marcas", function (event) {
      event.preventDefault();
      event.stopPropagation();
      var marcaSlugSelecionada = $(
        "select.select-marcas option:selected"
      ).val();
      var linha = $("select.select-marcas option:selected").attr('linha');
      $(".busca-rapida input#marcaSlug").val(marcaSlugSelecionada);

      if (categoriaAtiva) {
        if (marcaSlugSelecionada == "todos" || marcaSlugSelecionada == "null") {
          window.location.href =
            window.location.origin +
            urlPrevia +
            "/produtos/"+linha+"/" +
            categoriaAtiva;
        } else {
          window.location.href =
            window.location.origin +
            urlPrevia +
            "/produtos/"+linha+"/" + categoriaAtiva + "/" + marcaSlugSelecionada;
        }
      }
    });

    // PRODUTOS FILTRO CATEGORIA - verificando se existe marca selecionada
    $("body").on("click", ".lista-categorias .categoria", function (e) {
      e.preventDefault();
      e.stopPropagation();
      localStorage.clear();
      localStorage.setItem("categoriaId", $(this).attr("id"));

      var marcaSlugSelecionada = $(
        "select.select-marcas option:selected"
      ).val();
      var marcaSlugFiltrada = $(".marca-filtrada .link-marca").attr("slug");
      var categoriaSlugClicada = $(this).attr("slug");
      var linhaSlug = $(this).attr("linha");
      console.log(linhaSlug);

      if (marcaSlugSelecionada == "null") {
        if (marcaSlugFiltrada != null) {
          window.location.href =
            window.location.origin +
            urlPrevia +
            "/produtos/" + linhaSlug + "/" + categoriaSlugClicada + "/" + marcaSlugFiltrada;
        } else {
          window.location.href = $(this).attr("href");
        }
      } else if (marcaSlugSelecionada == "todos") {
        window.location.href = $(this).attr("href");
      } else {
        window.location.href =
          window.location.origin +
          urlPrevia +
          "/produtos/" + linhaSlug + "/" +
          categoriaSlugClicada +
          "/" +
          marcaSlugSelecionada;
      }
    });

    // LISTA DE PRODUTOS - REMOVE MARGIN-RIGHT DOS 3ºS ELEMENTOS
    $(
      ".produtos .link-produto:nth-child(3n), .busca-resultado .link-produto:nth-child(3n)"
    ).css("margin-right", "0");

    // ampliando variação / class active
    $("body").on("click", ".variacoes-produtos a.variacao", function (event) {
      event.preventDefault();
      $(".variacoes-produtos a.variacao.active").removeClass("active");
      $(this).addClass("active");
    });

    // VARIAÇÕES
    $(".variacoes-produtos .variacao:nth-child(4n)").css("margin-right", "0");

    $(".variacoes-produtos .variacao").click(function (e) {
      e.preventDefault();
      $(".variacoes-produtos .variacao.active").removeClass("active");
      $(this).addClass("active");
      var src = $(this).attr("href");
      $(".capa-produto .img-produto").attr("src", src);
    });
  });
}
