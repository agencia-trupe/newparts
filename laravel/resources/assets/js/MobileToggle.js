export default function MobileToggle() {
  var $handle = $("#mobile-toggle"),
    $nav = $("header nav");

  $handle.on("click touchstart", function (event) {
    event.preventDefault();
    $nav.slideToggle();
    $handle.toggleClass("close");

    if ($("#mobile-toggle").hasClass("close")) {
      $(".pop-up-orcamento").css("visibility", "hidden");
    } else {
      $(".pop-up-orcamento").css("visibility", "visible");
    }
  });
}
