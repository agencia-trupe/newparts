export default function Orcammentos() {
  var urlPrevia = "";
  // var urlPrevia = "/previa-newparts";

  $("body").on("click", "a.link-add-orcamento", function (event) {
    event.preventDefault();

    var url = $(this).attr("href");

    $.ajax({
      type: "POST",
      url: url,
      success: function (data, textStatus, jqXHR) {
        console.log(data);
        $(".pop-up-orcamento span.total-itens").html(data);
        $(".pop-up-orcamento").removeClass("closed");
        $("html, body").animate({ scrollTop: $("body").offset().top }, 500);
      },
      error: function (jqXHR, textStatus, errorThrown) {
        console.log(jqXHR, textStatus, errorThrown);
      },
    });
  });

  $(".pop-up-orcamento").click(function () {
    if ($(this).hasClass("closed")) {
      $(this).removeClass("closed");
    } else {
      window.location.href = $(this).data("url");
    }
  });

  // se route produtos, pop-up top
  if (window.location.href.indexOf("produtos") != -1) {
    if ($(window).width() > 1258) {
      $(".pop-up-orcamento").css("top", "20%");
    }
    if($(window).height() < 800) {
      $(".pop-up-orcamento").css("top", "30%");
    }
  }
  // fechar pop-up orçamentos
  $(".pop-up-orcamento .fechar").click(function (e) {
    e.stopPropagation();
    $(this).parent().addClass("closed");
  });

  // EXCLUIR ITEM
  $(".btn-excluir-item").click(function (event) {
    event.preventDefault();

    if (confirm("Deseja excluir este item do pedido de orçamento?")) {
      window.location.href = $(this).attr("href");
    } else {
      return false;
    }
  });

  // AUMENTAR QUANTIDADE
  $(".btn-aumentar-quantidade").click(function (event) {
    event.preventDefault();

    var produto = $(this).attr("id");

    var quantidade = $(".quantidade .numero#" + produto).html();
    quantidade++;

    var url = $(this).attr("href");

    atualizarSessionOrcamento(quantidade, url);
  });

  // DIMINUIR QUANTIDADE
  $(".btn-diminuir-quantidade").click(function (event) {
    event.preventDefault();

    var produto = $(this).attr("id");

    var quantidade = $(".quantidade .numero#" + produto).html();
    quantidade--;

    var url = $(this).attr("href");

    if (quantidade <= 0) {
      window.location.href =
        window.location.origin + urlPrevia + "/orcamentos/excluir/" + produto;
    } else {
      atualizarSessionOrcamento(quantidade, url);
    }
  });

  var atualizarSessionOrcamento = function (quantidade, url) {
    $.ajax({
      type: "GET",
      url: url,
      data: {
        quantidade,
      },
      success: function (data, textStatus, jqXHR) {
        $(".quantidade .numero#" + data.produto).html(quantidade);
      },
      error: function (jqXHR, textStatus, errorThrown) {
        console.log(jqXHR, textStatus, errorThrown);
      },
    });
  };

  $(".link-enviar-orcamento.nao-logado").click(function (event) {
    event.preventDefault();

    alert("Faça o login para enviar seu pedido de orçamento.");
  });
}
