import Clipboard from "./modules/Clipboard.js";
import DataTables from "./modules/DataTables.js";
import DatePicker from "./modules/DatePicker.js";
import DeleteButton from "./modules/DeleteButton.js";
import Filtro from "./modules/Filtro.js";
import GeneratorFields from "./modules/GeneratorFields.js";
import ImagesUpload from "./modules/ImagesUpload.js";
import MonthPicker from "./modules/MonthPicker.js";
import MultiSelect from "./modules/MultiSelect.js";
import OrderImages from "./modules/OrderImages.js";
import OrderTable from "./modules/OrderTable.js";
import TextEditor from "./modules/TextEditor.js";

$.ajaxSetup({
  headers: {
    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
  },
});

Clipboard();
DataTables();
DatePicker();
DeleteButton();
Filtro();
GeneratorFields();
ImagesUpload();
MonthPicker();
MultiSelect();
OrderImages();
OrderTable();
TextEditor();

var urlPrevia = "";
// var urlPrevia = "/previa-newparts";

// PRODUTOS - filtro por categoria
$(".select-categorias").on('change', function (e) {
  e.preventDefault();
  var categoria = $(this).val();
  window.location.href =
    window.location.origin +
    urlPrevia +
    "/painel/produtos" + "?categoria=" + categoria;
});

// PRODUTOS - get categorias por linha selecionada
$(".select-linha-form").on('change', function (e) {
  e.preventDefault();

  var linha = $(this).val();
  var url = window.location.origin + urlPrevia + "/painel/produtos/linhas/" + linha + "/categorias";

  $.ajax({
    type: "GET",
    url: url,
    beforeSend: function () { },
    success: function (data, textStatus, jqXHR) {
      $(".select-categoria-form option").html("").css('display', 'none');
      var htmlOptionBase = "<option value='' selected>Selecione uma categoria</option>";
      $(".select-categoria-form").append(htmlOptionBase);

      data.categorias.forEach(element => {
        var htmlOptions = "<option value='"+element.id+"'>"+element.titulo+"</option>";
        $(".select-categoria-form").append(htmlOptions);
      })

      $(".categorias").css('display', 'block');
    },
    error: function (jqXHR, textStatus, errorThrown) {
      console.log(jqXHR, textStatus, errorThrown);
    },
  });
});

// PRODUTOS - excluir variação
$(".btn-excluir-img").click(function (e) {
  e.preventDefault();

  var element = $(this);
  var variacao = $(this).attr("variacao");

  $.ajax({
    type: "GET",
    url: element.attr("href"),
    data: {
      variacao,
    },
    beforeSend: function () { },
    success: function (data, textStatus, jqXHR) {
      element.parent().children()[1].remove();
      element.remove();
    },
    error: function (jqXHR, textStatus, errorThrown) {
      console.log(jqXHR, textStatus, errorThrown);
    },
  });
});

// PRODUTOS - excluir desenho técnico (imagem)
$(".btn-excluir-desenho").click(function (e) {
  e.preventDefault();

  var element = $(this);

  $.ajax({
    type: "GET",
    url: element.attr("href"),
    beforeSend: function () { },
    success: function (data, textStatus, jqXHR) {
      element.parent().children()[1].remove();
      element.remove();
    },
    error: function (jqXHR, textStatus, errorThrown) {
      console.log(jqXHR, textStatus, errorThrown);
    },
  });
});

// PRODUTOS - excluir manual (arquivo)
$(".btn-excluir-manual").click(function (e) {
  e.preventDefault();

  var element = $(this);

  $.ajax({
    type: "GET",
    url: element.attr("href"),
    beforeSend: function () { },
    success: function (data, textStatus, jqXHR) {
      element.parent().children()[1].remove();
      element.remove();
    },
    error: function (jqXHR, textStatus, errorThrown) {
      console.log(jqXHR, textStatus, errorThrown);
    },
  });
});

