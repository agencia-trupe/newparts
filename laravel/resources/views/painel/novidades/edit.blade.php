@extends('painel.common.template')

@section('content')

<legend>
    <h2><small>Newsletter |</small> Editar Newsletter</h2>
</legend>

{!! Form::model($novidade, [
'route' => ['painel.novidades.update', $novidade->id],
'method' => 'patch',
'files' => true])
!!}

@include('painel.novidades.form', ['submitText' => 'Alterar'])

{!! Form::close() !!}

@endsection