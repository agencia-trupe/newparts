@extends('painel.common.template')

@section('content')

<legend>
    <h2><small>Newsletter |</small> Adicionar Newsletter</h2>
</legend>

{!! Form::open(['route' => 'painel.novidades.store', 'files' => true]) !!}

@include('painel.novidades.form', ['submitText' => 'Inserir'])

{!! Form::close() !!}

@endsection