@extends('painel.common.template')

@section('content')

<legend>
    <h2><small>Categorias de Novidades |</small> Adicionar Categoria</h2>
</legend>

{!! Form::open(['route' => 'painel.categorias-novidades.store', 'files' => true]) !!}

@include('painel.novidades.categorias.form', ['submitText' => 'Inserir'])

{!! Form::close() !!}

@endsection