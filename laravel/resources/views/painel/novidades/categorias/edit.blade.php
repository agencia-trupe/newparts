@extends('painel.common.template')

@section('content')

<legend>
    <h2><small>Categorias de Novidades |</small> Editar Categoria</h2>
</legend>

{!! Form::model($categoria, [
'route' => ['painel.categorias-novidades.update', $categoria->id],
'method' => 'patch',
'files' => true])
!!}

@include('painel.novidades.categorias.form', ['submitText' => 'Alterar'])

{!! Form::close() !!}

@endsection