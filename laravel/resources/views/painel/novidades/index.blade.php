@extends('painel.common.template')

@section('content')

@include('painel.common.flash')

<legend>
    <h2>
        Newsletter
        <a href="{{ route('painel.novidades.create') }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar Newsletter</a>
    </h2>
</legend>

<h4 class="titulo-categorias">Categorias:</h4>
<div class="lista-categorias">
    @foreach($categorias as $categoria)
    <a href="{{ route('painel.novidades.index', ['categoria' => $categoria->id]) }}" class="categoria {{ (isset($_GET['categoria']) && $_GET['categoria'] == $categoria->id) ? 'active' : '' }}">{{ $categoria->titulo }}</a>
    @endforeach
</div>

@if(!count($novidades))
<div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
@else
<table class="table table-striped table-bordered table-hover table-info table-sortable" data-table="novidades">
    <thead>
        <tr>
            <th>Data</th>
            <th>Capa</th>
            <th>Título</th>
            <th>Categoria</th>
            <th class="no-filter" style="min-width: 190px;"><span class="glyphicon glyphicon-cog"></span></th>
        </tr>
    </thead>

    <tbody>
        @foreach ($novidades as $novidade)
        <tr class="tr-row" id="{{ $novidade->id }}">
            <td>{{ strftime("%d/%m/%Y", strtotime($novidade->data)) }}</td>
            <td><img src="{{ asset('assets/img/novidades/miniaturas/'.$novidade->capa) }}" style="width: 100%; max-width:80px;" alt=""></td>
            <td>{{ $novidade->titulo }}</td>
            @php
                $categoria = $categorias->find($novidade->categoria_id)->titulo;
            @endphp
            <td>{{ $categoria }}</td>
            <td class="crud-actions">
                {!! Form::open([
                'route' => ['painel.novidades.destroy', $novidade->id],
                'method' => 'delete'
                ]) !!}

                <div class="btn-group btn-group-sm">
                    <a href="{{ route('painel.novidades.edit', $novidade->id ) }}" class="btn btn-primary btn-sm pull-left">
                        <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
                    </a>

                    <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                </div>

                {!! Form::close() !!}
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@endif

@endsection