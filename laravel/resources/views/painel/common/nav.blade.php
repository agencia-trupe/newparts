<ul class="nav navbar-nav">

    <li class="dropdown @if(Tools::routeIs(['painel.home-banners*', 'painel.home-diferenciais*'])) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Home <b class="caret"></b></a>
        <ul class="dropdown-menu">
            <li @if(Tools::routeIs('painel.home-banners*')) class="active" @endif>
                <a href="{{ route('painel.home-banners.index') }}">Banners</a>
            </li>
            <li @if(Tools::routeIs('painel.home-destaques*')) class="active" @endif>
                <a href="{{ route('painel.home-destaques.index') }}">Links Destaques</a>
            </li>
            <li @if(Tools::routeIs('painel.home-diferenciais*')) class="active" @endif>
                <a href="{{ route('painel.home-diferenciais.index') }}">Diferenciais</a>
            </li>
        </ul>
    </li>

    <li @if(Tools::routeIs('painel.marcas*')) class="active" @endif>
        <a href="{{ route('painel.marcas.index') }}">Marcas</a>
    </li>

    <li class="dropdown @if(Tools::routeIs(['painel.linhas-de-produtos*', 'painel.produtos*'])) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Produtos <b class="caret"></b></a>
        <ul class="dropdown-menu">
            <li @if(Tools::routeIs('painel.linhas*')) class="active" @endif>
                <a href="{{ route('painel.linhas.index') }}">Linhas e Categorias</a>
            </li>
            <li @if(Tools::routeIs('painel.produtos*')) class="active" @endif>
                <a href="{{ route('painel.produtos.index') }}">Produtos</a>
            </li>
        </ul>
    </li>

    <li class="dropdown @if(Tools::routeIs(['painel.empresa.index', 'painel.empresa.imagens*', 'painel.empresa.diferenciais*'])) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Empresa <b class="caret"></b></a>
        <ul class="dropdown-menu">
            <li @if(Tools::routeIs('painel.empresa.index')) class="active" @endif>
                <a href="{{ route('painel.empresa.index') }}">Empresa</a>
            </li>
            <li @if(Tools::routeIs('painel.empresa.imagens*')) class="active" @endif>
                <a href="{{ route('painel.empresa.imagens.index') }}">Imagens</a>
            </li>
            <li @if(Tools::routeIs('painel.empresa.diferenciais*')) class="active" @endif>
                <a href="{{ route('painel.empresa.diferenciais.index') }}">Diferenciais</a>
            </li>
        </ul>
    </li>

    <li class="dropdown @if(Tools::routeIs(['painel.categorias-novidades*', 'painel.novidades*'])) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Newsletter <b class="caret"></b></a>
        <ul class="dropdown-menu">
            <li @if(Tools::routeIs('painel.categorias-novidades*')) class="active" @endif>
                <a href="{{ route('painel.categorias-novidades.index') }}">Categorias</a>
            </li>
            <li @if(Tools::routeIs('painel.novidades*')) class="active" @endif>
                <a href="{{ route('painel.novidades.index') }}">Newsletter</a>
            </li>
        </ul>
    </li>

    <li @if(Tools::routeIs('painel.clientes*')) class="active" @endif>
        <a href="{{ route('painel.clientes.index') }}">Clientes</a>
    </li>

    <li class="dropdown @if(Tools::routeIs('painel.orcamentos*')) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            Orçamentos
            @if($orcamentosNaoLidos >= 1)
            <span class="label label-success" style="margin-left:3px;">{{ $orcamentosNaoLidos }}</span>
            @endif
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li @if(Tools::routeIs('painel.orcamentos.*')) class="active" @endif>
                <a href="{{ route('painel.orcamentos.index') }}">
                    Orçamentos Recebidos
                    @if($orcamentosNaoLidos >= 1)
                    <span class="label label-success" style="margin-left:3px;">{{ $orcamentosNaoLidos }}</span>
                    @endif
                </a>
            </li>
            <li @if(Tools::routeIs('painel.orcamentos-contatos*')) class="active" @endif>
                <a href="{{ route('painel.orcamentos-contatos.index') }}">Contatos (E-mails)</a>
            </li>
        </ul>
    </li>

    <li class="dropdown @if(Tools::routeIs('painel.contato*')) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            Contato
            @if($contatosNaoLidos >= 1)
            <span class="label label-success" style="margin-left:3px;">{{ $contatosNaoLidos }}</span>
            @endif
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li @if(Tools::routeIs('painel.contato.index')) class="active" @endif>
                <a href="{{ route('painel.contato.index') }}">Informações de Contato</a>
            </li>
            <li @if(Tools::routeIs('painel.contato.recebidos*')) class="active" @endif>
                <a href="{{ route('painel.contato.recebidos.index') }}">
                    Contatos Recebidos
                    @if($contatosNaoLidos >= 1)
                    <span class="label label-success" style="margin-left:3px;">{{ $contatosNaoLidos }}</span>
                    @endif
                </a>
            </li>
            <li @if(Tools::routeIs('painel.newsletter.index')) class="active" @endif>
                <a href="{{ route('painel.newsletter.index') }}">Cadastros Newsletter</a>
            </li>
        </ul>
    </li>

</ul>