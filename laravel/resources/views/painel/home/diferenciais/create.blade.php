@extends('painel.common.template')

@section('content')

<legend>
    <h2><small>Home - Diferenciais |</small> Adicionar Diferencial</h2>
</legend>

{!! Form::open(['route' => 'painel.home-diferenciais.store', 'files' => true]) !!}

@include('painel.home.diferenciais.form', ['submitText' => 'Inserir'])

{!! Form::close() !!}

@endsection