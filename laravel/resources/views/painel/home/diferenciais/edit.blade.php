@extends('painel.common.template')

@section('content')

<legend>
    <h2><small>Home - Diferenciais |</small> Editar Diferencial</h2>
</legend>

{!! Form::model($diferencial, [
'route' => ['painel.home-diferenciais.update', $diferencial->id],
'method' => 'patch',
'files' => true])
!!}

@include('painel.home.diferenciais.form', ['submitText' => 'Alterar'])

{!! Form::close() !!}

@endsection