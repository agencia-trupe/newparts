@extends('painel.common.template')

@section('content')

<legend>
    <h2><small>Home - Banners |</small> Adicionar Banner</h2>
</legend>

{!! Form::open(['route' => 'painel.home-banners.store', 'files' => true]) !!}

@include('painel.home.banners.form', ['submitText' => 'Inserir'])

{!! Form::close() !!}

@endsection