@include('painel.common.flash')

<div class="well form-group">
    {!! Form::label('imagem', 'Imagem') !!}
    @if($submitText == 'Alterar')
    <img src="{{ url('assets/img/home/banners/'.$banner->imagem) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    @endif
    {!! Form::file('imagem', ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('cx_titulo', 'Título (caixa de informações)') !!}
    {!! Form::text('cx_titulo', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('cx_subtitulo', 'Subtítulo (caixa de informações)') !!}
    {!! Form::text('cx_subtitulo', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('cx_link_btn', 'Link do Botão (caixa de informações)') !!}
    {!! Form::text('cx_link_btn', null, ['class' => 'form-control']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.home-banners.index') }}" class="btn btn-default btn-voltar">Voltar</a>