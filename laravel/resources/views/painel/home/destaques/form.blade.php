@include('painel.common.flash')

<div class="row">
    <div class="col-md-6">
        <div class="well form-group">
            {!! Form::label('imagem_1', 'Item 1 - Imagem') !!}
            @if($destaques->imagem_1)
            <img src="{{ url('assets/img/home/destaques/'.$destaques->imagem_1) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            @endif
            {!! Form::file('imagem_1', ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('titulo_1', 'Item 1 - Título') !!}
            {!! Form::text('titulo_1', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('link_1', 'Item 1 - Link') !!}
            {!! Form::text('link_1', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

<hr>

<div class="row">
    <div class="col-md-6">
        <div class="well form-group">
            {!! Form::label('imagem_2', 'Item 2 - Imagem') !!}
            @if($destaques->imagem_2)
            <img src="{{ url('assets/img/home/destaques/'.$destaques->imagem_2) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            @endif
            {!! Form::file('imagem_2', ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('titulo_2', 'Item 2 - Título') !!}
            {!! Form::text('titulo_2', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('link_2', 'Item 2 - Link') !!}
            {!! Form::text('link_2', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

<hr>

<div class="row">
    <div class="col-md-6">
        <div class="well form-group">
            {!! Form::label('imagem_3', 'Item 3 - Imagem') !!}
            @if($destaques->imagem_3)
            <img src="{{ url('assets/img/home/destaques/'.$destaques->imagem_3) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            @endif
            {!! Form::file('imagem_3', ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('titulo_3', 'Item 3 - Título') !!}
            {!! Form::text('titulo_3', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('link_3', 'Item 3 - Link') !!}
            {!! Form::text('link_3', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

<hr>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}