@extends('painel.common.template')

@section('content')

<legend>
    <h2>Home - Links Destaques <small>(abaixo dos banners)</small></h2>
</legend>

{!! Form::model($destaques, [
'route' => ['painel.home-destaques.update', $destaques->id],
'method' => 'patch',
'files' => true])
!!}

@include('painel.home.destaques.form', ['submitText' => 'Alterar'])

{!! Form::close() !!}

@endsection