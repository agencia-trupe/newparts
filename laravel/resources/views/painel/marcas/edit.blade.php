@extends('painel.common.template')

@section('content')

<legend>
    <h2><small>Marcas |</small> Editar Marca</h2>
</legend>

{!! Form::model($marca, [
'route' => ['painel.marcas.update', $marca->id],
'method' => 'patch',
'files' => true])
!!}

@include('painel.marcas.form', ['submitText' => 'Alterar'])

{!! Form::close() !!}

@endsection