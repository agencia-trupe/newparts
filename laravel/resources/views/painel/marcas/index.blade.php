@extends('painel.common.template')

@section('content')

@include('painel.common.flash')

<legend>
    <h2>
        Marcas
        <a href="{{ route('painel.marcas.create') }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar Marca</a>
    </h2>
</legend>


@if(!count($marcas))
<div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
@else
<table class="table table-striped table-bordered table-hover table-info table-sortable" data-table="marcas">
    <thead>
        <tr>
            <th style="width: 15%;">Imagem</th>
            <th style="width: 65%;">Nome</th>
            <th class="no-filter"><span class="glyphicon glyphicon-cog"></span></th>
        </tr>
    </thead>

    <tbody>
        @foreach ($marcas as $marca)
        <tr class="tr-row" id="{{ $marca->id }}">
            <td><img src="{{ asset('assets/img/marcas/'.$marca->imagem) }}" style="width: 100%; max-width:80px;" alt=""></td>
            <td>{{ $marca->nome }}</td>
            <td class="crud-actions">
                {!! Form::open([
                'route' => ['painel.marcas.destroy', $marca->id],
                'method' => 'delete'
                ]) !!}

                <div class="btn-group btn-group-sm">
                    <a href="{{ route('painel.marcas.edit', $marca->id ) }}" class="btn btn-primary btn-sm pull-left">
                        <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
                    </a>

                    <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                </div>

                {!! Form::close() !!}
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@endif

@endsection