@extends('painel.common.template')

@section('content')

@include('painel.common.flash')

<legend>
    <h2>Newsletter - Cadastros</h2>
</legend>

@if(!count($registros))
<div class="alert alert-warning" role="alert">Nenhum cadastro recebido.</div>
@else
<table class="table table-striped table-bordered table-hover data-table">
    <thead>
        <tr>
            <th>Data</th>
            <th>Nome</th>
            <th>E-mail</th>
        </tr>
    </thead>

    <tbody>
        @foreach ($registros as $cadastro)
        <tr class="tr-row">
            <td data-order="{{ $cadastro->created_at_order }}">{{ strftime("%d/%m/%Y %H:%M:%S", strtotime($cadastro->created_at)) }}</td>
            <td>{{ $cadastro->nome }}</td>
            <td>
                <button class="btn btn-info btn-sm clipboard" data-clipboard-text="{{ $cadastro->email }}" style="margin-right:5px;border:0;transition:background .3s">
                    <span class="glyphicon glyphicon-copy"></span>
                </button>
                {{ $cadastro->email }}
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@endif

@if(count($registros) > 0)
<div class="exportar-csv">
    <p>Para exportar os cadastros, clique aqui:</p>
    <a href="{{ route('painel.newsletter.export') }}">Exportar</a>
</div>
@endif

@stop