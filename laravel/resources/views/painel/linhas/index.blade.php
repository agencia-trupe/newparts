@extends('painel.common.template')

@section('content')

@include('painel.common.flash')

<legend>
    <h2>
        Linhas de Produtos
    </h2>
</legend>


@if(!count($linhas))
<div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
@else
<table class="table table-striped table-bordered table-hover table-info table-sortable" data-table="linhas">
    <thead>
        <tr>
            <th style="width: 80%;">Título</th>
            <th class="no-filter"><span class="glyphicon glyphicon-cog"></span></th>
        </tr>
    </thead>

    <tbody>
        @foreach ($linhas as $linha)
        <tr class="tr-row" id="{{ $linha->id }}">
            <td>{{ $linha->titulo }}</td>
            <td class="crud-actions">
                <div class="btn-group btn-group-sm">
                    <a href="{{ route('painel.linhas.categorias.index', $linha->id ) }}" class="btn btn-primary btn-sm pull-left">
                        <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Gerenciar Categorias
                    </a>
                </div>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>

@endif

@endsection