@extends('painel.common.template')

@section('content')

<legend>
    <h2><small>Categorias de {{ $linhas->titulo }} |</small> Editar Categoria</h2>
</legend>

{!! Form::model($categoria, [
'route' => ['painel.linhas.categorias.update', $linhas->id, $categoria->id],
'method' => 'patch',
'files' => true])
!!}

@include('painel.linhas.categorias.form', ['submitText' => 'Alterar'])

{!! Form::close() !!}

@endsection