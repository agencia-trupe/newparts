@extends('painel.common.template')

@section('content')

<legend>
    <h2><small>Categorias de {{ $linhas->titulo }} |</small> Adicionar Categoria</h2>
</legend>

{!! Form::open(['route' => ['painel.linhas.categorias.store', $linhas->id], 'files' => true]) !!}

@include('painel.linhas.categorias.form', ['submitText' => 'Inserir'])

{!! Form::close() !!}

@endsection