@extends('painel.common.template')

@section('content')

@include('painel.common.flash')

<legend>
    <h2>Orçamentos Recebidos</h2>
</legend>

@if(!count($orcamentos))
<div class="alert alert-warning" role="alert">Nenhuma mensagem recebida.</div>
@else
<table class="table table-striped table-bordered table-hover data-table">
    <thead>
        <tr>
            <th>Data do pedido</th>
            <th>Cliente</th>
            <th>Itens do pedido</th>
            <th>E-mail enviado para:</th>
            <th class="no-filter"><span class="glyphicon glyphicon-cog"></span></th>
        </tr>
    </thead>

    <tbody>
        @foreach ($orcamentos as $orcamento)
        <tr class="tr-row @if(!$orcamento->lido) warning @endif">
            <td data-order="{{ $orcamento->created_at_order }}">{{ $orcamento->created_at }}</td>
            @php
            $cliente = $clientes->find($orcamento->cliente_id)->nome;
            @endphp
            <td style="text-transform: capitalize;">{{ $cliente }}</td>
            <td>
                <table style="width: 100%;">
                    <thead style="border-bottom:1px solid #e5e5e5">
                        <tr>
                            <td style="font-weight:600;width:15%">Código</td>
                            <td style="font-weight:600;width:65%">Título</td>
                            <td style="text-align: center;font-weight:600;width:20%">Quantidade</td>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($orcamento->itens as $item)
                        <tr>
                            <td style="width:15%">{{ $item['codigo'] }}</td>
                            <td style="width:65%">{{ $item['titulo'] }}</td>
                            <td style="text-align:center;width:20%">{{ $item['quantidade'] }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </td>
            @if($orcamento->contato_id != null)
            @php
            $contato = $contatos->find($orcamento->contato_id)->email;
            @endphp

            @if($contato)
            <td>{{ $contato }}</td>
            @endif
            
            @else
            <td>N/A</td>
            @endif
            <td class="crud-actions">
                {!! Form::open([
                'route' => ['painel.orcamentos.destroy', $orcamento->id],
                'method' => 'delete'
                ]) !!}

                <div class="btn-group btn-group-sm">
                    <a href="{{ route('painel.orcamentos.show', $orcamento->id ) }}" class="btn btn-primary btn-sm pull-left">
                        <span class="glyphicon glyphicon-align-left" style="margin-right:10px;"></span>Ler mensagem
                    </a>

                    <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>

                    <a href="{{ route('painel.orcamentos.toggle', $orcamento->id) }}" class="btn btn-sm pull-left {{ ($orcamento->lido ? 'btn-warning' : 'btn-success') }}">
                        <span class="glyphicon small {{ ($orcamento->lido ? 'glyphicon-repeat' : 'glyphicon-ok') }}"></span>
                    </a>
                </div>

                {!! Form::close() !!}
            </td>
        </tr>

        @endforeach
    </tbody>
</table>
@endif

@stop