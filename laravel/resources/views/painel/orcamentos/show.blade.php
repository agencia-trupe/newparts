@extends('painel.common.template')

@section('content')

<legend>
    <h2>Orçamentos Recebidos</h2>
</legend>

<div class="form-group">
    <label>Data do pedido</label>
    <div class="well">{{ $orcamento->created_at }}</div>
</div>

<div class="form-group">
    <label>Cliente</label>
    <div class="well" style="text-transform: capitalize;">{{ $cliente->nome }}</div>
    <div class="well">{{ $cliente->email }}</div>
    @if($cliente->telefone)
    <div class="well">{{ $cliente->telefone }}</div>
    @endif
    @if($cliente->empresa)
    <div class="well">{{ $cliente->empresa }}</div>
    @endif
</div>

<div class="form-group">
    <label>Itens do pedido</label>
    <div class="well">
        <table style="width: 100%;">
            <thead style="border-bottom:1px solid #95A5A6">
                <tr>
                    <td style="font-weight:600">Código</td>
                    <td style="font-weight:600">Título</td>
                    <td style="text-align: center;font-weight:600";">Quantidade</td>
                </tr>
            </thead>
            <tbody>
                @foreach($orcamento->itens as $item)
                <tr>
                    <td>{{ $item['codigo'] }}</td>
                    <td>{{ $item['titulo'] }}</td>
                    <td style="text-align: center;">{{ $item['quantidade'] }}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>

<a href="{{ route('painel.orcamentos.index') }}" class="btn btn-default btn-voltar">Voltar</a>

@stop