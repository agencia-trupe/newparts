@extends('painel.common.template')

@section('content')

<legend>
    <h2><small>Orçamentos - Contatos/E-mails |</small> Adicionar E-mail</h2>
</legend>

{!! Form::open(['route' => 'painel.orcamentos-contatos.store', 'files' => true]) !!}

@include('painel.orcamentos.contatos.form', ['submitText' => 'Inserir'])

{!! Form::close() !!}

@endsection