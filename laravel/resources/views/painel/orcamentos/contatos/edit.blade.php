@extends('painel.common.template')

@section('content')

<legend>
    <h2><small>Orçamentos - Contatos/E-mails |</small> Editar E-mail</h2>
</legend>

{!! Form::model($contato, [
'route' => ['painel.orcamentos-contatos.update', $contato->id],
'method' => 'patch',
'files' => true])
!!}

@include('painel.orcamentos.contatos.form', ['submitText' => 'Alterar'])

{!! Form::close() !!}

@endsection