@extends('painel.common.template')

@section('content')

@include('painel.common.flash')

<legend>
    <h2>
        Orçamentos - Contatos/E-mails
        <a href="{{ route('painel.orcamentos-contatos.create') }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar E-mail</a>
    </h2>
</legend>


@if(!count($contatos))
<div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
@else
<table class="table table-striped table-bordered table-hover table-info table-sortable" data-table="contatos_orcamentos">
    <thead>
        <tr>
            <th style="width: 80%;">E-mail</th>
            <th class="no-filter"><span class="glyphicon glyphicon-cog"></span></th>
        </tr>
    </thead>

    <tbody>
        @foreach ($contatos as $contato)
        <tr class="tr-row" id="{{ $contato->id }}">
            <td>
                <button class="btn btn-info btn-sm clipboard" data-clipboard-text="{{ $contato->email }}" style="margin-right:5px;border:0;transition:background .3s">
                    <span class="glyphicon glyphicon-copy"></span>
                </button>
                {{ $contato->email }}
            </td>
            <td class="crud-actions">
                {!! Form::open([
                'route' => ['painel.orcamentos-contatos.destroy', $contato->id],
                'method' => 'delete'
                ]) !!}

                <div class="btn-group btn-group-sm">
                    <a href="{{ route('painel.orcamentos-contatos.edit', $contato->id ) }}" class="btn btn-primary btn-sm pull-left">
                        <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
                    </a>

                    <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                </div>

                {!! Form::close() !!}
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@endif

@endsection