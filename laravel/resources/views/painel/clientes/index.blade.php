@extends('painel.common.template')

@section('content')

@include('painel.common.flash')

<legend>
    <h2>Clientes - Cadastros</h2>
</legend>

@if(!count($clientes))
<div class="alert alert-warning" role="alert">Nenhum cadastro recebido.</div>
@else
<table class="table table-striped table-bordered table-hover data-table">
    <thead>
        <tr>
            <th>Data</th>
            <th>Nome</th>
            <th>E-mail</th>
            <th>Telefone</th>
            <th>Empresa</th>
            <th class="no-filter"><span class="glyphicon glyphicon-cog"></span></th>
        </tr>
    </thead>

    <tbody>
        @foreach ($clientes as $cliente)
        <tr class="tr-row">
            <td data-order="{{ $cliente->created_at_order }}">{{ strftime("%d/%m/%Y %H:%M:%S", strtotime($cliente->created_at)) }}</td>
            <td>{{ $cliente->nome }}</td>
            <td>
                <button class="btn btn-info btn-sm clipboard" data-clipboard-text="{{ $cliente->email }}" style="margin-right:5px;border:0;transition:background .3s">
                    <span class="glyphicon glyphicon-copy"></span>
                </button>
                {{ $cliente->email }}
            </td>
            <td>{{ $cliente->telefone }}</td>
            <td>{{ $cliente->empresa }}</td>

            <td class="crud-actions">
                {!! Form::open([
                'route' => ['painel.clientes.destroy', $cliente->id],
                'method' => 'delete'
                ]) !!}

                <div class="btn-group btn-group-sm">
                    <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                </div>

                {!! Form::close() !!}
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@endif

@if(count($clientes) > 0)
<div class="exportar-csv">
    <p>Para exportar os cadastros dos clientes, clique aqui:</p>
    <a href="{{ route('painel.clientes.export') }}">Exportar</a>
</div>
@endif

@stop