@extends('painel.common.template')

@section('content')

<legend>
    <h2>Empresa</h2>
</legend>

{!! Form::model($empresa, [
'route' => ['painel.empresa.update', $empresa->id],
'method' => 'patch',
'files' => true])
!!}

@include('painel.empresa.form', ['submitText' => 'Alterar'])

{!! Form::close() !!}

@endsection