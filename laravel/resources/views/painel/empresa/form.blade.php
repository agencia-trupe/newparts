@include('painel.common.flash')

<div class="well form-group">
    {!! Form::label('capa', 'Capa') !!}
    @if($empresa->capa)
    <img src="{{ url('assets/img/empresa/'.$empresa->capa) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    @endif
    {!! Form::file('capa', ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('texto', 'Texto') !!}
    {!! Form::textarea('texto', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
</div>

<div class="form-group">
    {!! Form::label('missao', 'Missão') !!}
    {!! Form::textarea('missao', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
</div>

<div class="form-group">
    {!! Form::label('visao', 'Visão') !!}
    {!! Form::textarea('visao', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
</div>

<div class="form-group">
    {!! Form::label('valores', 'Valores') !!}
    {!! Form::textarea('valores', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
</div>

<hr>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}