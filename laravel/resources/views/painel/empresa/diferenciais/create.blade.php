@extends('painel.common.template')

@section('content')

<legend>
    <h2><small>Empresa - Diferenciais |</small> Adicionar Diferencial</h2>
</legend>

{!! Form::open(['route' => 'painel.empresa.diferenciais.store', 'files' => true]) !!}

@include('painel.empresa.diferenciais.form', ['submitText' => 'Inserir'])

{!! Form::close() !!}

@endsection