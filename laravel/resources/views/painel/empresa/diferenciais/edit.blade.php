@extends('painel.common.template')

@section('content')

<legend>
    <h2><small>Empresa - Diferenciais |</small> Editar Diferencial</h2>
</legend>

{!! Form::model($diferencial, [
'route' => ['painel.empresa.diferenciais.update', $diferencial->id],
'method' => 'patch',
'files' => true])
!!}

@include('painel.empresa.diferenciais.form', ['submitText' => 'Alterar'])

{!! Form::close() !!}

@endsection