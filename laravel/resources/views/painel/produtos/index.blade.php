@extends('painel.common.template')

@section('content')

@include('painel.common.flash')

<legend>
    <h2>
        Produtos
        <a href="{{ route('painel.produtos.create') }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar Produto</a>
    </h2>
</legend>

@if(isset($_GET['linha']) || isset($_GET['categoria']) || isset($_GET['marca']))
<a href="{{ route('painel.produtos.index') }}" class="btn btn-default btn-sm pull-left btn-todos-projetos">Voltar para todos os produtos</a>
@endif

<div class="filtros">
    <div class="linhas">
        <h4 class="titulo-linhas">Linhas de Produtos</h4>
        <div class="lista-linhas">
            @foreach($linhas as $linha)
            <a href="{{ route('painel.produtos.index', ['linha' => $linha->id]) }}" class="linha {{ (isset($_GET['linha']) && $_GET['linha'] == $linha->id) ? 'active' : '' }}">{{ $linha->titulo }}</a>
            @endforeach
        </div>

        <div class="categorias">
            <h4 class="titulo-categorias">Categorias</h4>
            <div class="lista-categorias-select">
                <select name="categoria" class="select-categorias">
                    @foreach($categorias as $categoria)
                    <option value="{{ $categoria->id }}" @if(isset($_GET['categoria']) && $_GET['categoria'] == $categoria->id) selected @endif>
                        @if($categoria->linha_id == 1) 
                        [EL] {{ $categoria->titulo }}
                        @elseif($categoria->linha_id == 2)
                        [ER] {{ $categoria->titulo }}
                        @endif
                    </option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>

    <div class="marcas">
        <h4 class="titulo-marcas">Marcas</h4>
        <div class="lista-marcas">
            @foreach($marcas as $marca)
            <a href="{{ route('painel.produtos.index', ['marca' => $marca->id]) }}" class="marca {{ (isset($_GET['marca']) && $_GET['marca'] == $marca->id) ? 'active' : '' }}">{{ $marca->nome }}</a>
            @endforeach
        </div>
    </div>

    <div class="vitrines">
        <h4 class="titulo-vitrines">Vitrines</h4>
        <a href="{{ route('painel.produtos.index', ['vitrine' => 'vitrine_home']) }}" class="vitrine {{ (isset($_GET['vitrine']) && $_GET['vitrine'] == 'vitrine_home') ? 'active' : '' }}">Vitrine Home</a>
        <a href="{{ route('painel.produtos.index', ['vitrine' => 'vitrine_produtos']) }}" class="vitrine {{ (isset($_GET['vitrine']) && $_GET['vitrine'] == 'vitrine_produtos') ? 'active' : '' }}">Vitrine Produtos</a>
        <a href="{{ route('painel.produtos.index', ['vitrine' => 'lancamentos']) }}" class="vitrine {{ (isset($_GET['vitrine']) && $_GET['vitrine'] == 'lancamentos') ? 'active' : '' }}">Lançamentos</a>
    </div>
</div>

@if(!count($produtos))
<div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
@else
<table class="table table-striped table-bordered table-hover table-info table-sortable data-table" data-table="produtos">
    <thead>
        <tr>
            <th>Capa</th>
            <th>Título</th>
            <th>Código</th>
            <th>Marca</th>
            <th>Linha</th>
            <th>Categoria</th>
            <th class="no-filter" style="min-width: 175px;"><span class="glyphicon glyphicon-cog"></span></th>
        </tr>
    </thead>

    <tbody>
        @foreach ($produtos as $produto)
        <tr class="tr-row" id="{{ $produto->id }}">
            <td><img src="{{ asset('assets/img/produtos/'.$produto->capa) }}" style="width: 100%; max-width:80px;" alt=""></td>
            <td>{{ $produto->titulo }}</td>
            <td>{{ $produto->codigo }}</td>
            <td>{{ $produto->marca_nome }}</td>
            <td>{{ $produto->linha_titulo }}</td>
            <td>{{ $produto->categoria_titulo }}</td>
            <td class="crud-actions">
                {!! Form::open([
                'route' => ['painel.produtos.destroy', $produto->id],
                'method' => 'delete'
                ]) !!}

                <div class="btn-group btn-group-sm">
                    <a href="{{ route('painel.produtos.edit', $produto->id ) }}" class="btn btn-primary btn-sm pull-left">
                        <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
                    </a>

                    <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                </div>

                {!! Form::close() !!}
            </td>
        </tr>
        @endforeach
    </tbody>
</table>

@endif

@endsection