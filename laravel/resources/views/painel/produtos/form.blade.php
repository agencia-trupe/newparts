@include('painel.common.flash')

<div class="row">
    <div class="col-md-4">
        <div class="well">
            <div class="checkbox" style="margin:0">
                <label style="font-weight:bold">
                    {!! Form::hidden('vitrine_home', 0) !!}
                    {!! Form::checkbox('vitrine_home') !!}
                    Vitrine HOME
                </label>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="well">
            <div class="checkbox" style="margin:0">
                <label style="font-weight:bold">
                    {!! Form::hidden('vitrine_produtos', 0) !!}
                    {!! Form::checkbox('vitrine_produtos') !!}
                    Vitrine PRODUTOS
                </label>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="well">
            <div class="checkbox" style="margin:0">
                <label style="font-weight:bold">
                    {!! Form::hidden('lancamento', 0) !!}
                    {!! Form::checkbox('lancamento') !!}
                    Lançamento
                </label>
            </div>
        </div>
    </div>
</div>

<div class="form-group linhas">
    <select name="linha_id" class="select-linha-form form-control" required>
        <option value="">Selecione uma linha</option>
        @foreach($linhas as $linha)
        <option value="{{ $linha->id }}" @if($submitText=='Alterar' && isset($linhaSelected) && $linhaSelected->id == $linha->id) selected @endif>{{ $linha->titulo }}</option>
        @endforeach
    </select>
</div>

@if($submitText == 'Alterar')
<div class="form-group categorias">
    <select name="categoria_id" class="select-categoria-form form-control" required>
        @foreach($categorias as $categoria)
        <option value="{{ $categoria->id }}" @if($submitText=='Alterar' && isset($categoriaSelected) && $categoriaSelected->id == $categoria->id) selected @endif>{{ $categoria->titulo }}</option>
        @endforeach
    </select>
</div>
@else
<div class="form-group categorias" style="display: none;">
    <select name="categoria_id" class="select-categoria-form form-control" required>
        <!-- AJAX -->
    </select>
</div>
@endif

<div class="form-group">
    {!! Form::label('codigo', 'Código') !!}
    {!! Form::text('codigo', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('titulo', 'Título') !!}
    {!! Form::text('titulo', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('descricao', 'Descrição') !!}
    {!! Form::textarea('descricao', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
</div>

<div class="form-group linhas">
    <select name="marca_id" class="select-marca-form form-control" required>
        <option value="">Selecione uma marca</option>
        @foreach($marcas as $marca)
        <option value="{{ $marca->id }}" @if($submitText=='Alterar' && $produto->marca_id == $marca->id) selected @endif>{{ $marca->nome }}</option>
        @endforeach
    </select>
</div>

<div class="well form-group">
    {!! Form::label('capa', 'Capa') !!}
    @if($submitText == 'Alterar')
    <img src="{{ url('assets/img/produtos/'.$produto->capa) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    @endif
    {!! Form::file('capa', ['class' => 'form-control']) !!}
</div>

<div class="well form-group" style="position: relative;">
    {!! Form::label('imagem', 'Imagem / Desenho Técnico') !!}
    @if($submitText == 'Alterar')
    <img src="{{ url('assets/img/produtos/imagens/'.$produto->imagem) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    <a href="{{ route('painel.produtos.desenho.delete', $produto->id) }}" class="btn btn-danger btn-sm btn-excluir-desenho" style="position: absolute; bottom: 80px; right: 20px;" produto="{{$produto->id}}">
        <span class="glyphicon glyphicon-remove"></span>
    </a>
    @endif
    {!! Form::file('imagem', ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('video', 'Vídeo do YouTube') !!}
    {!! Form::text('video', null, ['class' => 'form-control']) !!}
    <p class="observacao" style="color: red; font-style: italic;">Incluir aqui somente a parte da url após o "v=". Exemplo: <span style="text-decoration: underline;">https://www.youtube.com/watch?v=<strong>Y4goaZhNt4k</strong></span></p>
</div>

<div class="well form-group" style="position: relative;">
    {!! Form::label('arquivo', 'Arquivo / Manual') !!}
    @if($submitText == 'Alterar')
    @if($produto->arquivo)
    <a href="{{ asset('assets/arquivos/'.$produto->arquivo) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">{{ $produto->arquivo }}</a>
    <a href="{{ route('painel.produtos.manual.delete', $produto->id) }}" class="btn btn-danger btn-sm btn-excluir-manual" style="position: absolute; bottom: 80px; right: 20px;" produto="{{$produto->id}}">
        <span class="glyphicon glyphicon-remove"></span>
    </a>
    @endif
    @endif
    {!! Form::file('arquivo', ['class' => 'form-control']) !!}
</div>

<hr>

<h2><small>Variações</small></h2>

<div class="row">
    <div class="col-md-6">
        <div class="well form-group">
            {!! Form::label('variacao_01', 'Variação 01 (opcional)') !!}
            @if($submitText == 'Alterar' && $produto->variacao_01)
            <img src="{{ url('assets/img/produtos/variacoes/'.$produto->variacao_01) }}" style="display:block; margin-bottom: 10px; max-width: 150px;" class="img-variacao">
            <a href="{{ route('painel.produtos.variacoes.delete', $produto->id) }}" class="btn btn-danger btn-sm btn-excluir-img" style="position: absolute; bottom: 100px; right: 40px;" produto="{{$produto->id}}" variacao="variacao_01">
                <span class="glyphicon glyphicon-remove"></span>
            </a>
            @endif
            {!! Form::file('variacao_01', ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="well form-group">
            {!! Form::label('variacao_02', 'Variação 02 (opcional)') !!}
            @if($submitText == 'Alterar' && $produto->variacao_02)
            <img src="{{ url('assets/img/produtos/variacoes/'.$produto->variacao_02) }}" style="display:block; margin-bottom: 10px; max-width: 150px;" class="img-variacao">
            <a href="{{ route('painel.produtos.variacoes.delete', $produto->id) }}" class="btn btn-danger btn-sm btn-excluir-img" style="position: absolute; bottom: 100px; right: 40px;" produto="{{$produto->id}}" variacao="variacao_02">
                <span class="glyphicon glyphicon-remove"></span>
            </a>
            @endif
            {!! Form::file('variacao_02', ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="well form-group">
            {!! Form::label('variacao_03', 'Variação 03 (opcional)') !!}
            @if($submitText == 'Alterar' && $produto->variacao_03)
            <img src="{{ url('assets/img/produtos/variacoes/'.$produto->variacao_03) }}" style="display:block; margin-bottom: 10px; max-width: 150px;" class="img-variacao">
            <a href="{{ route('painel.produtos.variacoes.delete', $produto->id) }}" class="btn btn-danger btn-sm btn-excluir-img" style="position: absolute; bottom: 100px; right: 40px;" produto="{{$produto->id}}" variacao="variacao_03">
                <span class="glyphicon glyphicon-remove"></span>
            </a>
            @endif
            {!! Form::file('variacao_03', ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="well form-group">
            {!! Form::label('variacao_04', 'Variação 04 (opcional)') !!}
            @if($submitText == 'Alterar' && $produto->variacao_04)
            <img src="{{ url('assets/img/produtos/variacoes/'.$produto->variacao_04) }}" style="display:block; margin-bottom: 10px; max-width: 150px;" class="img-variacao">
            <a href="{{ route('painel.produtos.variacoes.delete', $produto->id) }}" class="btn btn-danger btn-sm btn-excluir-img" style="position: absolute; bottom: 100px; right: 40px;" produto="{{$produto->id}}" variacao="variacao_04">
                <span class="glyphicon glyphicon-remove"></span>
            </a>
            @endif
            {!! Form::file('variacao_04', ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="well form-group">
            {!! Form::label('variacao_05', 'Variação 05 (opcional)') !!}
            @if($submitText == 'Alterar' && $produto->variacao_05)
            <img src="{{ url('assets/img/produtos/variacoes/'.$produto->variacao_05) }}" style="display:block; margin-bottom: 10px; max-width: 150px;" class="img-variacao">
            <a href="{{ route('painel.produtos.variacoes.delete', $produto->id) }}" class="btn btn-danger btn-sm btn-excluir-img" style="position: absolute; bottom: 100px; right: 40px;" produto="{{$produto->id}}" variacao="variacao_05">
                <span class="glyphicon glyphicon-remove"></span>
            </a>
            @endif
            {!! Form::file('variacao_05', ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="well form-group">
            {!! Form::label('variacao_06', 'Variação 06 (opcional)') !!}
            @if($submitText == 'Alterar' && $produto->variacao_06)
            <img src="{{ url('assets/img/produtos/variacoes/'.$produto->variacao_06) }}" style="display:block; margin-bottom: 10px; max-width: 150px;" class="img-variacao">
            <a href="{{ route('painel.produtos.variacoes.delete', $produto->id) }}" class="btn btn-danger btn-sm btn-excluir-img" style="position: absolute; bottom: 100px; right: 40px;" produto="{{$produto->id}}" variacao="variacao_06">
                <span class="glyphicon glyphicon-remove"></span>
            </a>
            @endif
            {!! Form::file('variacao_06', ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="well form-group">
            {!! Form::label('variacao_07', 'Variação 07 (opcional)') !!}
            @if($submitText == 'Alterar' && $produto->variacao_07)
            <img src="{{ url('assets/img/produtos/variacoes/'.$produto->variacao_07) }}" style="display:block; margin-bottom: 10px; max-width: 150px;" class="img-variacao">
            <a href="{{ route('painel.produtos.variacoes.delete', $produto->id) }}" class="btn btn-danger btn-sm btn-excluir-img" style="position: absolute; bottom: 100px; right: 40px;" produto="{{$produto->id}}" variacao="variacao_07">
                <span class="glyphicon glyphicon-remove"></span>
            </a>
            @endif
            {!! Form::file('variacao_07', ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="well form-group">
            {!! Form::label('variacao_08', 'Variação 08 (opcional)') !!}
            @if($submitText == 'Alterar' && $produto->variacao_08)
            <img src="{{ url('assets/img/produtos/variacoes/'.$produto->variacao_08) }}" style="display:block; margin-bottom: 10px; max-width: 150px;" class="img-variacao">
            <a href="{{ route('painel.produtos.variacoes.delete', $produto->id) }}" class="btn btn-danger btn-sm btn-excluir-img" style="position: absolute; bottom: 100px; right: 40px;" produto="{{$produto->id}}" variacao="variacao_08">
                <span class="glyphicon glyphicon-remove"></span>
            </a>
            @endif
            {!! Form::file('variacao_08', ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="well form-group">
            {!! Form::label('variacao_09', 'Variação 09 (opcional)') !!}
            @if($submitText == 'Alterar' && $produto->variacao_09)
            <img src="{{ url('assets/img/produtos/variacoes/'.$produto->variacao_09) }}" style="display:block; margin-bottom: 10px; max-width: 150px;" class="img-variacao">
            <a href="{{ route('painel.produtos.variacoes.delete', $produto->id) }}" class="btn btn-danger btn-sm btn-excluir-img" style="position: absolute; bottom: 100px; right: 40px;" produto="{{$produto->id}}" variacao="variacao_09">
                <span class="glyphicon glyphicon-remove"></span>
            </a>
            @endif
            {!! Form::file('variacao_09', ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="well form-group">
            {!! Form::label('variacao_10', 'Variação 10 (opcional)') !!}
            @if($submitText == 'Alterar' && $produto->variacao_10)
            <img src="{{ url('assets/img/produtos/variacoes/'.$produto->variacao_10) }}" style="display:block; margin-bottom: 10px; max-width: 150px;" class="img-variacao">
            <a href="{{ route('painel.produtos.variacoes.delete', $produto->id) }}" class="btn btn-danger btn-sm btn-excluir-img" style="position: absolute; bottom: 100px; right: 40px;" produto="{{$produto->id}}" variacao="variacao_10">
                <span class="glyphicon glyphicon-remove"></span>
            </a>
            @endif
            {!! Form::file('variacao_10', ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

<hr>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.produtos.index') }}" class="btn btn-default btn-voltar">Voltar</a>