@extends('frontend.common.template')

@section('content')

<section class="home">

    <div class="banners">
        @foreach($banners as $banner)
        <div class="banner" style="background-image: url({{ asset('assets/img/home/banners/'.$banner->imagem) }})">
            <div class="overlay-banner">
                <p class="titulo">{{ $banner->cx_titulo }}</p>
                <p class="subtitulo">{{ $banner->cx_subtitulo }}</p>
                <a href="{{ $banner->cx_link_btn }}" class="btn-link"><img src="{{ asset('assets/img/layout/setinha-fios-branca.svg') }}" alt="" class="img-setinha"></a>
            </div>
        </div>
        @endforeach
        <div class=cycle-pager></div>
    </div>

    <div class="itens-destaques">
        <div class="left"></div>
        <a href="{{ $destaques->link_1 }}" class="destaque1">
            <img src="{{ asset('assets/img/home/destaques/'.$destaques->imagem_1) }}" alt="{{ $destaques->titulo_1}}" class="fundo">
            <span class="titulo">{{ $destaques->titulo_1}}</span>
        </a>
        <a href="{{ $destaques->link_2 }}" class="destaque2">
            <img src="{{ asset('assets/img/home/destaques/'.$destaques->imagem_2) }}" alt="{{ $destaques->titulo_2}}" class="fundo">
            <span class="titulo">{{ $destaques->titulo_2}}</span>
        </a>
        <a href="{{ $destaques->link_3 }}" class="destaque3">
            <img src="{{ asset('assets/img/home/destaques/'.$destaques->imagem_3) }}" alt="{{ $destaques->titulo_3}}" class="fundo">
            <span class="titulo">{{ $destaques->titulo_3}}</span>
        </a>
        <div class="right"></div>
    </div>

    <div class="orcamento">
        <div class="centralizado">
            <div class="titulo">Solicite seu orçamento on-line</div>
            <div class="texto">Nosso website é um catálogo completo de produtos onde você pode organizar e enviar seu pedido de orçamento diretamente para os nossos consultores</div>
            <a href="{{ route('produtos', ['linha_slug' => 'elevadores']) }}" class="btn-orcamento">SELECIONAR PRODUTOS <span>»</span></a>
        </div>
    </div>

    <div class="produtos" id="produtosVitrineHome">
        <!-- JQUERY-AJAX (ver main.js) -->
    </div>

    <div class="diferenciais">
        <div class="centralizado">
            <h2 class="titulo">CONTE COM A EXPERIÊNCIA E PROFISSIONALISMO DA NEW PARTS</h2>
            <div class="itens">
                @foreach($diferenciais as $diferencial)
                @if($diferencial->link)
                <a href="{{ $diferencial->link }}" class="diferencial">
                    <div class="img-diferencial">
                        <img src="{{ asset('assets/img/home/diferenciais/'.$diferencial->imagem) }}" alt="{{ $diferencial->titulo }}">
                    </div>
                    <p class="titulo">{{ $diferencial->titulo }}</p>
                </a>
                @else
                <div class="diferencial">
                    <div class="img-diferencial">
                        <img src="{{ asset('assets/img/home/diferenciais/'.$diferencial->imagem) }}" alt="{{ $diferencial->titulo }}">
                    </div>
                    <p class="titulo">{{ $diferencial->titulo }}</p>
                </div>
                @endif
                @endforeach
            </div>
        </div>
    </div>

    <div class="novidades">
        <h2 class="titulo">NEWSLETTER</h2>
        <div class="itens-novidades">
            @foreach($novidades as $novidade)
            <a href="{{ route('novidades.show', $novidade->slug) }}" class="novidade">
                <img src="{{ asset('assets/img/novidades/miniaturas/'.$novidade->capa) }}" alt="{{ $novidade->titulo }}" class="img-novidade">
                <span class="categoria">{{ $novidade->cat_titulo }}</span>
                <p class="titulo-novidade">{{ $novidade->titulo }}</p>
            </a>
            @endforeach
        </div>
    </div>

    <div class="marcas">
        <h2 class="titulo">MARCAS</h2>
        <div class="centralizado" id="marcasHome">
            <!-- JQUERY-AJAX (ver main.js) -->
        </div>
    </div>

    <div class="newsletter">
        <form action="{{ route('newsletter.post') }}" method="POST" class="form-newsletter">
            {!! csrf_field() !!}
            <p class="frase">Cadastre-se para receber novidades em seu e-mail:</p>
            <input type="text" name="nome" placeholder="nome" value="{{ old('nome') }}" required>
            <input type="email" name="email" placeholder="e-mail" value="{{ old('email') }}" required>
            <button type="submit" class="btn-enviar"><img src="{{ asset('assets/img/layout/setinha-fios.svg') }}" alt="" class="img-seta"></button>
        </form>
        @if($errors->any())
        <div class="flash flash-erro">
            @foreach($errors->all() as $error)
            {!! $error !!}<br>
            @endforeach
        </div>
        @endif

        @if(session('success'))
        <div class="flash flash-sucesso">
            <p>Cadastro enviado com sucesso!</p>
        </div>
        @endif
    </div>

</section>

@endsection