@extends('frontend.common.template')

@section('content')

<section class="marcas">
    @foreach($marcas as $marca)
    <a href="{{ route('produtos.marcas', ['elevadores', $marca->slug]) }}" class="marca">
        <img src="{{ asset('assets/img/marcas/'.$marca->imagem) }}" alt="{{ $marca->nome }}" class="img-marca">
    </a>
    @endforeach
</section>

@endsection