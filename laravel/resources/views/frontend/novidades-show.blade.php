@extends('frontend.common.template')

@section('content')

<section class="novidades-show">

    <div class="faixa-superior">
        <h4 class="titulo">{{ $novidade->titulo }}</h4>
    </div>

    <div class="conteudo">
        <div class="left">
            <img src="{{ asset('assets/img/novidades/capas/'.$novidade->capa) }}" class="img-capa" alt="{{ $novidade->titulo }}">
            <span class="categoria">{{ $novidade->cat_titulo }}</span>
            <a href="{{ route('novidades') }}" class="btn-voltar">« voltar</a>
        </div>
        <div class="right">
            <div class="texto">{!! $novidade->texto !!}</div>
        </div>
    </div>

</section>

@endsection