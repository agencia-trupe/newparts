@extends('frontend.common.template')

@section('content')

<section class="empresa">

    <div class="dados-empresa">
        <div class="capa-empresa">
            <img src="{{ asset('assets/img/empresa/'.$empresa->capa) }}" alt="{{ $config->title }}" class="img-capa">
        </div>
        <div class="texto-empresa">
            {!! $empresa->texto !!}
        </div>
    </div>
    <div class="imagens-empresa">
        @foreach($imagens as $imagem)
        <a href="#" class="empresa-imagem" data-empresa-id="{{ $empresa->id }}">
            <img src="{{ asset('assets/img/empresa/imagens/'.$imagem->imagem) }}" class="img-empresa" alt="">
        </a>
        @endforeach
    </div>

    <div class="pilares">
        <div class="missao">
            <div class="img"><img src="{{ asset('assets/img/layout/icone-missao.svg') }}" alt="" class="img-missao"></div>
            <p class="titulo">MISSÃO</p>
            <div class="texto">{!! $empresa->missao !!}</div>
        </div>
        <div class="visao">
            <div class="img"><img src="{{ asset('assets/img/layout/icone-visao.svg') }}" alt="" class="img-visao"></div>
            <p class="titulo">VISÃO</p>
            <div class="texto">{!! $empresa->visao !!}</div>
        </div>
        <div class="valores">
            <div class="img"><img src="{{ asset('assets/img/layout/icone-valores.svg') }}" alt="" class="img-valores"></div>
            <p class="titulo">VALORES</p>
            <div class="texto">{!! $empresa->valores !!}</div>
        </div>
    </div>

    <div class="diferenciais">
        <h2 class="titulo">NOSSOS DIFERENCIAIS</h2>
        <div class="centralizado">
            @foreach($diferenciais as $diferencial)
            <div class="diferencial">
                <div class="img-diferencial">
                    <img src="{{ asset('assets/img/empresa/diferenciais/'.$diferencial->imagem) }}" alt="{{ $diferencial->titulo }}">
                </div>
                <p class="titulo-diferencial">{{ $diferencial->titulo }}</p>
            </div>
            @endforeach
        </div>
    </div>

    <div class="empresa-imgs-show" style="display:none;">
        @foreach($imagens as $imagem)
        <a href="{{ asset('assets/img/empresa/imagens/ampliacoes/'.$imagem->imagem) }}" class="fancybox" rel="empresa-{{ $empresa->id }}"></a>
        @endforeach
    </div>

</section>

@endsection