@extends('frontend.common.template')

@section('content')

<section class="busca-resultado">

    <div class="submenu-resultado">
        <h2 class="titulo">RESULTADO DA BUSCA</h2>

        @if($palavra != null)
        <p class="palavra-chave"><span class="item">Palavra-chave:</span>{{ $palavra }}</p>
        @endif

        @if($marca != null)
        <p class="marca"><span class="item">Marca:</span>{{ $marca->nome }}</p>
        @endif
    </div>

    <div class="resultados-produtos" id="resultadosProdutos">
        @foreach($categorias as $categoria)
        <h4 class="categoria-titulo">{{ $categoria->titulo }}</h4>
        <div class="produtos-categoria">
            @foreach($produtos as $produto)
            @if($produto->categoria_id == $categoria->id)
            <a href="{{ route('produtos.show', [$linha->slug, $produto->slug]) }}" class="link-produto">
                <div class="imagens">
                    <img src="{{ asset('assets/img/produtos/'.$produto->capa) }}" alt="{{ $produto->titulo }}" class="img-produto">
                    <img src="{{ asset('assets/img/layout/marca-newparts-cinza.svg') }}" alt="{{ $config->title }}" class="img-logo">
                </div>
                <div class="marca">
                    <img src="{{ asset('assets/img/marcas/'.$produto->marca_imagem) }}" alt="{{ $produto->marca_nome }}" class="img-marca">
                </div>
                <p class="codigo">[{{ $produto->codigo }}]</p>
                <p class="titulo">{{ $produto->titulo }}</p>
            </a>
            @endif
            @endforeach
        </div>
        @endforeach
    </div>

</section>

@endsection