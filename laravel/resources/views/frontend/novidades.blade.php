@extends('frontend.common.template')

@section('content')

<section class="novidades">

    <div class="destaque-topo">
        <a href="{{ route('novidades.show', $destaqueTopo->slug) }}" class="centralizado">
            <h2 class="titulo">DESTAQUES E NOVIDADES DO SETOR</h2>
            <div class="capa-categoria">
                <img src="{{ asset('assets/img/novidades/destaques/'.$destaqueTopo->capa) }}" alt="{{ $destaqueTopo->titulo }}" class="img-novidade">
                <span class="categoria">{{ $destaqueTopo->cat_titulo }}</span>
            </div>
            <p class="titulo-novidade">{{ $destaqueTopo->titulo }}</p>
        </a>
    </div>

    <div class="destaques">
        @foreach($destaques as $destaque)
        <a href="{{ route('novidades.show', $destaque->slug) }}" class="novidade">
            <img src="{{ asset('assets/img/novidades/miniaturas/'.$destaque->capa) }}" alt="{{ $destaque->titulo }}" class="img-novidade">
            <span class="categoria">{{ $destaque->cat_titulo }}</span>
            <p class="titulo-novidade">{{ $destaque->titulo }}</p>
        </a>
        @endforeach
    </div>

    <div class="todas-novidades">
        @foreach($novidades as $novidade)
        <div class="novidade">
            <div class="categoria">{{ $novidade->cat_titulo }}</div>
            <a href="{{ route('novidades.show', $novidade->slug) }}" class="link-novidade">{{ $novidade->titulo }}</a>
        </div>
        @endforeach
    </div>
    <button class="btn-mais-novidades">ver mais +</button>

</section>

@endsection