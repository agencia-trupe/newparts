<footer>
    <div class="footer">

        <div class="parte-um">
            <a href="{{ route('home') }}" @if(Tools::routeIs('home*')) class="link-nav active" @endif class="link-nav">• HOME</a>
            <p class="grupo-produtos">• PRODUTOS</p>
            <p class="linha {{ strpos(url()->current(), 'escadas-rolantes') ? 'active' : '' }}">» Escadas Rolantes</p>
            <a href="{{ route('produtos', ['escadas-rolantes']) }}" class="sublink">Todas as categorias</a>
            <p class="linha {{ strpos(url()->current(), 'elevadores') ? 'active' : '' }}">» Elevadores</p>
            <a href="{{ route('produtos.filtro', ['linha_slug' => 'elevadores', 'categoria_slug' => $cabines->slug]) }}" class="sublink" id="linha{{ $cabines->id }}">Linhas de Botoeiras</a>
            <a href="{{ route('produtos.filtro', ['linha_slug' => 'elevadores', 'categoria_slug' => $cabines->slug]) }}" class="sublink" id="linha{{ $cabines->id }}">Linhas de Cabines</a>
            <a href="{{ route('produtos', ['elevadores']) }}" class="sublink">Todas as categorias</a>
        </div>

        <div class="parte-dois">
            <a href="{{ route('orcamentos') }}" @if(Tools::routeIs('orcamentos*')) class="link-nav active" @endif class="link-nav">• ORÇAMENTO</a>
            <a href="{{ route('marcas') }}" @if(Tools::routeIs('marcas*')) class="link-nav active" @endif class="link-nav">• MARCAS</a>
            <a href="{{ route('empresa') }}" @if(Tools::routeIs('empresa*')) class="link-nav active" @endif class="link-nav">• EMPRESA</a>
            <a href="{{ route('novidades') }}" @if(Tools::routeIs('novidades*')) class="link-nav active" @endif class="link-nav">• NEWSLETTER</a>
            <a href="{{ route('contato') }}" @if(Tools::routeIs('contato*')) class="link-nav active" @endif class="link-nav">• CONTATO</a>
            <a href="#" class="link-nav">• ÁREA DO CLIENTE</a>
        </div>

        <div class="parte-tres">
            <a href="{{ route('home') }}" class="logo-home" title="{{ $config->title }}"><img src="{{ asset('assets/img/layout/marca-newparts-branco.svg') }}" alt="{{ $config->title }}" class="img-logo-home"></a>
            @php $telefone = str_replace(" ", "", $contato->telefone); @endphp
            <a href="tel:{{ $telefone }}" class="link-telefone">{{ $contato->telefone }}</a>
            <div class="endereco">{{ $contato->endereco }}</div>
            <div class="redes-sociais">
                <a href="{{ $contato->facebook }}" target="_blank" class="facebook"><img src="{{ asset('assets/img/layout/icone-facebook.svg') }}" alt="Facebook"></a>
                <a href="{{ $contato->linkedin }}" target="_blank" class="linkedin"><img src="{{ asset('assets/img/layout/icone-linkedin.svg') }}" alt="LinkedIn"></a>
                <a href="{{ $contato->instagram }}" target="_blank" class="instagram"><img src="{{ asset('assets/img/layout/icone-instagram.svg') }}" alt="Instagram"></a>
                <a href="{{ $contato->youtube }}" target="_blank" class="youtube"><img src="{{ asset('assets/img/layout/icone-youtube-branco.svg') }}" alt="YouTube"></a>
            </div>
        </div>

        <div class="parte-quatro">
            <h4 class="titulo">FALE CONOSCO</h4>
            <form action="{{ route('contato-footer.post') }}" method="POST" enctype="multipart/form-data">
                {!! csrf_field() !!}
                <input type="text" name="nome" placeholder="nome" value="{{ old('nome') }}" required>
                <input type="email" name="email" placeholder="e-mail" value="{{ old('email') }}" required>
                <input type="text" name="telefone" class="input-telefone" placeholder="telefone" value="{{ old('telefone') }}">
                <textarea name="mensagem" placeholder="mensagem" required>{{ old('mensagem') }}</textarea>
                <button type="submit" class="btn-enviar">ENVIAR <img src="{{ asset('assets/img/layout/setinha-fios-branca.svg') }}" alt="" class="img-seta"></button>
            </form>
        </div>

        <div class="parte-cinco">
            <a href="#" class="link-nav link-busca {{ strpos(url()->current(), 'busca') ? 'active' : '' }}">• BUSCA AVANÇADA</a>
            <a href="{{ route('politica-de-privacidade') }}" @if(Tools::routeIs('politica-de-privacidade*')) class="link-nav active" @endif class="link-nav">• POLÍTICA DE PRIVACIDADE</a>

            <div class="direitos-criacao">
                <p class="empresa">© {{ date('Y') }} {{ $config->title }}</p>
                <p class="direitos">Todos os direitos reservados</p>
                <a href="http://www.trupe.net" target="_blank" class="link-trupe">Criação de sites:</a>
                <a href="http://www.trupe.net" target="_blank" class="link-trupe">Trupe Agência Criativa</a>
            </div>
        </div>

    </div>
</footer>