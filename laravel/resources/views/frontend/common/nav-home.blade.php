<div class="itens-menu">
    <a href="{{ route('home') }}" class="link-nav">HOME</a>
    <a href="{{ route('produtos', ['linha_slug' => 'elevadores']) }}" class="link-nav">PRODUTOS</a>
    <a href="{{ route('orcamentos') }}" class="link-nav">ORÇAMENTO</a>
    <a href="{{ route('marcas') }}" class="link-nav">MARCAS</a>
    <a href="{{ route('empresa') }}" class="link-nav">EMPRESA</a>
    <a href="{{ route('novidades') }}" class="link-nav">NEWSLETTER</a>
    <a href="{{ route('contato') }}" class="link-nav">CONTATO</a>
    <div class="linha-vertical"></div>
    <a href="#" class="link-busca">BUSCA</a>
</div>