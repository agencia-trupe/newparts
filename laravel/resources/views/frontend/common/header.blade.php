@if(Tools::routeIs('home'))
<header class="home">
    @else
    <header>
        @endif

        @if(Tools::routeIs('home'))
        <div class="header-topo-home">
            <div class="centralizado">
                @php $telefone = str_replace(" ", "", $contato->telefone); @endphp
                <a href="tel:{{ $telefone }}" class="link-telefone">{{ str_replace("+55 ", "", $contato->telefone) }}</a>
                <a href="#" class="link-area-cliente">ÁREA DO CLIENTE</a>
                <div class="redes-sociais">
                    <a href="{{ $contato->facebook }}" target="_blank" class="facebook"><img src="{{ asset('assets/img/layout/icone-facebook-vinho.svg') }}" alt="Facebook"></a>
                    <a href="{{ $contato->linkedin }}" target="_blank" class="linkedin"><img src="{{ asset('assets/img/layout/icone-linkedin-vinho.svg') }}" alt="LinkedIn"></a>
                    <a href="{{ $contato->instagram }}" target="_blank" class="instagram"><img src="{{ asset('assets/img/layout/icone-instagram-vinho.svg') }}" alt="Instagram"></a>
                    <a href="{{ $contato->youtube }}" target="_blank" class="youtube"><img src="{{ asset('assets/img/layout/icone-youtube.svg') }}" alt="YouTube"></a>
                </div>
            </div>
        </div>
        <div class="header-home">
            <a href="{{ route('home') }}" class="logo-home" title="{{ $config->title }}"><img src="{{ asset('assets/img/layout/marca-newparts-branco.svg') }}" alt="{{ $config->title }}" class="img-logo-home"></a>
            <button id="mobile-toggle" type="button" role="button">
                <span class="lines"></span>
            </button>
            <nav>
                @include('frontend.common.nav-home')
            </nav>
        </div>
        @else
        <div class="header-topo">
            <div class="centralizado">
                @php $telefone = str_replace(" ", "", $contato->telefone); @endphp
                <a href="tel:{{ $telefone }}" class="link-telefone">{{ str_replace("+55 ", "", $contato->telefone) }}</a>
                <a href="#" class="link-area-cliente">ÁREA DO CLIENTE</a>
                <div class="redes-sociais">
                    <a href="{{ $contato->facebook }}" target="_blank" class="facebook"><img src="{{ asset('assets/img/layout/icone-facebook.svg') }}" alt="Facebook"></a>
                    <a href="{{ $contato->linkedin }}" target="_blank" class="linkedin"><img src="{{ asset('assets/img/layout/icone-linkedin.svg') }}" alt="LinkedIn"></a>
                    <a href="{{ $contato->instagram }}" target="_blank" class="instagram"><img src="{{ asset('assets/img/layout/icone-instagram.svg') }}" alt="Instagram"></a>
                    <a href="{{ $contato->youtube }}" target="_blank" class="youtube"><img src="{{ asset('assets/img/layout/icone-youtube-branco.svg') }}" alt="YouTube"></a>
                </div>
            </div>
        </div>
        <div class="header-paginas">
            <a href="{{ route('home') }}" class="logo-home" title="{{ $config->title }}"><img src="{{ asset('assets/img/layout/marca-newparts.svg') }}" alt="{{ $config->title }}" class="img-logo-home"></a>
            <button id="mobile-toggle" type="button" role="button">
                <span class="lines internas"></span>
            </button>
            <nav>
                @include('frontend.common.nav')
            </nav>

            <!-- SUBMENU LINHAS E LETRAS -->
            @php
            $letras = [];
            foreach ($categorias as $categoria) {
            $letras[] = mb_substr($categoria->slug, 0, 1);
            }
            $letrasLinhaElevadores = array_unique($letras);
            @endphp

            <div class="submenu-nav">
                <div class="linhas">
                    <a href="{{ route('produtos', ['linha_slug' => 'escadas-rolantes']) }}" class="linha {{ $currentLinha == 'escadas-rolantes' ? 'active' : '' }}" id="escadas-rolantes">ESCADAS ROLANTES</a>
                    <a href="{{ route('produtos', ['linha_slug' => 'elevadores']) }}" class="linha {{ $currentLinha == 'elevadores' ? 'active' : '' }}" id="elevadores">ELEVADORES</a>
                </div>
                <div class="categorias-elevadores">
                    <a href="{{ route('produtos.filtro', ['linha_slug' => 'elevadores', 'categoria_slug' => $botoeiras->slug]) }}" class="categoria {{ strpos(url()->current(), $botoeiras->slug) ? 'active' : '' }}" id="categoria{{ $botoeiras->id }}">Linha de Botoeiras</a>
                    <a href="{{ route('produtos.filtro', ['linha_slug' => 'elevadores', 'categoria_slug' => $cabines->slug]) }}" class="categoria {{ strpos(url()->current(), $cabines->slug) ? 'active' : '' }}" id="categoria{{ $cabines->id }}">Linha de Cabines</a>
                    <div class="letras">
                        @foreach($letrasLinhaElevadores as $letra)
                        <a href="{{ route('produtos.letra', ['elevadores', $letra]) }}" class="letra" id="letra-{{ $letra }}" slug="{{ $letra }}">{{ $letra }}</a>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        @endif

        <div class="busca-avancada" style="display: none;">
            <h2 class="titulo">BUSCA AVANÇADA
                <img src="{{ asset('assets/img/layout/icone-fechar.svg') }}" alt="" class="img-fechar">
            </h2>
            <form action="{{ route('busca') }}" method="POST" class="form-busca">
                <input type="hidden" name="linha_slug" value="{{ $currentLinha }}">
                <p class="tipo">por palavras</p>
                <input type="text" name="palavraChave" value="" placeholder="palavra-chave (nome do produto ou código)">
                <select name="marca" class="busca-marcas">
                    <option value="">Selecione o fabricante/marca (opcional)</option>
                    @foreach($marcas as $marca)
                    <option value="{{ $marca->id }}">{{ $marca->nome }}</option>
                    @endforeach
                </select>
                <button type="submit" class="btn-buscar">BUSCAR <img src="{{ asset('assets/img/layout/icone-busca.svg') }}" alt="" class="img-lupa"></button>
            </form>
            <div class="filtro">
                <p class="tipo">por categorias</p>
                <!-- SUBMENU LINHAS E LETRAS -->
                @php
                $letras = [];
                foreach ($categorias as $categoria) {
                $letras[] = mb_substr($categoria->slug, 0, 1);
                }
                $letrasLinhaElevadores = array_unique($letras);
                @endphp

                <div class="linhas-letras">
                    <div class="linhas">
                        <a href="{{ route('produtos', ['linha_slug' => 'escadas-rolantes']) }}" class="linha {{ $currentLinha == 'escadas-rolantes' ? 'active' : '' }}" id="escadas-rolantes">ESCADAS ROLANTES</a>
                        <a href="{{ route('produtos', ['linha_slug' => 'elevadores']) }}" class="linha {{ $currentLinha == 'elevadores' ? 'active' : '' }}" id="elevadores">ELEVADORES</a>
                    </div>
                    <div class="categorias-elevadores">
                        <a href="{{ route('produtos.filtro', ['linha_slug' => 'elevadores', 'categoria_slug' => $botoeiras->slug]) }}" class="categoria" id="categoria{{ $botoeiras->id }}">Linha de Botoeiras</a>
                        <a href="{{ route('produtos.filtro', ['linha_slug' => 'elevadores', 'categoria_slug' => $cabines->slug]) }}" class="categoria" id="categoria{{ $cabines->id }}">Linha de Cabines</a>
                        <div class="letras">
                            @foreach($letrasLinhaElevadores as $letra)
                            <div class="letra-resultado">
                                <a href="{{ route('getCategorias', [$currentLinha, $letra]) }}" class="letra" id="letra-{{ $letra }}">{{ $letra }}</a>
                                <div class="categorias-por-letra" id="letra-{{ $letra }}" style="display: none;"></div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </header>