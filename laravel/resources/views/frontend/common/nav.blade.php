<div class="itens-menu">
    <a href="{{ route('home') }}" @if(Tools::routeIs('home*')) class="link-nav active" @endif class="link-nav">HOME</a>

    <a href="{{ route('produtos', ['linha_slug' => 'elevadores']) }}" @if(Tools::routeIs('produtos*')) class="link-nav link-produtos active" @endif class="link-nav link-produtos" id="navProdutos">PRODUTOS</a>

    <a href="{{ route('orcamentos') }}" @if(Tools::routeIs('orcamentos*')) class="link-nav active" @endif class="link-nav">ORÇAMENTO</a>
    <a href="{{ route('marcas') }}" @if(Tools::routeIs('marcas*')) class="link-nav active" @endif class="link-nav">MARCAS</a>
    <a href="{{ route('empresa') }}" @if(Tools::routeIs('empresa*')) class="link-nav active" @endif class="link-nav">EMPRESA</a>
    <a href="{{ route('novidades') }}" @if(Tools::routeIs('novidades*')) class="link-nav active" @endif class="link-nav">NEWSLETTER</a>
    <a href="{{ route('contato') }}" @if(Tools::routeIs('contato*')) class="link-nav active" @endif class="link-nav">CONTATO</a>
    <div class="linha-vertical"></div>
    <a href="#" class="link-busca {{ strpos(url()->current(), 'busca') ? 'active' : '' }}">BUSCA</a>
</div>