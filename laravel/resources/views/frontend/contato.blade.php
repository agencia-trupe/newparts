@extends('frontend.common.template')

@section('content')

<section class="contato">
    <div class="centralizado">
        <div class="contatos">
            @php
            $telefone = str_replace(" ", "", $contato->telefone);
            @endphp
            <a href="tel:{{ $telefone }}" class="telefone">{{ $contato->telefone }}</a>
            <p class="endereco">{{ $contato->endereco }}</p>
        </div>
        <div class="fale-conosco">
            <h4 class="titulo">FALE CONOSCO</h4>
            <form action="{{ route('contato.post') }}" method="POST" enctype="multipart/form-data">
                {!! csrf_field() !!}
                <div class="dados">
                    <input type="text" name="nome" placeholder="nome" value="{{ old('nome') }}" required>
                    <input type="email" name="email" placeholder="e-mail" value="{{ old('email') }}" required>
                    <input type="text" name="telefone" class="input-telefone" placeholder="telefone" value="{{ old('telefone') }}">
                    <input type="text" name="empresa" placeholder="empresa" value="{{ old('empresa') }}">
                </div>
                <textarea name="mensagem" placeholder="mensagem" required>{{ old('mensagem') }}</textarea>
                <button type="submit" class="btn-contato"><img src="{{ asset('assets/img/layout/setinha-enviar-mensagem.svg') }}" alt=""></button>
            </form>
            @if($errors->any())
            <div class="flash flash-erro">
                @foreach($errors->all() as $error)
                {!! $error !!}<br>
                @endforeach
            </div>
            @endif

            @if(session('enviado'))
            <div class="flash flash-sucesso">
                <p>Mensagem enviada com sucesso!</p>
            </div>
            @endif
        </div>
    </div>

    <div class="contato-mapa">
        {!! $contato->google_maps !!}
    </div>

</section>

@endsection