@extends('frontend.common.template')

@section('content')

<section class="produtos-show">

    <div class="submenu-produtos">
        <a href="{{ route('produtos.marcas', [$produto->linha_slug, $produto->marca_slug]) }}" class="link-marca"><span>Marca: </span>{{ $produto->marca_nome }}</a>
        <a href="{{ route('produtos.filtro', ['linha_slug' => $produto->linha_slug, 'categoria_slug' => $produto->categoria_slug]) }}" class="link-categoria" id="categoria{{ $produto->categoria_id }}" slug="{{ $produto->categoria_slug }}"><span>Categoria: </span>{{ $produto->categoria_titulo }}</a>

        <div class="lista-marcas">
            <select name="marca" class="select-marcas">
                <option value="null" selected>Selecione o fabricante/marca (opcional)</option>
                <option value="todos">TODOS</option>
                @foreach($marcas as $marca)
                <option value="{{ $marca->slug }}" linha="{{ $produto->linha_slug }}">
                    {{ $marca->nome }}
                </option>
                @endforeach
            </select>
        </div>

        <form action="{{ route('produtos.busca', $produto->linha_slug) }}" method="get" class="busca-rapida">
            <input type="hidden" id="marcaSlug" name="marca_slug" value="">
            <input type="text" name="palavraChave" class="input-submenu" placeholder="BUSCA POR PALAVRA-CHAVE" value="{{ old('palavraChave') }}">
            <button type="submit" class="btn-busca"><img src="{{ asset('assets/img/layout/icone-busca-escuro.svg') }}" alt="" class="img-lupa"></button>
        </form>

        <p class="titulo-categorias">CATEGORIAS</p>
        <div class="lista-categorias">
            @foreach($categorias as $categoria)
            <a href="{{ route('produtos.filtro', ['linha_slug' => $produto->linha_slug, 'categoria_slug' => $categoria->slug]) }}" class="categoria" id="categoria{{$categoria->id}}" slug="{{$categoria->slug}}" linha="{{ $produto->linha_slug }}">{{ $categoria->titulo }}</a>
            @endforeach
        </div>

        <a href="{{ route('produtos.lancamentos', $produto->linha_slug) }}" class="lancamentos">LANÇAMENTOS <img src="{{ asset('assets/img/layout/setinha-fios.svg') }}" alt="" class="img-setinha"></a>
        <a href="{{ route('produtos.categorias', $produto->linha_slug) }}" class="todas-categorias">VER TODAS AS CATEGORIAS <img src="{{ asset('assets/img/layout/setinha-fios.svg') }}" alt="" class="img-setinha"></a>
    </div>

    <div id="detalhesProduto">
        <div id="dadosProduto">
            <div class="imagens">
                <div class="capa-produto">
                    <img src="{{ asset('assets/img/produtos/'.$produto->capa) }}" alt="{{ $produto->titulo }}" class="img-produto">
                </div>
                <div class="marca-dagua">
                    <img src="{{ asset('assets/img/layout/marca-newparts-cinza.svg') }}" alt="" class="img-marca">
                </div>
                <div class="variacoes-produtos">
                    @if($produto->variacao_01 || $produto->variacao_02 || $produto->variacao_03 || $produto->variacao_04 || $produto->variacao_05 || $produto->variacao_06 || $produto->variacao_07 || $produto->variacao_08 || $produto->variacao_09 || $produto->variacao_10)
                    <a href="{{ asset('assets/img/produtos/'.$produto->capa) }}" class="variacao active"><img src="{{ asset('assets/img/produtos/'.$produto->capa) }}" alt="{{ $produto->titulo }}" class="img-variacao"></a>
                    @endif
                    @if($produto->variacao_01)
                    <a href="{{ asset('assets/img/produtos/variacoes/'.$produto->variacao_01) }}" class="variacao"><img src="{{ asset('assets/img/produtos/variacoes/'.$produto->variacao_01) }}" alt="{{ $produto->titulo }}" class="img-variacao"></a>
                    @endif
                    @if($produto->variacao_02)
                    <a href="{{ asset('assets/img/produtos/variacoes/'.$produto->variacao_02) }}" class="variacao"><img src="{{ asset('assets/img/produtos/variacoes/'.$produto->variacao_02) }}" alt="{{ $produto->titulo }}" class="img-variacao"></a>
                    @endif
                    @if($produto->variacao_03)
                    <a href="{{ asset('assets/img/produtos/variacoes/'.$produto->variacao_03) }}" class="variacao"><img src="{{ asset('assets/img/produtos/variacoes/'.$produto->variacao_03) }}" alt="{{ $produto->titulo }}" class="img-variacao"></a>
                    @endif
                    @if($produto->variacao_04)
                    <a href="{{ asset('assets/img/produtos/variacoes/'.$produto->variacao_04) }}" class="variacao"><img src="{{ asset('assets/img/produtos/variacoes/'.$produto->variacao_04) }}" alt="{{ $produto->titulo }}" class="img-variacao"></a>
                    @endif
                    @if($produto->variacao_05)
                    <a href="{{ asset('assets/img/produtos/variacoes/'.$produto->variacao_05) }}" class="variacao"><img src="{{ asset('assets/img/produtos/variacoes/'.$produto->variacao_05) }}" alt="{{ $produto->titulo }}" class="img-variacao"></a>
                    @endif
                    @if($produto->variacao_06)
                    <a href="{{ asset('assets/img/produtos/variacoes/'.$produto->variacao_06) }}" class="variacao"><img src="{{ asset('assets/img/produtos/variacoes/'.$produto->variacao_06) }}" alt="{{ $produto->titulo }}" class="img-variacao"></a>
                    @endif
                    @if($produto->variacao_07)
                    <a href="{{ asset('assets/img/produtos/variacoes/'.$produto->variacao_07) }}" class="variacao"><img src="{{ asset('assets/img/produtos/variacoes/'.$produto->variacao_07) }}" alt="{{ $produto->titulo }}" class="img-variacao"></a>
                    @endif
                    @if($produto->variacao_08)
                    <a href="{{ asset('assets/img/produtos/variacoes/'.$produto->variacao_08) }}" class="variacao"><img src="{{ asset('assets/img/produtos/variacoes/'.$produto->variacao_08) }}" alt="{{ $produto->titulo }}" class="img-variacao"></a>
                    @endif
                    @if($produto->variacao_09)
                    <a href="{{ asset('assets/img/produtos/variacoes/'.$produto->variacao_09) }}" class="variacao"><img src="{{ asset('assets/img/produtos/variacoes/'.$produto->variacao_09) }}" alt="{{ $produto->titulo }}" class="img-variacao"></a>
                    @endif
                    @if($produto->variacao_10)
                    <a href="{{ asset('assets/img/produtos/variacoes/'.$produto->variacao_10) }}" class="variacao"><img src="{{ asset('assets/img/produtos/variacoes/'.$produto->variacao_10) }}" alt="{{ $produto->titulo }}" class="img-variacao"></a>
                    @endif
                </div>
            </div>

            <div class="sobre-produto">
                <p class="codigo">{{ $produto->codigo }}</p>
                <h2 class="titulo">{{ $produto->titulo }}</h2>
                <div class="descricao">{!! $produto->descricao !!}</div>
                <div class='marca'>
                    <img src="{{ asset('assets/img/marcas/'.$produto->marca_imagem) }}" alt="{{ $produto->marca_nome }}" class="img-marca">
                </div>
                @if($produto->imagem)
                <div class="desenho-tecnico">
                    <img src="{{ asset('assets/img/produtos/imagens/'.$produto->imagem) }}" alt="{{ $produto->titulo }}" class="img-desenho">
                </div>
                @endif
                @if($produto->video)
                <iframe class="video" src="{{ 'https://www.youtube.com/embed/'.$produto->video }}"></iframe>
                @endif
                @if($produto->arquivo)
                <a href="{{ route('produtos.pdf', $produto->slug) }}" target="_blank" class="link-download">
                    <div class="img-pdf">
                        <img src="{{ asset('assets/img/layout/icone-pdf.svg') }}" alt="">
                    </div>
                    DOWNLOAD DO MANUAL DO PRODUTO
                </a>
                @endif
            </div>
        </div>
        <div class="add-orcamento">
            <hr class="linha-add-orcamento">
            <a href="{{ route('orcamentos.adicionar', [$produto->linha_slug, $produto->slug]) }}" class="link-add-orcamento">
                <img src="{{ asset('assets/img/layout/icone-adicionar-orcamento.svg') }}" alt="" class="img-add">
                ADICIONAR ESTE PRODUTO AO PEDIDO DE ORÇAMENTO
            </a>
        </div>
    </div>

</section>



@endsection