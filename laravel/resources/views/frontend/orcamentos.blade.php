@extends('frontend.common.template')

@section('content')

<section class="pedido-de-orcamento">

    <div class="left">
        @if(auth('clientes')->check())
        <div class="logado">
            <h2 class="titulo">PEDIDO DE ORÇAMENTO</h2>
            <div class="cliente-logout">
                <p class="cliente">Olá <span class="nome">{{ auth('clientes')->user()->nome }}</span></p>
                <a href="{{ route('cliente.logout') }}" class="btn-sair">SAIR
                    <div class="img-sair"><img src="{{ asset('assets/img/layout/icone-fechar-branco.svg') }}" alt=""></div>
                </a>
            </div>
        </div>
        @else
        <div class="login">
            <h2 class="titulo">PEDIDO DE ORÇAMENTO</h2>
            <p class="frase">Faça o login no seu cadastro para encaminhar o pedido de orçamento.</p>
            <form action="{{ route('cliente.login') }}" method="POST" class="form-login">
                {!! csrf_field() !!}
                <div class="dados">
                    <input type="email" name="email" placeholder="login" value="{{ old('email') }}" required>
                    <input type="password" name="senha" placeholder="senha" value="{{ old('senha') }}" required>
                </div>
                <button type="submit" class="btn-login" id="btnLogin">ENTRAR</button>
            </form>
        </div>
        @endif

        @if(!auth('clientes')->check())
        <div class="cadastro">
            <h2 class="titulo">NOVO CADASTRO</h2>
            <p class="frase">Se você ainda não tem login, cadastre-se para enviar e acompanhar o seu pedido de orçamento.</p>
            <form action="{{ route('cliente.cadastro') }}" method="POST" class="form-cadastro">
                {!! csrf_field() !!}
                <div class="dados">
                    <input type="text" name="nome" placeholder="nome" value="{{ old('nome') }}" required>
                    <input type="email" name="email" placeholder="e-mail" value="{{ old('email') }}" required>
                    <input type="text" name="telefone" class="input-telefone" placeholder="telefone" value="{{ old('telefone') }}">
                    <input type="text" name="empresa" placeholder="empresa" value="{{ old('empresa') }}">
                    <input type="text" name="cnpj" class="input-cnpj" placeholder="cnpj" value="{{ old('cnpj') }}" required>
                    <input type="password" name="senha" placeholder="senha" value="{{ old('senha') }}" required>
                    <input type="password" name="senha_confirmation" placeholder="repetir senha" id="senha_confirmation" required>
                </div>
                <button type="submit" class="btn-cadastrar" id="btnCadastrar">CADASTRAR</button>
            </form>
            @if($errors->any())
            <div class="flash flash-erro">
                @foreach($errors->all() as $error)
                {!! $error !!}<br>
                @endforeach
            </div>
            @endif
        </div>
        @endif
    </div>

    <div class="right">
        @if($produtos)
        <div class="produtos-orcamento">
            @foreach($produtos as $produto)
            <div class="produto" id="{{$produto['slug']}}">
                <div class="img-produto">
                    <img src="{{ asset('assets/img/produtos/'.$produto['capa']) }}" alt="{{ $produto['titulo'] }}">
                </div>
                <div class="dados-produto">
                    <p class="codigo">{{ $produto['codigo'] }}</p>
                    <a href="{{ route('produtos.show', [$produto['linha_slug'], $produto['slug']]) }}" class="titulo">{{ $produto['titulo'] }}</a>
                </div>
                <div class="quantidade">
                    <span>QUANTIDADE</span>
                    <p class="numero" id="{{ $produto['slug'] }}">{{ $produto['quantidade'] }}</p>
                    <div class="btns">
                        <a href="{{ route('orcamentos.atualizar', $produto['slug']) }}" class="aumentar btn-aumentar-quantidade" id="{{$produto['slug']}}"><img src="{{ asset('assets/img/layout/seta-quantidade-subir.svg') }}" alt=""></a>
                        <a href="{{ route('orcamentos.atualizar', $produto['slug']) }}" class="diminuir btn-diminuir-quantidade" id="{{$produto['slug']}}"><img src="{{ asset('assets/img/layout/seta-quantidade-descer.svg') }}" alt=""></a>
                        <a href="{{ route('orcamentos.excluir', $produto['slug']) }}" class="excluir btn-excluir-item"><img src="{{ asset('assets/img/layout/icone-fechar.svg') }}" alt=""></a>
                    </div>
                </div>
            </div>
            @endforeach
            <div class="enviar">
                <hr class="linha-enviar-orcamento">
                @if(auth('clientes')->check())
                <a href="{{ route('orcamentos.post', auth('clientes')->user()->id) }}" class="link-enviar-orcamento">
                    <img src="{{ asset('assets/img/layout/icone-enviar-orcamento.svg') }}" alt="" class="img-enviar">
                    ENVIAR PEDIDO DE ORÇAMENTO
                </a>
                @else
                <a href="#" class="link-enviar-orcamento nao-logado">
                    <img src="{{ asset('assets/img/layout/icone-enviar-orcamento.svg') }}" alt="" class="img-enviar">
                    ENVIAR PEDIDO DE ORÇAMENTO
                </a>
                @endif
            </div>
        </div>
        @else
        <div class="texto-adicionar">
            <p>Você ainda não adicionou nenhum produto ao pedido de orçamento. Para adicionar produtos navegue ao menu PRODUTOS e clique no botão de <strong>ADICIONAR PRODUTO AO PEDIDO DE ORÇAMENTO.</strong></p>
            <p>Se preferir apenas faça seu cadastro e encaminhe o formulário ao lado com seus comentários para ser atendido pela equipe de vendas.</p>
        </div>
        @endif
        @if(session('success'))
        <div class="flash flash-sucesso">
            {!! session('success') !!}<br>
        </div>
        @endif
    </div>

</section>

@endsection