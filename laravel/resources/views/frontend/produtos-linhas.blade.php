@extends('frontend.common.template')

@section('content')

<section class="produtos-categorias grid-linhas">

    @if($linha->slug == 'elevadores')
    <a href="{{ route('produtos.filtro', ['linha_slug' => $linha->slug, 'categoria_slug' => $botoeiras->slug]) }}" class="linha-botoeiras categoria" id="categoria{{ $botoeiras->id }}">LINHA DE {{ $botoeiras->titulo }}</a>
    <a href="{{ route('produtos.filtro', ['linha_slug' => $linha->slug, 'categoria_slug' => $cabines->slug]) }}" class="linha-cabines categoria" id="categoria{{ $cabines->id }}">LINHA DE {{ $cabines->titulo }}</a>
    @endif

    @foreach($letrasCategorias as $letra)
    <div class="grupo-letra">
        <p class="letra">{{ $letra }}</p>

        @foreach($categorias as $categoria)

        @if (mb_substr($categoria->slug, 0, 1) == $letra)
        <a href="{{ route('produtos.filtro', ['linha_slug' => $linha->slug, 'categoria_slug' => $categoria->slug]) }}" class="categoria" id="categoria{{ $categoria->id }}" linha="{{ $linha->slug }}">{{ $categoria->titulo }}</a>
        @endif

        @endforeach
    </div>
    @endforeach
</section>

@endsection