@extends('frontend.common.template')

@section('content')

<section class="produtos">

    <div class="submenu-produtos">
        @if(isset($marcaFiltrada))
        <p class="marca-filtrada">Marca:<a href="{{ route('produtos.marcas', [$linha->slug, $marcaFiltrada->slug]) }}" class="link-marca" id="marca{{ $marcaFiltrada->id }}" slug="{{ $marcaFiltrada->slug }}">{{ $marcaFiltrada->nome }}</a></p>
        @endif

        @if(isset($categoriaFiltrada))
        <p class="categoria-filtrada">Categoria:<a href="{{ route('produtos.filtro', ['linha_slug' => $linha->slug, 'categoria_slug' => $categoriaFiltrada->slug]) }}" class="link-categoria" id="categoria{{ $categoriaFiltrada->id }}" slug="{{ $categoriaFiltrada->slug }}">{{ $categoriaFiltrada->titulo }}</a></p>
        @endif

        @if(isset($palavra))
        <p class="palavra-filtrada">Palavra-chave:<a href="#" class="palavra">{{ $palavra }}</a></p>
        @endif

        <div class="lista-marcas">
            <select name="marca" class="select-marcas">
                <option value="null" selected>Selecione o fabricante/marca (opcional)</option>
                <option value="todos">TODOS</option>
                @foreach($marcas as $marca)
                <option value="{{ $marca->slug }}" linha="{{ $linha->slug }}">
                    {{ $marca->nome }}
                </option>
                @endforeach
            </select>
        </div>

        <form action="{{ route('produtos.busca', $linha->slug) }}" method="get" class="busca-rapida">
            <input type="hidden" id="marcaSlug" name="marca_slug" value="">
            <input type="text" name="palavraChave" class="input-submenu" placeholder="BUSCA POR PALAVRA-CHAVE" value="{{ old('palavraChave') }}">
            <button type="submit" class="btn-busca"><img src="{{ asset('assets/img/layout/icone-busca-escuro.svg') }}" alt="" class="img-lupa"></button>
        </form>

        <p class="titulo-categorias">CATEGORIAS</p>
        <div class="lista-categorias">
            @foreach($categorias as $categoria)
            <a href="{{ route('produtos.filtro', ['linha_slug' => $linha->slug, 'categoria_slug' => $categoria->slug]) }}" class="categoria" id="categoria{{$categoria->id}}" slug="{{$categoria->slug}}" linha="{{ $linha->slug }}">{{ $categoria->titulo }}</a>
            @endforeach
        </div>

        <a href="{{ route('produtos.lancamentos', $linha->slug) }}" class="lancamentos">LANÇAMENTOS <img src="{{ asset('assets/img/layout/setinha-fios.svg') }}" alt="" class="img-setinha"></a>
        <a href="{{ route('produtos.categorias', $linha->slug) }}" class="todas-categorias">VER TODAS AS CATEGORIAS <img src="{{ asset('assets/img/layout/setinha-fios.svg') }}" alt="" class="img-setinha"></a>
    </div>

    <div class="resultados-produtos" id="resultadosProdutos">
        @if(count($produtos) > 0)
        @foreach($produtos as $produto)
        <a href="{{ route('produtos.show', [$linha->slug, $produto->slug]) }}" class="link-produto">
            <div class="imagens">
                <img src="{{ asset('assets/img/produtos/'.$produto->capa) }}" alt="{{ $produto->titulo }}" class="img-produto">
                <img src="{{ asset('assets/img/layout/marca-newparts-cinza.svg') }}" alt="" class="img-logo">
            </div>
            <div class="marca">
                <img src="{{ asset('assets/img/marcas/'.$produto->marca_imagem) }}" alt="{{ $produto->marca_nome }}" class="img-marca">
            </div>
            <p class="codigo">[{{ $produto->codigo }}]</p>
            <p class="titulo">{{ $produto->titulo }}</p>
        </a>
        @endforeach
        @else
        <p class="nenhum-resultado">Nenhum resultado encontrado</p>
        @endif

        @if(!Tools::routeIs('produtos.busca'))
        <div class="btns-filtro-linhas">
            <a href="{{ route('produtos.categorias', $linha->slug) }}" class="btn-anterior" linha="{{ $linha->slug }}">
                <img src="{{ asset('assets/img/layout/setinha-fios.svg') }}" alt="" class="img-setinha-anterior">
                <p class="descricao">Ver categoria anterior:
                    <span class="nome-categoria"></span>
                </p>
            </a>
            <a href="" class="btn-topo"><img src="{{ asset('assets/img/layout/setinha-fios.svg') }}" alt="" class="img-setinha-topo">TOPO</a>
            <a href="{{ route('produtos.categorias', $linha->slug) }}" class="btn-proximo" linha="{{ $linha->slug }}">
                <p class="descricao">
                    Ver próxima categoria: <span class="nome-categoria"></span>
                </p>
                <img src="{{ asset('assets/img/layout/setinha-fios.svg') }}" alt="" class="img-setinha-proximo">
            </a>
        </div>
        @endif
    </div>

</section>



@endsection