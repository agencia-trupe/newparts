<!DOCTYPE html>
<html>

<head>
    <title>[ORÇAMENTO] {{ config('app.name') }}</title>
    <meta charset="utf-8">
</head>

<body>
    <span style='font-weight:bold;font-size:18px;font-family:Verdana;color:#A03335;'>Dados do cliente</span><br>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;color:#373435;'>Nome:</span> <span style='color:#000;font-size:14px;font-family:Verdana;color:#373435;'>{{ $cliente->nome }}</span><br>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;color:#373435;'>E-mail:</span> <span style='color:#000;font-size:14px;font-family:Verdana;color:#373435;'>{{ $cliente->email }}</span><br>
    @if($cliente->telefone)
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;color:#373435;'>Telefone:</span> <span style='color:#000;font-size:14px;font-family:Verdana;color:#373435;'>{{ $cliente->telefone }}</span><br>
    @endif
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;color:#373435;'>Empresa:</span> <span style='color:#000;font-size:14px;font-family:Verdana;color:#373435;'>{{ $cliente->empresa }}</span><br>
    @if($cliente->cnpj)
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;color:#373435;'>CNPJ:</span> <span style='color:#000;font-size:14px;font-family:Verdana;color:#373435;'>{{ $cliente->cnpj }}</span><br>
    @endif

    <br><span style='font-weight:bold;font-size:18px;font-family:Verdana;color:#A03335;'>Produtos</span><br>
    @foreach($produtos as $produto)
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;color:#373435;'>Código:</span> <span style='color:#000;font-size:14px;font-family:Verdana;color:#373435;'>{{ $produto['codigo'] }}</span><br>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;color:#373435;'>Título:</span> <span style='color:#000;font-size:14px;font-family:Verdana;color:#373435;'>{{ $produto['titulo'] }}</span><br>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;color:#373435;'>Quantidade:</span> <span style='color:#000;font-size:14px;font-family:Verdana;color:#373435;'>{{ $produto['quantidade'] }}</span><br>
    <hr>
    @endforeach
</body>

</html>