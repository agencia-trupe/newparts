<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateCategoriasNovidadesTable extends Migration
{
    public function up()
    {
        Schema::create('categorias_novidades', function (Blueprint $table) {
            $table->increments('id');
            $table->string('slug');
            $table->string('titulo');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('categorias_novidades');
    }
}
