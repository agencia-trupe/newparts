<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateContatosOrcamentosTable extends Migration
{
    public function up()
    {
        Schema::create('contatos_orcamentos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('contatos_orcamentos');
    }
}
