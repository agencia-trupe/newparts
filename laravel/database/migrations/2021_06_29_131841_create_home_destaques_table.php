<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateHomeDestaquesTable extends Migration
{
    public function up()
    {
        Schema::create('home_destaques', function (Blueprint $table) {
            $table->increments('id');
            $table->string('imagem_1');
            $table->string('titulo_1');
            $table->string('link_1');
            $table->string('imagem_2');
            $table->string('titulo_2');
            $table->string('link_2');
            $table->string('imagem_3');
            $table->string('titulo_3');
            $table->string('link_3');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('home_destaques');
    }
}
