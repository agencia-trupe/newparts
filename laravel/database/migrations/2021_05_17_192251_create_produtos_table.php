<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateProdutosTable extends Migration
{
    public function up()
    {
        Schema::create('produtos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('linha_id')->unsigned();
            $table->foreign('linha_id')->references('id')->on('linhas_produtos')->onDelete('cascade');
            $table->string('codigo');
            $table->string('slug');
            $table->string('titulo');
            $table->text('descricao');
            $table->integer('marca_id')->unsigned();
            $table->foreign('marca_id')->references('id')->on('marcas')->onDelete('cascade');
            $table->string('capa');
            $table->string('imagem')->nullable();
            $table->string('video')->nullable();
            $table->string('arquivo')->nullable();
            $table->boolean('vitrine_home')->default(true);
            $table->boolean('vitrine_produtos')->default(false);
            $table->boolean('lancamento')->default(false);
            $table->string('variacao_01')->nullable();
            $table->string('variacao_02')->nullable();
            $table->string('variacao_03')->nullable();
            $table->string('variacao_04')->nullable();
            $table->string('variacao_05')->nullable();
            $table->string('variacao_06')->nullable();
            $table->string('variacao_07')->nullable();
            $table->string('variacao_08')->nullable();
            $table->string('variacao_09')->nullable();
            $table->string('variacao_10')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('produtos');
    }
}
