<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateNovidadesTable extends Migration
{

    public function up()
    {
        Schema::create('novidades', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('categoria_id')->unsigned();
            $table->foreign('categoria_id')->references('id')->on('categorias_novidades')->onDelete('cascade');
            $table->date('data');
            $table->string('slug');
            $table->string('titulo');
            $table->text('texto');
            $table->string('capa');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('novidades');
    }
}
