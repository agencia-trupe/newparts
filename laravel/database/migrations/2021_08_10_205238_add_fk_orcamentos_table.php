<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
Use Illuminate\Support\Facades\Schema;

class AddFkOrcamentosTable extends Migration
{
    public function up()
    {
        Schema::table('orcamentos', function (Blueprint $table) {
            $table->integer('contato_id')->unsigned();
            $table->foreign('contato_id')->references('id')->on('contatos_orcamentos')->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::table('orcamentos', function (Blueprint $table) {
            $table->dropForeign('orcamentos_contato_id_foreign');
            $table->dropColumn('contato_id');
        });
    }
}
