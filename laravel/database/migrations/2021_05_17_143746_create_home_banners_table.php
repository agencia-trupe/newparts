<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateHomeBannersTable extends Migration
{
    public function up()
    {
        Schema::create('home_banners', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('imagem');
            $table->string('cx_titulo');
            $table->string('cx_subtitulo');
            $table->string('cx_link_btn');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('home_banners');
    }
}
