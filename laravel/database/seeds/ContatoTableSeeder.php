<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ContatoTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('contato')->insert([
            'nome'        => 'New Parts',
            'email'       => 'contato@trupe.net',
            'telefone'    => '+55 11 2076 8458',
            'endereco'    => 'Rua Dom Joaquim de Melo, 317 Parque da Mooca, São Paulo - SP 03122-050',
            'instagram'   => 'https://www.instagram.com/_newparts/',
            'facebook'    => 'https://www.facebook.com/npelev',
            'linkedin'    => 'https://www.linkedin.com/company/new-parts-elevator/',
            'youtube'     => 'https://www.youtube.com/channel/UCZSZL2105fE4rMSxDKqr-FA',
            'google_maps' => '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3657.0310172250843!2d-46.60031348440672!3d-23.567329467699246!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce5953d8861659%3A0x7ec5f15f7258ffd0!2sR.%20Dom%20Joaquim%20de%20Melo%2C%20317%20-%20Parque%20da%20Mooca%2C%20S%C3%A3o%20Paulo%20-%20SP%2C%2003122-050!5e0!3m2!1spt-BR!2sbr!4v1621259944113!5m2!1spt-BR!2sbr" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>',
        ]);
    }
}
