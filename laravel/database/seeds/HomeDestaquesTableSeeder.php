<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class HomeDestaquesTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('home_destaques')->insert([
            'imagem_1' => '',
            'titulo_1' => 'Linha de Botoeiras',
            'link_1'   => '',
            'imagem_2' => '',
            'titulo_2' => 'Linha de Cabines',
            'link_2'   => '',
            'imagem_3' => '',
            'titulo_3' => 'Todos os Produtos',
            'link_3'   => '',
        ]);
    }
}
