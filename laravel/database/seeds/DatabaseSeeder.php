<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(ConfiguracoesTableSeeder::class);
        $this->call(PoliticaDePrivacidadeTableSeeder::class);
        $this->call(ContatoTableSeeder::class);
        $this->call(ClientesTableSeeder::class);
        $this->call(EmpresaTableSeeder::class);
        $this->call(HomeDestaquesTableSeeder::class);
        $this->call(ContatosOrcamentosTableSeeder::class);
        $this->call(LinhasTableSeeder::class);
    }
}
