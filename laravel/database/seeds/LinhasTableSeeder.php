<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LinhasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('linhas')->insert([
            'id'     => 1,
            'slug' => 'elevadores',
            'titulo' => 'Elevadores',
        ]);

        DB::table('linhas')->insert([
            'id'     => 2,
            'slug' => 'escadas-rolantes',
            'titulo' => 'Escadas Rolantes',
        ]);
    }
}
