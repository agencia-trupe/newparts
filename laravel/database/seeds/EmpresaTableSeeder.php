<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EmpresaTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('empresa')->insert([
            'capa'    => '',
            'texto'   => '',
            'missao'  => '',
            'visao'   => '',
            'valores' => '',
        ]);
    }
}
