<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ContatosOrcamentosTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('contatos_orcamentos')->insert([
            'id'    => 1,
            'email' => 'alexandre@npelev.com.br',
        ]);

        DB::table('contatos_orcamentos')->insert([
            'id'    => 2,
            'email' => 'ivo@npelev.com.br',
        ]);

        DB::table('contatos_orcamentos')->insert([
            'id'    => 3,
            'email' => 's.abe@npelev.com.br',
        ]);
    }
}
