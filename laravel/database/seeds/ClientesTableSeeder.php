<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ClientesTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('clientes')->insert([
            'nome'     => 'trupe',
            'email'    => 'contato@trupe.net',
            'telefone' => '11 5555 5555',
            'empresa'  => 'Trupe',
            'senha'    => bcrypt('senhatrupe'),
        ]);
    }
}
