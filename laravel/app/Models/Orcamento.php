<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Orcamento extends Model
{
    protected $table = 'orcamentos';

    protected $guarded = ['id'];

    protected $casts = [
        'itens' => 'array'
    ];

    public function getCreatedAtAttribute($date)
    {
        return \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('d/m/Y');
    }

    public function getCreatedAtOrderAttribute()
    {
        return \Carbon\Carbon::createFromFormat('d/m/Y', $this->created_at)->format('Y-m-d');
    }

    public function scopeNaoLidos($query)
    {
        return $query->where('lido', '!=', 1);
    }

    public function countNaoLidos()
    {
        return $this->naoLidos()->count();
    }

    public function scopeCliente($query, $id)
    {
        return $query->where('cliente_id', $id);
    }

    public function produtos()
    {
        return $this->hasMany('App\Models\OrcamentoProduto', 'orcamento_id');
    }
}
