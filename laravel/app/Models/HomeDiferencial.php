<?php

namespace App\Models;

use App\Helpers\CropImage;
use App\Helpers\CropImageTinify;
use Illuminate\Database\Eloquent\Model;

class HomeDiferencial extends Model
{
    protected $table = 'home_diferenciais';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public static function upload_imagem()
    {
        return CropImageTinify::make('imagem', [
            'width'  => 80,
            'height' => null,
            'path'   => 'assets/img/home/diferenciais/'
        ]);
    }
}
