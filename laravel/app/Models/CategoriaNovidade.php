<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CategoriaNovidade extends Model
{
    protected $table = 'categorias_novidades';

    protected $guarded = ['id'];
}
