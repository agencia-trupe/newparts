<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ContatoOrcamento extends Model
{
    protected $table = 'contatos_orcamentos';

    protected $guarded = ['id'];
}
