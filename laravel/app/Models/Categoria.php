<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Categoria extends Model
{
    protected $table = 'categorias';

    protected $guarded = ['id'];

    public function scopeLinha($query, $id)
    {
        return $query->where('linha_id', $id);
    }
}
