<?php

namespace App\Models;

use App\Helpers\CropImage;
use App\Helpers\CropImageTinify;
use Illuminate\Database\Eloquent\Model;

class Novidade extends Model
{
    protected $table = 'novidades';

    protected $guarded = ['id'];

    public function getCreatedAtAttribute($data)
    {
        return \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $data)->format('d/m/Y');
    }

    public function scopeCategoria($query, $id)
    {
        return $query->where('categoria_id', $id);
    }

    public static function upload_capa()
    {
        return CropImageTinify::make('capa', [
            [
                'width'  => 250,
                'height' => 250,
                'path'   => 'assets/img/novidades/miniaturas/'
            ],
            [
                'width'  => 340,
                'height' => 340,
                'path'   => 'assets/img/novidades/destaques/'
            ],
            [
                'width'  => 600,
                'height' => null,
                'path'   => 'assets/img/novidades/capas/'
            ]
        ]);
    }
}
