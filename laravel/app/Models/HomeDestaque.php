<?php

namespace App\Models;

use App\Helpers\CropImage;
use App\Helpers\CropImageTinify;
use Illuminate\Database\Eloquent\Model;

class HomeDestaque extends Model
{
    protected $table = 'home_destaques';

    protected $guarded = ['id'];

    public static function upload_imagem_1()
    {
        return CropImageTinify::make('imagem_1', [
            'width'  => 420,
            'height' => null,
            'path'   => 'assets/img/home/destaques/'
        ]);
    }

    public static function upload_imagem_2()
    {
        return CropImageTinify::make('imagem_2', [
            'width'  => 420,
            'height' => null,
            'path'   => 'assets/img/home/destaques/'
        ]);
    }

    public static function upload_imagem_3()
    {
        return CropImageTinify::make('imagem_3', [
            'width'  => 420,
            'height' => null,
            'path'   => 'assets/img/home/destaques/'
        ]);
    }
}
