<?php

namespace App\Models;

use App\Helpers\CropImage;
use Illuminate\Database\Eloquent\Model;

class Produto extends Model
{
    protected $table = 'produtos';

    protected $guarded = ['id'];

    public function scopeCategoria($query, $id)
    {
        return $query->where('categoria_id', $id);
    }

    public function scopeMarca($query, $id)
    {
        return $query->where('marca_id', $id);
    }

    public function scopeHome($query)
    {
        return $query->where('vitrine_home', '=', 1);
    }

    public function scopeProdutos($query)
    {
        return $query->where('vitrine_produtos', '=', 1);
    }

    public function scopeLancamentos($query)
    {
        return $query->where('lancamento', '=', 1);
    }


    public static function upload_capa()
    {
        return CropImage::make('capa', [
            'width'  => 400,
            'height' => null,
            'path'   => 'assets/img/produtos/'
        ]);
    }

    public static function upload_imagem()
    {
        return CropImage::make('imagem', [
            'width'  => 600,
            'height' => null,
            'path'   => 'assets/img/produtos/imagens/'
        ]);
    }

    public static function upload_variacao_01()
    {
        return CropImage::make('variacao_01', [
            'width'  => 400,
            'height' => null,
            'path'   => 'assets/img/produtos/variacoes/'
        ]);
    }

    public static function upload_variacao_02()
    {
        return CropImage::make('variacao_02', [
            'width'  => 400,
            'height' => null,
            'path'   => 'assets/img/produtos/variacoes/'
        ]);
    }

    public static function upload_variacao_03()
    {
        return CropImage::make('variacao_03', [
            'width'  => 400,
            'height' => null,
            'path'   => 'assets/img/produtos/variacoes/'
        ]);
    }

    public static function upload_variacao_04()
    {
        return CropImage::make('variacao_04', [
            'width'  => 400,
            'height' => null,
            'path'   => 'assets/img/produtos/variacoes/'
        ]);
    }

    public static function upload_variacao_05()
    {
        return CropImage::make('variacao_05', [
            'width'  => 400,
            'height' => null,
            'path'   => 'assets/img/produtos/variacoes/'
        ]);
    }

    public static function upload_variacao_06()
    {
        return CropImage::make('variacao_06', [
            'width'  => 400,
            'height' => null,
            'path'   => 'assets/img/produtos/variacoes/'
        ]);
    }

    public static function upload_variacao_07()
    {
        return CropImage::make('variacao_07', [
            'width'  => 400,
            'height' => null,
            'path'   => 'assets/img/produtos/variacoes/'
        ]);
    }

    public static function upload_variacao_08()
    {
        return CropImage::make('variacao_08', [
            'width'  => 400,
            'height' => null,
            'path'   => 'assets/img/produtos/variacoes/'
        ]);
    }

    public static function upload_variacao_09()
    {
        return CropImage::make('variacao_09', [
            'width'  => 400,
            'height' => null,
            'path'   => 'assets/img/produtos/variacoes/'
        ]);
    }

    public static function upload_variacao_10()
    {
        return CropImage::make('variacao_10', [
            'width'  => 400,
            'height' => null,
            'path'   => 'assets/img/produtos/variacoes/'
        ]);
    }
}
