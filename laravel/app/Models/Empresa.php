<?php

namespace App\Models;

use App\Helpers\CropImage;
use App\Helpers\CropImageTinify;
use Illuminate\Database\Eloquent\Model;

class Empresa extends Model
{
    protected $table = 'empresa';

    protected $guarded = ['id'];

    public static function upload_capa()
    {
        return CropImageTinify::make(
            'capa',
            [
                'width'  => 635,
                'height' => null,
                'path'   => 'assets/img/empresa/'
            ]
        );
    }
}
