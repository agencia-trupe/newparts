<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Routing\Router;

class RouteServiceProvider extends ServiceProvider
{
    protected $namespace = 'App\Http\Controllers';

    public function boot(Router $router)
    {
        $router->model('orcamentos', 'App\Models\Orcamento');
        $router->model('novidades', 'App\Models\Novidade');
        $router->model('categorias_novidades', 'App\Models\CategoriaNovidade');
        $router->model('empresa_diferenciais', 'App\Models\EmpresaDiferencial');
        $router->model('empresa_imagens', 'App\Models\EmpresaImagem');
        $router->model('empresa', 'App\Models\Empresa');
        $router->model('clientes', 'App\Models\Cliente');
        $router->model('produtos', 'App\Models\Produto');
        $router->model('marcas', 'App\Models\Marca');
        $router->model('linhas', 'App\Models\Linha');
        $router->model('categorias', 'App\Models\Categoria');
        $router->model('home_diferenciais', 'App\Models\HomeDiferencial');
        $router->model('home_destaques', 'App\Models\HomeDestaque');
        $router->model('home_banners', 'App\Models\HomeBanner');
        $router->model('newsletter', 'App\Models\Newsletter');
        $router->model('contatos_recebidos', 'App\Models\ContatoRecebido');
        $router->model('contato', 'App\Models\Contato');
        $router->model('politica_de_privacidade', 'App\Models\PoliticaDePrivacidade');
        $router->model('configuracoes', 'App\Models\Configuracao');
        $router->model('usuarios', 'App\Models\User');
        $router->model('aceite-de-cookies', 'App\Models\AceiteDeCookies');

        parent::boot($router);
    }

    public function map(Router $router)
    {
        $router->group(['namespace' => $this->namespace], function ($router) {
            require app_path('Http/routes.php');
        });
    }
}
