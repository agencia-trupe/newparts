<?php

namespace App\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    public function boot()
    {
        view()->composer('*', function ($view) {
            $view->with('config', \App\Models\Configuracao::first());
        });

        view()->composer('frontend.common.*', function ($view) {
            $view->with('contato', \App\Models\Contato::first());
            $view->with('linhas', \App\Models\Linha::orderBy('id', 'desc')->get());
            $view->with('categorias', \App\Models\Categoria::where('linha_id', 1)->where('slug', '!=', 'botoeiras')->where('slug', '!=', 'cabines')->orderBy('titulo', 'asc')->get());
            $view->with('marcas', \App\Models\Marca::orderBy('nome', 'asc')->get());
        });

        view()->composer('frontend.*', function ($view) {
            $view->with('botoeiras', \App\Models\Categoria::where('slug', 'botoeiras')->first());
            $view->with('cabines', \App\Models\Categoria::where('slug', 'cabines')->first());
            $currentLinha = is_null(request()->linha) ? 'elevadores' : request()->linha;
            $view->with('currentLinha', $currentLinha);
        });

        view()->composer('painel.common.*', function ($view) {
            $view->with('contatosNaoLidos', \App\Models\ContatoRecebido::naoLidos()->count());
            $view->with('orcamentosNaoLidos', \App\Models\Orcamento::naoLidos()->count());
        });

    }

    public function register()
    {
        //
    }
}
