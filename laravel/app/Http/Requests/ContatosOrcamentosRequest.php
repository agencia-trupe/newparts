<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ContatosOrcamentosRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'email' => 'required|email',
        ];
    }

    public function messages()
    {
        return [
            'required' => "Preencha todos os campos corretamente.",
            'email'    => "Insira um endereço de e-mail válido.",
        ];
    }
}
