<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class HomeDestaquesRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'imagem_1' => 'required|image',
            'titulo_1' => 'required',
            'link_1'   => 'required',
            'imagem_2' => 'required|image',
            'titulo_2' => 'required',
            'link_2'   => 'required',
            'imagem_3' => 'required|image',
            'titulo_3' => 'required',
            'link_3'   => 'required',
        ];

        if ($this->method() != 'POST') {
            $rules['imagem_1'] = 'image';
            $rules['imagem_2'] = 'image';
            $rules['imagem_3'] = 'image';
        }

        return $rules;
    }

    public function messages()
    {
        return [
            'required' => "Preencha todos os campos corretamente.",
        ];
    }
}
