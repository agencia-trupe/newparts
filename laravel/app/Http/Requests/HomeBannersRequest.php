<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class HomeBannersRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'imagem'       => 'required|image',
            'cx_titulo'    => 'required',
            'cx_subtitulo' => 'required',
            'cx_link_btn'  => 'required',
        ];

        if ($this->method() != 'POST') {
            $rules['imagem'] = 'image';
        }

        return $rules;
    }

    public function messages()
    {
        return [
            'required' => "Preencha todos os campos corretamente.",
        ];
    }
}
