<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ClientesRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'nome'  => 'required|max:255',
            'cnpj'  => 'required',
            'email' => 'required|email|max:255|unique:clientes',
            'senha' => 'required|confirmed|min:6',
        ];

        if ($this->method() != 'POST') {
            $rules['email'] = 'required|email|max:255|unique:clientes,email,' . $this->route('clientes')->id;
            $rules['senha'] = 'confirmed|min:6';
        }

        return $rules;
    }

    public function messages()
    {
        return [
            'required' => 'Preencha todos os campos corretamente',
            'unique'   => 'Este e-mail já existe.',
            'confirmed' => 'As senhas devem ser iguais.',
            'min' => 'A senha deve ter no mínimo 6 caracteres.'
        ];
    }
}
