<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ProdutosRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'codigo'    => 'required',
            'titulo'    => 'required',
            'descricao' => 'required',
            'capa'      => 'required|image',
            'imagem'    => 'image',
        ];

        if ($this->method() != 'POST') {
            $rules['capa']   = 'image';
            $rules['imagem'] = 'image';
        }

        return $rules;
    }

    public function messages()
    {
        return [
            'required' => "Preencha todos os campos corretamente.",
        ];
    }
}
