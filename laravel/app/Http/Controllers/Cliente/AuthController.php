<?php

namespace App\Http\Controllers\Cliente;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\ClientesRequest;
use App\Models\Cliente;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    protected function login(Request $request)
    {
        if (Auth::guard('clientes')->attempt([
            'email'    => $request->get('email'),
            'password' => $request->get('senha')
        ])) {
            $notification = array(
                'message' => 'Usuario logado com sucesso!',
                'alert-type' => 'success'
            );
            return redirect()->back()->with($notification);
        } else {
            $notification = array(
                'message' => 'Erro ao logar, e-mail ou senha inválidos',
                'alert-type' => 'error'
            );
            return redirect()->back()->with($notification);
        }
    }

    protected function logout()
    {
        auth('clientes')->logout();

        $notification = array(
            'message' => 'Usuário deslogado com sucesso!',
            'alert-type' => 'success'
        );
        return redirect()->route('home')->with($notification);
    }

    protected function create(ClientesRequest $request)
    {
        try {
            $input = $request->except('senha_confirmation');
            $input['senha'] = bcrypt($request->get('senha'));

            $usuario = Cliente::create($input);

            Auth::guard('clientes')->loginUsingId($usuario->id);

            return redirect()->route('orcamentos');
        } catch (\Exception $e) {

            return redirect()->back()->withErrors(['Erro ao criar cadastro: '.$e->getMessage()]);
        }
    }
}
