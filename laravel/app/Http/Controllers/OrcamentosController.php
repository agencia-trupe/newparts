<?php

namespace App\Http\Controllers;

use App\Models\Cliente;
use App\Models\Contato;
use App\Models\ContatoOrcamento;
use App\Models\Linha;
use App\Models\Orcamento;
use App\Models\Produto;
use Illuminate\Support\Facades\Mail;

class OrcamentosController extends Controller
{
    public function index()
    {
        // session()->forget('orcamento');
        $produtos = session()->has('orcamento') ? session()->get('orcamento') : [];

        return view('frontend.orcamentos', compact('produtos'));
    }

    public function adicionar($linha_slug, $produto_slug)
    {
        $produto = Produto::where('slug', $produto_slug)->first();

        if (!$produto) {
            return response()->json('Produto não encontrado.', 500);
        }

        $orcamento = session()->get('orcamento');

        // se o orcamento está vazio, primeiro produto a ser incluso:
        if (!$orcamento) {
            $orcamento = [
                $produto_slug => [
                    'id' => $produto->id,
                    'linha_slug' => $linha_slug,
                    'slug' => $produto->slug,
                    'titulo' => $produto->titulo,
                    'capa' => $produto->capa,
                    'codigo' => $produto->codigo,
                    'quantidade' => 1
                ]
            ];
            session()->put('orcamento', $orcamento);

            $quantidadeTotal = 0;
            foreach (session()->get('orcamento') as $orcamento) {
                $quantidadeTotal += $orcamento['quantidade'];
            }

            return response()->json($quantidadeTotal);
        };

        // se o orcamento NÃO está vazio, verifica se o produto existe e adiciona na quantidade:
        if (isset($orcamento[$produto_slug])) {
            $orcamento[$produto_slug]['quantidade']++;
            session()->put('orcamento', $orcamento);

            $quantidadeTotal = 0;
            foreach (session()->get('orcamento') as $orcamento) {
                $quantidadeTotal += $orcamento['quantidade'];
            }

            return response()->json($quantidadeTotal);
        };

        // se o produto não existe no orcamento, adiciona o produto com quantidade = 1:
        $orcamento[$produto_slug] = [
            'id' => $produto->id,
            'linha_slug' => $linha_slug,
            'slug' => $produto->slug,
            'titulo' => $produto->titulo,
            'capa' => $produto->capa,
            'codigo' => $produto->codigo,
            'quantidade' => 1
        ];

        session()->put('orcamento', $orcamento);

        $quantidadeTotal = 0;
        foreach (session()->get('orcamento') as $orcamento) {
            $quantidadeTotal += $orcamento['quantidade'];
        }

        return response()->json($quantidadeTotal);
    }

    public function atualizar($produto_slug)
    {
        $quantidade = $_GET['quantidade'];

        $produtos = session('orcamento');
        foreach ($produtos as $key => $value) {
            if ($value['slug'] == $produto_slug) {
                // dd($produtos[$key]['quantidade']);
                $produtos[$key]['quantidade'] = $quantidade;
            }
        }
        session()->put('orcamento', $produtos);

        return response()->json(['produto' => $produto_slug]);
    }

    public function excluir($produto_slug)
    {
        $produtos = session('orcamento');
        foreach ($produtos as $key => $value) {
            if ($value['slug'] == $produto_slug) {
                unset($produtos[$key]);
            }
        }
        session()->put('orcamento', $produtos);

        return redirect()->back();
    }

    public function post($cliente_id)
    {
        $cliente = Cliente::where('id', $cliente_id)->first();
        $produtos = session('orcamento');

        $anterior = Orcamento::orderBy('id', 'desc')->first();
        if (isset($anterior)) {
            $proximo = ContatoOrcamento::where('id', '>', $anterior->contato_id)->first();
            if (isset($proximo)) {
                $input = array(
                    'cliente_id' => $cliente->id,
                    'itens' => $produtos,
                    'lido' => 0,
                    'contato_id' => $proximo->id,
                );
            } else {
                $contato = ContatoOrcamento::first();
                $input = array(
                    'cliente_id' => $cliente->id,
                    'itens' => $produtos,
                    'lido' => 0,
                    'contato_id' => $contato->id,
                );
            }
        } else {
            $contato = ContatoOrcamento::first();
            $input = array(
                'cliente_id' => $cliente->id,
                'itens' => $produtos,
                'lido' => 0,
                'contato_id' => $contato->id,
            );
        }

        $orcamento = Orcamento::create($input);
        $this->sendMail($orcamento);

        session()->forget('orcamento');

        return redirect()->back()->with('success', "Pedido de orçamento enviado com sucesso!");
    }

    private function sendMail($orcamento)
    {
        $contato = ContatoOrcamento::where('id', $orcamento->contato_id)->first();
        if (!$email = $contato->email) {
            return false;
        }

        $cliente = Cliente::where('id', $orcamento['cliente_id'])->first();
        $produtos = $orcamento['itens'];

        Mail::send('emails.orcamento', ['cliente' => $cliente, 'produtos' => $produtos], function ($m) use ($email, $cliente) {
            $m->to($email, config('app.name'))
                ->subject('[ORÇAMENTO] ' . config('app.name'))
                ->replyTo($cliente['email'], $cliente['nome']);
        });
    }
}
