<?php

namespace App\Http\Controllers;

use App\Models\Novidade;

class NovidadesController extends Controller
{
    public function index()
    {
        $novidades = Novidade::join('categorias_novidades', 'categorias_novidades.id', '=', 'novidades.categoria_id')
            ->select('categorias_novidades.id as cat_id', 'categorias_novidades.slug as cat_slug', 'categorias_novidades.titulo as cat_titulo', 'novidades.id', 'novidades.data', 'novidades.slug', 'novidades.titulo', 'novidades.capa')
            ->orderBy('data', 'desc')->get();
        $destaqueTopo = $novidades->take(1)->first();
        $destaques = $novidades->take(3);

        return view('frontend.novidades', compact('novidades', 'destaqueTopo', 'destaques'));
    }

    public function show($novidade_slug)
    {
        $novidade = Novidade::join('categorias_novidades', 'categorias_novidades.id', '=', 'novidades.categoria_id')
            ->select('categorias_novidades.id as cat_id', 'categorias_novidades.slug as cat_slug', 'categorias_novidades.titulo as cat_titulo', 'novidades.id', 'novidades.data', 'novidades.slug', 'novidades.titulo', 'novidades.texto', 'novidades.capa')
            ->where('novidades.slug', $novidade_slug)->first();

        return view('frontend.novidades-show', compact('novidade'));
    }
}
