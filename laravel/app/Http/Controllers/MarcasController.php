<?php

namespace App\Http\Controllers;

use App\Models\Marca;

class MarcasController extends Controller
{
    public function index()
    {
        $marcas = Marca::orderBy('nome', 'asc')->get();

        return view('frontend.marcas', compact('marcas'));
    }
}
