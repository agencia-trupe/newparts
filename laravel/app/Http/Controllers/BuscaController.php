<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\Categoria;
use App\Models\Linha;
use App\Models\LinhaProduto;
use App\Models\Marca;
use App\Models\Produto;

class BuscaController extends Controller
{
    public function buscar(Request $request)
    {
        $linha = Linha::where('slug', $request->linha_slug)->first();

        $produtos = Produto::join('categorias', 'categorias.id', '=', 'produtos.categoria_id')
            ->join('linhas', 'linhas.id', '=', 'categorias.linha_id')
            ->join('marcas', 'marcas.id', '=', 'produtos.marca_id')
            ->select('linhas.id as linha_id', 'linhas.slug as linha_slug', 'linhas.titulo as linha_titulo', 'categorias.id as categoria_id', 'categorias.slug as categoria_slug', 'categorias.titulo as categoria_titulo', 'marcas.id as marca_id', 'marcas.slug as marca_slug', 'marcas.nome as marca_nome', 'marcas.imagem as marca_imagem', 'produtos.id', 'produtos.slug', 'produtos.titulo', 'produtos.capa', 'produtos.codigo')
            ->where('linhas.slug', $linha->slug)
            ->distinct()
            ->orderBy('titulo', 'asc');

        if ($request->has('palavraChave')) {
            $palavra = $request->palavraChave;
            $produtos = $produtos->where(function ($query) use ($palavra) {
                $query->where('produtos.titulo', 'LIKE', "%" . $palavra . "%")
                    ->orWhere('produtos.codigo', 'LIKE', "%" . $palavra . "%");
            });
        } else {
            $palavra = null;
        }

        if ($request->has('marca')) {
            $marca = $request->marca;
            $produtos = $produtos->where('produtos.marca_id', intval($marca));

            $marca = Marca::where('id', $marca)->first();
        } else {
            $marca = null;
        }

        if ($palavra == null && $marca == null) {
            return redirect()->route('produtos');
        }

        $produtos = $produtos->get();

        $categoriasLinha = Categoria::linha($linha->id)->orderBy('titulo', 'asc')->get();

        $categorias = [];
        foreach ($categoriasLinha as $categoria) {
            foreach ($produtos as $produto) {
                if ($produto->categoria_id == $categoria->id) {
                    $categorias[] = $categoria;
                }
            }
        }
        $categorias = array_unique($categorias);

        return view('frontend.busca-resultado', compact('produtos', 'palavra', 'marca', 'categorias', 'linha'));
    }

    public function getCategorias($linha_slug, $letra)
    {
        $linha = Linha::where('slug', $linha_slug)->first();
        $categoriasLinha = Categoria::linha($linha->id)->orderBy('titulo', 'asc')->get();

        $categorias = [];
        foreach ($categoriasLinha as $categoria) {
            if (mb_substr($categoria->slug, 0, 1) == $letra) {
                $categorias[] = $categoria;
            }
        }
        $categorias = array_unique($categorias);

        return response()->json(['categorias' => $categorias, 'linha' => $linha]);
    }
}
