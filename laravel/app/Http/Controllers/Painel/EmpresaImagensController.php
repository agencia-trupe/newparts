<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\EmpresaImagensRequest;
use App\Models\EmpresaImagem;

class EmpresaImagensController extends Controller
{
    public function index()
    {
        $imagens = EmpresaImagem::ordenados()->get();

        return view('painel.empresa.imagens.index', compact('imagens'));
    }

    public function show(EmpresaImagem $imagem)
    {
        return $imagem;
    }

    public function store(EmpresaImagensRequest $request)
    {
        try {
            $input = $request->all();
            $input['imagem'] = EmpresaImagem::upload_imagem();

            $imagem = EmpresaImagem::create($input);

            $view = view('painel.empresa.imagens.imagem', compact('imagem'))->render();

            return response()->json(['body' => $view]);
        } catch (\Exception $e) {

            return 'Erro ao adicionar imagem: ' . $e->getMessage();
        }
    }

    public function destroy(EmpresaImagem $imagem)
    {
        try {
            $imagem->delete();

            return redirect()->route('painel.empresa.imagens.index')
                ->with('success', 'Imagem excluída com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir imagem: ' . $e->getMessage()]);
        }
    }

    public function clear()
    {
        try {
            EmpresaImagem::truncate();

            return redirect()->route('painel.empresa.imagens.index')
                ->with('success', 'Imagens excluídas com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir imagens: ' . $e->getMessage()]);
        }
    }
}
