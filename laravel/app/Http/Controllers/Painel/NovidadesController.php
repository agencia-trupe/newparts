<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\NovidadesRequest;
use App\Models\CategoriaNovidade;
use App\Models\Novidade;

class NovidadesController extends Controller
{
    public function index()
    {
        $categorias = CategoriaNovidade::orderBy('titulo', 'asc')->get();

        if (isset($_GET['categoria'])) {
            $categoria = $_GET['categoria'];
            $novidades = Novidade::categoria($categoria)->orderBy('data', 'desc')->get();
        } else {
            $novidades = Novidade::orderBy('data', 'desc')->get();
        }

        return view('painel.novidades.index', compact('novidades', 'categorias'));
    }

    public function create()
    {
        $categorias = CategoriaNovidade::orderBy('titulo', 'asc')->pluck('titulo', 'id');

        return view('painel.novidades.create', compact('categorias'));
    }

    public function store(NovidadesRequest $request)
    {
        try {
            $input = $request->all();
            $input['slug'] = str_slug($request->titulo, '-');

            if (isset($input['capa'])) $input['capa'] = Novidade::upload_capa();

            $count = count(Novidade::where('slug', $input['slug'])->where('categoria_id', $input['categoria_id'])->get());

            if ($count == 0) {
                Novidade::create($input);
            } else {
                return back()->withErrors(['Erro ao adicionar registro: Verifique se esse registro já existe']);
            }

            return redirect()->route('painel.novidades.index')->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: ' . $e->getMessage()]);
        }
    }

    public function edit(Novidade $novidade)
    {
        $categorias = CategoriaNovidade::orderBy('titulo', 'asc')->pluck('titulo', 'id');

        return view('painel.novidades.edit', compact('novidade', 'categorias'));
    }

    public function update(NovidadesRequest $request, Novidade $novidade)
    {
        try {
            $input = $request->all();
            $input['slug'] = str_slug($request->titulo, '-');

            if (isset($input['capa'])) $input['capa'] = Novidade::upload_capa();

            $novidade->update($input);

            return redirect()->route('painel.novidades.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(Novidade $novidade)
    {
        try {
            $novidade->delete();

            return redirect()->route('painel.novidades.index')->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }
}
