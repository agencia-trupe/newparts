<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\MarcasRequest;
use App\Models\Marca;

class MarcasController extends Controller
{
    public function index()
    {
        $marcas = Marca::orderBy('nome', 'ASC')->get();

        return view('painel.marcas.index', compact('marcas'));
    }

    public function create()
    {
        return view('painel.marcas.create');
    }

    public function store(MarcasRequest $request)
    {
        try {
            $input = $request->all();
            $input['slug'] = str_slug($request->nome, '-');

            if (isset($input['imagem'])) $input['imagem'] = Marca::upload_imagem();

            $count = count(Marca::where('slug', $input['slug'])->get());

            if ($count == 0) {
                Marca::create($input);
            } else {
                return back()->withErrors(['Erro ao adicionar registro: Verifique se esse registro já existe']);
            }

            return redirect()->route('painel.marcas.index')->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: ' . $e->getMessage()]);
        }
    }

    public function edit(Marca $marca)
    {
        return view('painel.marcas.edit', compact('marca'));
    }

    public function update(MarcasRequest $request, Marca $marca)
    {
        try {
            $input = $request->all();
            $input['slug'] = str_slug($request->nome, '-');

            if (isset($input['imagem'])) $input['imagem'] = Marca::upload_imagem();

            $marca->update($input);

            return redirect()->route('painel.marcas.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(Marca $marca)
    {
        try {
            $marca->delete();

            return redirect()->route('painel.marcas.index')->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }
}
