<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\EmpresaRequest;
use App\Models\Empresa;

class EmpresaController extends Controller
{
    public function index()
    {
        $empresa = Empresa::first();

        return view('painel.empresa.edit', compact('empresa'));
    }

    public function update(EmpresaRequest $request, Empresa $empresa)
    {
        try {
            $input = $request->all();

            if (isset($input['capa'])) $input['capa'] = Empresa::upload_capa();

            $empresa->update($input);

            return redirect()->route('painel.empresa.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }
}
