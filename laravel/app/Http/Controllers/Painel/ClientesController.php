<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Models\Cliente;
use Maatwebsite\Excel\Facades\Excel;

class ClientesController extends Controller
{
    public function index()
    {
        $clientes = Cliente::orderBy('nome', 'ASC')->paginate(10);

        return view('painel.clientes.index', compact('clientes'));
    }

    public function destroy(Cliente $cliente)
    {
        try {
            $cliente->delete();

            return redirect()->route('painel.clientes.index')->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }

    public function export()
    {
        $row[] = array('Data', 'Nome', 'E-mail', 'Telefone', 'Empresa');

        $exportDados = Cliente::all();

        foreach ($exportDados as $dados) {
            $row[] = array(
                $dados->created_at,
                $dados->nome,
                $dados->email,
                $dados->telefone,
                $dados->empresa
            );
        }

        return Excel::create('Clientes - Cadastros', function ($excel) use ($row) {
            $excel->setTitle('Cadastros');
            $excel->sheet('Cadastros', function ($sheet) use ($row) {
                $sheet->fromArray($row, null, 'A1', false, false);
            });
        })->export('csv');
    }
}
