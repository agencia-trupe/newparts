<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Models\Cliente;
use App\Models\ContatoOrcamento;
use App\Models\Orcamento;
use App\Models\Produto;

class OrcamentosController extends Controller
{
    public function index()
    {
        $orcamentos = Orcamento::orderBy('created_at', 'desc')->get();
        $produtos = Produto::orderBy('titulo', 'asc')->get();
        $clientes = Cliente::all();
        $contatos = ContatoOrcamento::get();

        return view('painel.orcamentos.index', compact('orcamentos', 'clientes', 'produtos', 'contatos'));
    }

    public function show(Orcamento $orcamento)
    {
        $orcamento->update(['lido' => 1]);
        $cliente = Cliente::where('id', $orcamento->cliente_id)->first();

        return view('painel.orcamentos.show', compact('orcamento', 'cliente'));
    }

    public function destroy(Orcamento $orcamento)
    {
        try {
            $orcamento->delete();

            return redirect()->route('painel.orcamentos.index')->with('success', 'Mensagem excluída com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir mensagem: ' . $e->getMessage()]);
        }
    }

    public function toggle(Orcamento $orcamento)
    {
        try {
            $orcamento->update([
                'lido' => !$orcamento->lido
            ]);

            return redirect()->route('painel.orcamentos.index')->with('success', 'Mensagem alterada com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar mensagem: ' . $e->getMessage()]);
        }
    }
}
