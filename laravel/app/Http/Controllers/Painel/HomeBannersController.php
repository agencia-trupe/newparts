<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\HomeBannersRequest;
use App\Models\HomeBanner;

class HomeBannersController extends Controller
{
    public function index()
    {
        $banners = HomeBanner::ordenados()->get();

        return view('painel.home.banners.index', compact('banners'));
    }

    public function create()
    {
        return view('painel.home.banners.create');
    }

    public function store(HomeBannersRequest $request)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = HomeBanner::upload_imagem();

            HomeBanner::create($input);

            return redirect()->route('painel.home-banners.index')->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: ' . $e->getMessage()]);
        }
    }

    public function edit(HomeBanner $banner)
    {
        return view('painel.home.banners.edit', compact('banner'));
    }

    public function update(HomeBannersRequest $request, HomeBanner $banner)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = HomeBanner::upload_imagem();

            $banner->update($input);

            return redirect()->route('painel.home-banners.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(HomeBanner $banner)
    {
        try {
            $banner->delete();

            return redirect()->route('painel.home-banners.index')->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }
}
