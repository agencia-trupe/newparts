<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Models\Newsletter;
use Maatwebsite\Excel\Facades\Excel;

class NewsletterController extends Controller
{
    public function index()
    {
        $registros = Newsletter::orderBy('created_at', 'DESC')->paginate(10);

        return view('painel.contato.newsletter.index', compact('registros'));
    }

    public function export()
    {
        $row[] = array('Data', 'Nome', 'E-mail');

        $exportDados = Newsletter::all();

        foreach ($exportDados as $dados) {
            $row[] = array(
                $dados->created_at,
                $dados->nome,
                $dados->email
            );
        }

        return Excel::create('Newsletter - Cadastros', function ($excel) use ($row) {
            $excel->setTitle('Cadastros');
            $excel->sheet('Cadastros', function ($sheet) use ($row) {
                $sheet->fromArray($row, null, 'A1', false, false);
            });
        })->export('csv');
    }
}
