<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\CategoriasRequest;
use App\Http\Requests\LinhasRequest;
use App\Models\Categoria;
use App\Models\Linha;

class CategoriasController extends Controller
{
    public function index(Linha $linhas)
    {
        $categorias = Categoria::linha($linhas->id)->orderBy('titulo', 'asc')->paginate(15);

        return view('painel.linhas.categorias.index', compact('linhas', 'categorias'));
    }

    public function create(Linha $linhas)
    {
        return view('painel.linhas.categorias.create', compact('linhas'));
    }

    public function store(LinhasRequest $request, Linha $linhas)
    {
        try {
            $input = $request->all();
            $input['slug'] = str_slug($request->titulo, '-');
            $input['linha_id'] = $linhas->id;

            $count = count(Categoria::where('slug', $input['slug'])->where('linha_id', $input['linha_id'])->get());

            if ($count == 0) {
                Categoria::create($input);
            } else {
                return back()->withErrors(['Erro ao adicionar registro: Verifique se esse registro já existe']);
            }

            return redirect()->route('painel.linhas.categorias.index', $linhas->id)->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: ' . $e->getMessage()]);
        }
    }

    public function edit(Linha $linhas, Categoria $categorias)
    {
        $categoria = $categorias;

        return view('painel.linhas.categorias.edit', compact('linhas', 'categoria'));
    }

    public function update(CategoriasRequest $request, Linha $linhas, Categoria $categorias)
    {
        try {
            $input = $request->all();
            $input['slug'] = str_slug($request->titulo, '-');

            $categorias->update($input);

            return redirect()->route('painel.linhas.categorias.index', $linhas->id)->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(Linha $linhas, Categoria $categorias)
    {
        try {
            $categorias->delete();

            return redirect()->route('painel.linhas.categorias.index', $linhas->id)->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }
}
