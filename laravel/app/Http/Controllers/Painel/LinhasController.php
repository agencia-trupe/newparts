<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\LinhasRequest;
use App\Models\Linha;

class LinhasController extends Controller
{
    public function index()
    {
        $linhas = Linha::orderBy('id', 'asc')->get();

        return view('painel.linhas.index', compact('linhas'));
    }
}
