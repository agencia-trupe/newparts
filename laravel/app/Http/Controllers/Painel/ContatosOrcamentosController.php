<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\ContatosOrcamentosRequest;
use App\Models\ContatoOrcamento;

class ContatosOrcamentosController extends Controller
{
    public function index()
    {
        $contatos = ContatoOrcamento::orderBy('id', 'asc')->get();

        return view('painel.orcamentos.contatos.index', compact('contatos'));
    }

    public function create()
    {
        return view('painel.orcamentos.contatos.create');
    }

    public function store(ContatosOrcamentosRequest $request)
    {
        try {
            $input = $request->all();

            ContatoOrcamento::create($input);

            return redirect()->route('painel.orcamentos-contatos.index')->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: ' . $e->getMessage()]);
        }
    }

    public function edit($contato_id)
    {
        $contato = ContatoOrcamento::where('id', $contato_id)->first();

        return view('painel.orcamentos.contatos.edit', compact('contato'));
    }

    public function update(ContatosOrcamentosRequest $request, $contato_id)
    {
        try {
            $input = $request->all();

            $contato = ContatoOrcamento::where('id', $contato_id)->first();
            $contato->update($input);

            return redirect()->route('painel.orcamentos-contatos.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy($contato_id)
    {
        try {
            $contato = ContatoOrcamento::where('id', $contato_id)->first();
            $contato->delete();

            return redirect()->route('painel.orcamentos-contatos.index')->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }
}
