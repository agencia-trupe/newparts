<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\EmpresaDiferenciaisRequest;
use App\Models\EmpresaDiferencial;

class EmpresaDiferenciaisController extends Controller
{
    public function index()
    {
        $diferenciais = EmpresaDiferencial::ordenados()->get();

        return view('painel.empresa.diferenciais.index', compact('diferenciais'));
    }

    public function create()
    {
        return view('painel.empresa.diferenciais.create');
    }

    public function store(EmpresaDiferenciaisRequest $request)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = EmpresaDiferencial::upload_imagem();

            EmpresaDiferencial::create($input);

            return redirect()->route('painel.empresa.diferenciais.index')->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: ' . $e->getMessage()]);
        }
    }

    public function edit($diferencial_id)
    {
        $diferencial = EmpresaDiferencial::where('id', $diferencial_id)->first();

        return view('painel.empresa.diferenciais.edit', compact('diferencial'));
    }

    public function update(EmpresaDiferenciaisRequest $request, $diferencial_id)
    {
        try {
            $diferencial = EmpresaDiferencial::where('id', $diferencial_id)->first();

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = EmpresaDiferencial::upload_imagem();

            $diferencial->update($input);

            return redirect()->route('painel.empresa.diferenciais.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy($diferencial_id)
    {
        try {
            $diferencial = EmpresaDiferencial::where('id', $diferencial_id)->first();
            $diferencial->delete();

            return redirect()->route('painel.empresa.diferenciais.index')->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }
}
