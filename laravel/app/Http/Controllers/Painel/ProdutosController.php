<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProdutosRequest;
use App\Models\Categoria;
use App\Models\Linha;
use App\Models\LinhaProduto;
use App\Models\Marca;
use App\Models\Produto;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProdutosController extends Controller
{
    public function removerAcentos()
    {
        $conversao = array(
            'á' => 'a', 'à' => 'a', 'ã' => 'a', 'â' => 'a', 'é' => 'e',
            'ê' => 'e', 'í' => 'i', 'ï' => 'i', 'ó' => 'o', 'ô' => 'o', 'õ' => 'o', "ö" => "o",
            'ú' => 'u', 'ü' => 'u', 'ç' => 'c', 'ñ' => 'n', 'Á' => 'A', 'À' => 'A', 'Ã' => 'A',
            'Â' => 'A', 'É' => 'E', 'Ê' => 'E', 'Í' => 'I', 'Ï' => 'I', "Ö" => "O", 'Ó' => 'O',
            'Ô' => 'O', 'Õ' => 'O', 'Ú' => 'U', 'Ü' => 'U', 'Ç' => 'C', 'N' => 'Ñ'
        );

        return $conversao;
    }

    public function index()
    {
        $linhas = Linha::orderBy('titulo', 'asc')->get();
        $categorias = Categoria::orderBy('titulo', 'asc')->get();
        $marcas = Marca::orderBy('nome', 'asc')->get();

        $produtos = DB::table('produtos')
            ->join('categorias', 'categorias.id', '=', 'produtos.categoria_id')
            ->join('linhas', 'linhas.id', '=', 'categorias.linha_id')
            ->join('marcas', 'marcas.id', '=', 'marca_id')
            ->select(['produtos.*', 'linhas.id as linha_id', 'linhas.titulo as linha_titulo', 'categorias.id as categoria_id', 'categorias.titulo as categoria_titulo', 'marcas.id as marca_id', 'marcas.nome as marca_nome'])
            ->orderBy('produtos.titulo', 'asc')
            ->distinct();

        if (isset($_GET['linha'])) {
            $linha = $_GET['linha'];
            $produtos = $produtos->where('linha_id', $linha);
            $categorias = Categoria::linha($linha)->orderBy('titulo', 'asc')->get();
        } elseif (isset($_GET['categoria'])) {
            $categoria = $_GET['categoria'];
            $produtos = $produtos->where('categoria_id', $categoria);
        } elseif (isset($_GET['marca'])) {
            $marca = $_GET['marca'];
            $produtos = $produtos->where('marca_id', $marca);
        } elseif (isset($_GET['vitrine'])) {
            $vitrine = $_GET['vitrine'];
            if ($vitrine == "vitrine_home") {
                $produtos = $produtos->home($vitrine);
            }
            if ($vitrine == "vitrine_produtos") {
                $produtos = $produtos->produtos($vitrine);
            }
            if ($vitrine == "lancamentos") {
                $produtos = $produtos->lancamentos($vitrine);
            }
        } else {
            $produtos = $produtos;
        }
        $produtos = $produtos->get();

        return view('painel.produtos.index', compact('produtos', 'linhas', 'categorias', 'marcas'));
    }

    public function create()
    {
        $linhas = Linha::orderBy('titulo', 'asc')->get();
        $marcas = Marca::orderBy('nome', 'asc')->get();

        return view('painel.produtos.create', compact('linhas', 'marcas'));
    }

    public function getCategorias($linha)
    {
        $categorias = Categoria::linha($linha)->orderBy('titulo', 'asc')->get();

        return response()->json(['categorias' => $categorias]);
    }

    public function store(ProdutosRequest $request)
    {
        try {
            $input = $request->except('linha_id');
            $input['slug'] = str_slug($request->titulo, '-');

            if (isset($input['capa'])) $input['capa'] = Produto::upload_capa();
            if (isset($input['imagem'])) $input['imagem'] = Produto::upload_imagem();

            // variações
            if (isset($input['variacao_01'])) $input['variacao_01'] = Produto::upload_variacao_01();
            if (isset($input['variacao_02'])) $input['variacao_02'] = Produto::upload_variacao_02();
            if (isset($input['variacao_03'])) $input['variacao_03'] = Produto::upload_variacao_03();
            if (isset($input['variacao_04'])) $input['variacao_04'] = Produto::upload_variacao_04();
            if (isset($input['variacao_05'])) $input['variacao_05'] = Produto::upload_variacao_05();
            if (isset($input['variacao_06'])) $input['variacao_06'] = Produto::upload_variacao_06();
            if (isset($input['variacao_07'])) $input['variacao_07'] = Produto::upload_variacao_07();
            if (isset($input['variacao_08'])) $input['variacao_08'] = Produto::upload_variacao_08();
            if (isset($input['variacao_09'])) $input['variacao_09'] = Produto::upload_variacao_09();
            if (isset($input['variacao_10'])) $input['variacao_10'] = Produto::upload_variacao_10();

            $file = $request->file('arquivo');

            if ($request->hasFile('arquivo')) {
                if ($request->file('arquivo')->isValid()) {
                    $filename = strtoupper(str_replace(' ', '-', strtr($file->getClientOriginalName(), $this->removerAcentos())));
                    $path = public_path() . '\assets\arquivos';
                    $file->move($path, $filename);
                    $input['arquivo'] = $filename;
                }
            }

            $count = count(Produto::where('slug', $input['slug'])->where('categoria_id', $input['categoria_id'])->get());

            if ($count == 0) {
                Produto::create($input);
            } else {
                return back()->withErrors(['Erro ao adicionar registro: Verifique se esse registro já existe']);
            }

            return redirect()->route('painel.produtos.index')->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: ' . $e->getMessage()]);
        }
    }

    public function edit(Produto $produto)
    {
        $linhas = Linha::orderBy('titulo', 'asc')->get();
        $marcas = Marca::orderBy('nome', 'asc')->get();
        $categoriaSelected = Categoria::where('id', $produto->categoria_id)->first();
        $linhaSelected = Linha::where('id', $categoriaSelected->linha_id)->first();
        $categorias = Categoria::linha($linhaSelected->id)->get();

        return view('painel.produtos.edit', compact('produto', 'linhas', 'marcas', 'categorias', 'categoriaSelected', 'linhaSelected'));
    }

    public function update(ProdutosRequest $request, Produto $produto)
    {
        try {
            $input = $request->except('linha_id');
            $input['slug'] = str_slug($request->titulo, '-');

            if (isset($input['capa'])) $input['capa'] = Produto::upload_capa();
            if (isset($input['imagem'])) $input['imagem'] = Produto::upload_imagem();

            // variações
            if (isset($input['variacao_01'])) $input['variacao_01'] = Produto::upload_variacao_01();
            if (isset($input['variacao_02'])) $input['variacao_02'] = Produto::upload_variacao_02();
            if (isset($input['variacao_03'])) $input['variacao_03'] = Produto::upload_variacao_03();
            if (isset($input['variacao_04'])) $input['variacao_04'] = Produto::upload_variacao_04();
            if (isset($input['variacao_05'])) $input['variacao_05'] = Produto::upload_variacao_05();
            if (isset($input['variacao_06'])) $input['variacao_06'] = Produto::upload_variacao_06();
            if (isset($input['variacao_07'])) $input['variacao_07'] = Produto::upload_variacao_07();
            if (isset($input['variacao_08'])) $input['variacao_08'] = Produto::upload_variacao_08();
            if (isset($input['variacao_09'])) $input['variacao_09'] = Produto::upload_variacao_09();
            if (isset($input['variacao_10'])) $input['variacao_10'] = Produto::upload_variacao_10();

            $file = $request->file('arquivo');

            if ($request->hasFile('arquivo')) {
                if ($request->file('arquivo')->isValid()) {
                    $filename = strtoupper(str_replace(' ', '-', strtr($file->getClientOriginalName(), $this->removerAcentos())));
                    $path = public_path() . '\assets\arquivos';
                    $file->move($path, $filename);
                    $input['arquivo'] = $filename;
                }
            }

            $produto->update($input);

            return redirect()->route('painel.produtos.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(Produto $produto)
    {
        try {
            $produto->delete();

            return redirect()->route('painel.produtos.index')->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }

    public function deleteImagem(Produto $produto)
    {
        $variacao = $_GET['variacao'];

        if ($produto[$variacao]) {
            $produto[$variacao] = null;
            $produto->save();
        }

        return response()->json(['produto' => $produto, 'variacao' => $variacao]);
    }

    public function deleteDesenho(Produto $produto)
    {
        if ($produto['imagem']) {
            $produto['imagem'] = null;
            $produto->save();
        }

        return response()->json(['produto' => $produto]);
    }

    public function deleteManual(Produto $produto)
    {
        if ($produto['arquivo']) {
            $produto['arquivo'] = null;
            $produto->save();
        }

        return response()->json(['produto' => $produto]);
    }
}
