<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\HomeDiferenciaisRequest;
use App\Models\HomeDiferencial;

class HomeDiferenciaisController extends Controller
{
    public function index()
    {
        $diferenciais = HomeDiferencial::ordenados()->get();

        return view('painel.home.diferenciais.index', compact('diferenciais'));
    }

    public function create()
    {
        return view('painel.home.diferenciais.create');
    }

    public function store(HomeDiferenciaisRequest $request)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = HomeDiferencial::upload_imagem();

            HomeDiferencial::create($input);

            return redirect()->route('painel.home-diferenciais.index')->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: ' . $e->getMessage()]);
        }
    }

    public function edit(HomeDiferencial $diferencial)
    {
        return view('painel.home.diferenciais.edit', compact('diferencial'));
    }

    public function update(HomeDiferenciaisRequest $request, HomeDiferencial $diferencial)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = HomeDiferencial::upload_imagem();

            $diferencial->update($input);

            return redirect()->route('painel.home-diferenciais.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(HomeDiferencial $diferencial)
    {
        try {
            $diferencial->delete();

            return redirect()->route('painel.home-diferenciais.index')->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }
}
