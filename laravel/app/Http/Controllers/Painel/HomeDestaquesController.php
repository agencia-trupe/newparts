<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\HomeDestaquesRequest;
use App\Models\HomeDestaque;

class HomeDestaquesController extends Controller
{
    public function index()
    {
        $destaques = HomeDestaque::first();

        return view('painel.home.destaques.edit', compact('destaques'));
    }

    public function update(HomeDestaquesRequest $request, HomeDestaque $destaques)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem_1'])) $input['imagem_1'] = HomeDestaque::upload_imagem_1();
            if (isset($input['imagem_2'])) $input['imagem_2'] = HomeDestaque::upload_imagem_2();
            if (isset($input['imagem_3'])) $input['imagem_3'] = HomeDestaque::upload_imagem_3();

            $destaques->update($input);

            return redirect()->route('painel.home-destaques.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }
}
