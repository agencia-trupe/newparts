<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\CategoriasNovidadesRequest;
use App\Models\CategoriaNovidade;

class CategoriasNovidadesController extends Controller
{
    public function index()
    {
        $categorias = CategoriaNovidade::orderBy('titulo', 'desc')->get();

        return view('painel.novidades.categorias.index', compact('categorias'));
    }

    public function create()
    {
        return view('painel.novidades.categorias.create');
    }

    public function store(CategoriasNovidadesRequest $request)
    {
        try {
            $input = $request->all();
            $input['slug'] = str_slug($request->titulo, '-');

            $count = count(CategoriaNovidade::where('slug', $input['slug'])->get());

            if ($count == 0) {
                CategoriaNovidade::create($input);
            } else {
                return back()->withErrors(['Erro ao adicionar registro: Verifique se esse registro já existe']);
            }

            return redirect()->route('painel.categorias-novidades.index')->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: ' . $e->getMessage()]);
        }
    }

    public function edit(CategoriaNovidade $categoria)
    {
        return view('painel.novidades.categorias.edit', compact('categoria'));
    }

    public function update(CategoriasNovidadesRequest $request, CategoriaNovidade $categoria)
    {
        try {
            $input = $request->all();
            $input['slug'] = str_slug($request->titulo, '-');

            $categoria->update($input);

            return redirect()->route('painel.categorias-novidades.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(CategoriaNovidade $categoria)
    {
        try {
            $categoria->delete();

            return redirect()->route('painel.categorias-novidades.index')->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }
}
