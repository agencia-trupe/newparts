<?php

namespace App\Http\Controllers;

use App\Models\Empresa;
use App\Models\EmpresaDiferencial;
use App\Models\EmpresaImagem;

class EmpresaController extends Controller
{
    public function index()
    {
        $empresa = Empresa::first();
        $imagens = EmpresaImagem::ordenados()->get();
        $diferenciais = EmpresaDiferencial::ordenados()->get();

        return view('frontend.empresa', compact('empresa', 'imagens', 'diferenciais'));
    }
}
