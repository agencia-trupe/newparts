<?php

namespace App\Http\Controllers;

use App\Http\Requests\NewsletterRequest;
use App\Models\AceiteDeCookies;
use App\Models\Contato;
use App\Models\HomeBanner;
use App\Models\HomeDestaque;
use App\Models\HomeDiferencial;
use App\Models\Marca;
use App\Models\Newsletter;
use App\Models\Novidade;
use App\Models\Produto;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class HomeController extends Controller
{
    public function index(Request $request)
    {
        $banners = HomeBanner::ordenados()->get();
        $destaques = HomeDestaque::first();
        $diferenciais = HomeDiferencial::ordenados()->get();
        $novidades = Novidade::join('categorias_novidades', 'categorias_novidades.id', '=', 'novidades.categoria_id')
            ->select('categorias_novidades.id as cat_id', 'categorias_novidades.slug as cat_slug', 'categorias_novidades.titulo as cat_titulo', 'novidades.id', 'novidades.data', 'novidades.slug', 'novidades.titulo', 'novidades.capa')
            ->orderBy('data', 'desc')->take(3)->get();

        $verificacao = AceiteDeCookies::where('ip', $request->ip())->first();

        return view('frontend.home', compact('banners', 'destaques', 'diferenciais', 'novidades', 'verificacao'));
    }

    public function getProdutosHome()
    {
        $produtos = Produto::home()
            ->join('categorias', 'categorias.id', '=', 'produtos.categoria_id')
            ->join('linhas', 'linhas.id', '=', 'categorias.linha_id')
            ->select('produtos.*', 'linhas.id as linha_id', 'linhas.slug as linha_slug', 'linhas.titulo as linha_titulo')
            ->get();

        return response()->json(['produtos' => $produtos]);
    }

    public function getMarcasHome()
    {
        $marcas = Marca::get();

        return response()->json(['marcas' => $marcas]);
    }

    public function postNewsletter(NewsletterRequest $request, Newsletter $newsletter)
    {
        $data = $request->all();

        $newsletter->create($data);
        $this->sendMail($data);

        return redirect()->route('home')->with('success', true);
    }

    private function sendMail($data)
    {
        if (!$email = Contato::first()->email) {
            return false;
        }

        Mail::send('emails.newsletter', $data, function ($m) use ($email, $data) {
            $m->to($email, config('app.name'))
                ->subject('[CONTATO] ' . config('app.name'))
                ->replyTo($data['email'], $data['nome']);
        });
    }

    public function postCookies(Request $request)
    {
        $input = $request->all();
        $input['ip'] = $request->ip();

        AceiteDeCookies::create($input);

        return redirect('/');
    }
}
