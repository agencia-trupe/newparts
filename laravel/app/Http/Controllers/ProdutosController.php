<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Categoria;
use App\Models\Linha;
use App\Models\LinhaProduto;
use App\Models\Marca;
use App\Models\Produto;
use Illuminate\Routing\Route;
use Illuminate\Support\Facades\DB;

class ProdutosController extends Controller
{
    public function index($linha_slug) //ok
    {
        $linha = Linha::where('slug', $linha_slug)->first();
        $categorias = Categoria::linha($linha->id)->orderBy('titulo', 'asc')->get();
        $marcas = Marca::orderBy('nome', 'asc')->get();

        return view('frontend.produtos', compact('produtos', 'marcas', 'linha', 'categorias'));
    }

    public function busca(Request $request, $linha_slug) //ok
    {
        $linha = Linha::where('slug', $linha_slug)->first();
        $categorias = Categoria::linha($linha->id)->orderBy('titulo', 'asc')->get();
        $marcas = Marca::orderBy('nome', 'asc')->get();

        $marcaFiltrada = $request->marca_slug;

        $produtos = Produto::join('categorias', 'categorias.id', '=', 'produtos.categoria_id')
            ->join('linhas', 'linhas.id', '=', 'categorias.linha_id')
            ->join('marcas', 'marcas.id', '=', 'produtos.marca_id')
            ->select('linhas.id as linha_id', 'linhas.slug as linha_slug', 'linhas.titulo as linha_titulo', 'categorias.id as categoria_id', 'categorias.slug as categoria_slug', 'categorias.titulo as categoria_titulo', 'marcas.id as marca_id', 'marcas.slug as marca_slug', 'marcas.nome as marca_nome', 'marcas.imagem as marca_imagem', 'produtos.id', 'produtos.slug', 'produtos.titulo', 'produtos.capa', 'produtos.codigo')
            ->where('linhas.slug', $linha->slug)
            ->distinct()
            ->orderBy('titulo', 'asc');

        if ($marcaFiltrada == "" || $marcaFiltrada == null || $marcaFiltrada == "null" || $marcaFiltrada == "todos") {
            $marcaFiltrada = null;
        } else {
            $marcaFiltrada = Marca::where('slug', $request->marca_slug)->first();
            $produtos = $produtos->where('marca_id', $marcaFiltrada->id);
        }

        if ($request->has('palavraChave')) {
            $palavra = $request->palavraChave;
            $produtos = $produtos->where(function ($query) use ($palavra) {
                $query->where('produtos.titulo', 'LIKE', "%" . $palavra . "%")
                    ->orWhere('produtos.codigo', 'LIKE', "%" . $palavra . "%");
            });
        }

        $produtos = $produtos->get();

        return view('frontend.produtos-filtro', compact('produtos', 'marcas', 'linha', 'categorias', 'palavra', 'marcaFiltrada'));
    }

    public function filtro($linha_slug, $categoria_slug, $marca_slug = null) //ok
    {
        $linha = Linha::where('slug', $linha_slug)->first();

        if ($marca_slug == null) {
            $categoriaFiltrada = Categoria::where('slug', $categoria_slug)->first();
            $produtos = Produto::join('categorias', 'categorias.id', '=', 'produtos.categoria_id')
                ->join('linhas', 'linhas.id', '=', 'categorias.linha_id')
                ->join('marcas', 'marcas.id', '=', 'produtos.marca_id')
                ->select('linhas.id as linha_id', 'linhas.slug as linha_slug', 'linhas.titulo as linha_titulo', 'categorias.id as categoria_id', 'categorias.slug as categoria_slug', 'categorias.titulo as categoria_titulo', 'marcas.id as marca_id', 'marcas.slug as marca_slug', 'marcas.nome as marca_nome', 'marcas.imagem as marca_imagem', 'produtos.id', 'produtos.slug', 'produtos.titulo', 'produtos.capa', 'produtos.codigo')
                ->where('linhas.slug', $linha->slug)
                ->where('categorias.slug', $categoriaFiltrada->slug)
                ->orderBy('produtos.titulo', 'asc')->get();
        } else {
            $marcaFiltrada = Marca::where('slug', $marca_slug)->first();
            $categoriaFiltrada = Categoria::where('slug', $categoria_slug)->first();
            $produtos = Produto::join('categorias', 'categorias.id', '=', 'produtos.categoria_id')
                ->join('linhas', 'linhas.id', '=', 'categorias.linha_id')
                ->join('marcas', 'marcas.id', '=', 'produtos.marca_id')
                ->select('linhas.id as linha_id', 'linhas.slug as linha_slug', 'linhas.titulo as linha_titulo', 'categorias.id as categoria_id', 'categorias.slug as categoria_slug', 'categorias.titulo as categoria_titulo', 'marcas.id as marca_id', 'marcas.slug as marca_slug', 'marcas.nome as marca_nome', 'marcas.imagem as marca_imagem', 'produtos.id', 'produtos.slug', 'produtos.titulo', 'produtos.capa', 'produtos.codigo')
                ->where('marcas.slug', $marcaFiltrada->slug)
                ->where('linhas.slug', $linha->slug)
                ->where('categorias.slug', $categoriaFiltrada->slug)
                ->orderBy('produtos.titulo', 'asc')->get();
        }

        $marcas = Marca::orderBy('nome', 'asc')->get();
        $categorias = Categoria::linha($linha->id)->orderBy('titulo', 'asc')->get();

        return view('frontend.produtos-filtro', compact('marcaFiltrada', 'linha', 'categoriaFiltrada', 'produtos', 'marcas', 'categorias'));
    }

    public function getProdutos($linha_slug) //ok
    {
        $produtos = DB::table('produtos')
            ->join('marcas', 'marcas.id', '=', 'produtos.marca_id')
            ->join('categorias', 'categorias.id', '=', 'produtos.categoria_id')
            ->join('linhas', 'linhas.id', '=', 'categorias.linha_id')
            ->select('produtos.id', 'produtos.slug', 'produtos.titulo', 'produtos.capa', 'produtos.codigo', 'marcas.imagem as marca_imagem', 'marcas.nome as marca_nome', 'linhas.id as linha_id', 'linhas.slug as linha_slug', 'linhas.titulo as linha_titulo', 'categorias.id as categoria_id', 'categorias.slug as categoria_slug', 'categorias.titulo as categoria_titulo')
            ->where('linhas.slug', $linha_slug)
            ->orderBy('produtos.titulo', 'asc')->get();

        return response()->json(['produtos' => $produtos]);
    }

    public function indexCategorias($linha_slug) //ok
    {
        $botoeiras = Categoria::where('slug', 'botoeiras')->first();
        $cabines = Categoria::where('slug', 'cabines')->first();

        $linha = Linha::where('slug', $linha_slug)->first();

        $categorias = Categoria::linha($linha->id)->orderBy('titulo', 'asc')->get();

        $letras = [];
        foreach ($categorias as $categoria) {
            $letras[] = mb_substr($categoria->slug, 0, 1);
        }
        $letrasCategorias = array_unique($letras);

        return view('frontend.produtos-linhas', compact('botoeiras', 'cabines', 'categorias', 'letrasCategorias', 'linha'));
    }

    public function filtroLetras($linha_slug, $letra) //ok
    {
        $linha = Linha::where('slug', $linha_slug)->first();

        $categorias = Categoria::linha($linha->id)->orderBy('titulo', 'asc')->get();

        if (strlen($letra) == 1) {
            foreach ($categorias as $categoria) {
                if (mb_substr($categoria->slug, 0, 1) == $letra) {
                    $categoriasSubmenu[] = $categoria;
                }
            }
            foreach ($categoriasSubmenu as $categoria) {
                $produtos[] = Produto::join('categorias', 'categorias.id', '=', 'produtos.categoria_id')
                    ->join('linhas', 'linhas.id', '=', 'categorias.linha_id')
                    ->join('marcas', 'marcas.id', '=', 'produtos.marca_id')
                    ->select('linhas.id as linha_id', 'linhas.slug as linha_slug', 'linhas.titulo as linha_titulo', 'categorias.id as categoria_id', 'categorias.slug as categoria_slug', 'categorias.titulo as categoria_titulo', 'marcas.id as marca_id', 'marcas.slug as marca_slug', 'marcas.nome as marca_nome', 'marcas.imagem as marca_imagem', 'produtos.id', 'produtos.slug', 'produtos.titulo', 'produtos.capa', 'produtos.codigo')
                    ->where('linhas.slug', $linha->slug)
                    ->where('categorias.slug', $categoria->slug)
                    ->orderBy('produtos.titulo', 'asc')->get();
            }
        }

        $marcas = Marca::orderBy('nome', 'asc')->get();

        return view('frontend.produtos-letras', compact('linha', 'categorias', 'produtos', 'marcas', 'categoriasSubmenu', 'linha'));
    }

    public function filtroMarcas($linha_slug, $marca_slug) //ok
    {
        $linha = Linha::where('slug', $linha_slug)->first();

        $marca = Marca::where('slug', $marca_slug)->first();

        $produtos = Produto::join('categorias', 'categorias.id', '=', 'produtos.categoria_id')
            ->join('linhas', 'linhas.id', '=', 'categorias.linha_id')
            ->join('marcas', 'marcas.id', '=', 'produtos.marca_id')
            ->select('linhas.id as linha_id', 'linhas.slug as linha_slug', 'linhas.titulo as linha_titulo', 'categorias.id as categoria_id', 'categorias.slug as categoria_slug', 'categorias.titulo as categoria_titulo', 'marcas.id as marca_id', 'marcas.slug as marca_slug', 'marcas.nome as marca_nome', 'marcas.imagem as marca_imagem', 'produtos.id', 'produtos.slug', 'produtos.titulo', 'produtos.capa', 'produtos.codigo')
            ->where('linhas.slug', $linha->slug)
            ->where('marcas.slug', $marca_slug)
            ->orderBy('titulo', 'asc')->distinct()->get();

        $categorias = Categoria::linha($linha->id)->orderBy('titulo', 'asc')->get();

        $marcas = Marca::orderBy('nome', 'asc')->get();

        return view('frontend.produtos-marca', compact('linha', 'categorias', 'produtos', 'marca', 'marcas'));
    }

    public function filtroLancamentos($linha_slug) //ok
    {
        $linha = Linha::where('slug', $linha_slug)->first();

        $produtos = Produto::lancamentos()
            ->join('categorias', 'categorias.id', '=', 'produtos.categoria_id')
            ->join('linhas', 'linhas.id', '=', 'categorias.linha_id')
            ->join('marcas', 'marcas.id', '=', 'produtos.marca_id')
            ->select('linhas.id as linha_id', 'linhas.slug as linha_slug', 'linhas.titulo as linha_titulo', 'categorias.id as categoria_id', 'categorias.slug as categoria_slug', 'categorias.titulo as categoria_titulo', 'marcas.id as marca_id', 'marcas.slug as marca_slug', 'marcas.nome as marca_nome', 'marcas.imagem as marca_imagem', 'produtos.id', 'produtos.slug', 'produtos.titulo', 'produtos.capa', 'produtos.codigo')
            ->where('linhas.slug', $linha->slug)
            ->orderBy('produtos.titulo', 'asc')->distinct()->paginate(15);

        $categorias = Categoria::linha($linha->id)->orderBy('titulo', 'asc')->get();
        $marcas = Marca::orderBy('nome', 'asc')->get();

        return view('frontend.produtos-lancamentos', compact('produtos', 'linha', 'categorias', 'marcas'));
    }

    public function showProduto($linha_slug, $produto_slug)
    {
        $linha = Linha::where('slug', $linha_slug)->first();
        $categorias = Categoria::linha($linha->id)->orderBy('titulo', 'asc')->get();

        $produto = Produto::join('marcas', 'marcas.id', '=', 'produtos.marca_id')
            ->join('categorias', 'categorias.id', '=', 'produtos.categoria_id')
            ->join('linhas', 'linhas.id', '=', 'categorias.linha_id')
            ->select('linhas.id as linha_id', 'linhas.slug as linha_slug', 'linhas.titulo as linha_titulo', 'categorias.id as categoria_id', 'categorias.slug as categoria_slug', 'categorias.titulo as categoria_titulo', 'marcas.id as marca_id', 'marcas.slug as marca_slug', 'marcas.nome as marca_nome', 'marcas.imagem as marca_imagem', 'produtos.id', 'produtos.codigo', 'produtos.slug', 'produtos.titulo', 'produtos.descricao', 'produtos.capa', 'produtos.imagem', 'produtos.video', 'produtos.arquivo', 'produtos.variacao_01', 'produtos.variacao_02', 'produtos.variacao_03', 'produtos.variacao_04', 'produtos.variacao_05', 'produtos.variacao_06', 'produtos.variacao_07', 'produtos.variacao_08', 'produtos.variacao_09', 'produtos.variacao_10')
            ->where('produtos.slug', $produto_slug)->first();

        $marcas = Marca::orderBy('nome', 'asc')->get();

        return view('frontend.produtos-show', compact('produto', 'marcas', 'linha', 'categorias'));
    }

    public function openPdf($produto_slug)
    {
        $arquivo = Produto::where('slug', $produto_slug)->select('arquivo')->first();

        $arquivoPath = public_path() . "/assets/arquivos/" . $arquivo->arquivo;

        return response()->file($arquivoPath);
    }
}
