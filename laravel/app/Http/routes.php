<?php

use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Route;

Route::group(['middleware' => ['web']], function () {
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('home/produtos', 'HomeController@getProdutosHome')->name('getProdutosHome');
    Route::get('home/marcas', 'HomeController@getMarcasHome')->name('getMarcasHome');
    Route::post('newsletter', 'HomeController@postNewsletter')->name('newsletter.post');
    Route::get('produtos/{linha}', 'ProdutosController@index')->name('produtos'); //ok
    Route::get('produtos/{linha}/produto/{produto}', 'ProdutosController@showProduto')->name('produtos.show');
    Route::get('produtos/{linha}/busca', 'ProdutosController@busca')->name('produtos.busca'); //ok
    Route::get('produtos/{linha}/lancamentos', 'ProdutosController@filtroLancamentos')->name('produtos.lancamentos');
    Route::get('produtos/{linha}/getProdutos', 'ProdutosController@getProdutos')->name('produtos.getProdutos');
    Route::get('produtos/{linha}/categorias', 'ProdutosController@indexCategorias')->name('produtos.categorias');
    Route::get('produtos/{linha}/categorias/letra/{letra}', 'ProdutosController@filtroLetras')->name('produtos.letra');
    Route::get('produtos/{linha}/marcas/{marca}', 'ProdutosController@filtroMarcas')->name('produtos.marcas');
    Route::get('produtos/{linha}/{categoria}/{marca_slug?}', 'ProdutosController@filtro')->name('produtos.filtro');
    Route::get('produtos/pdf/{produto}', 'ProdutosController@openPdf')->name('produtos.pdf');
    Route::get('orcamentos', 'OrcamentosController@index')->name('orcamentos');
    Route::post('orcamentos/adicionar/{linha}/{produto}', 'OrcamentosController@adicionar')->name('orcamentos.adicionar');
    Route::get('orcamentos/atualizar/{produto}', 'OrcamentosController@atualizar')->name('orcamentos.atualizar');
    Route::get('orcamentos/excluir/{produto}', 'OrcamentosController@excluir')->name('orcamentos.excluir');
    Route::get('orcamentos/{cliente}', 'OrcamentosController@post')->name('orcamentos.post');
    Route::get('marcas', 'MarcasController@index')->name('marcas');
    Route::get('empresa', 'EmpresaController@index')->name('empresa');
    Route::get('novidades', 'NovidadesController@index')->name('novidades');
    Route::get('novidades/{novidade}', 'NovidadesController@show')->name('novidades.show');
    Route::get('contato', 'ContatoController@index')->name('contato');
    Route::post('contato', 'ContatoController@post')->name('contato.post');
    Route::post('contato-footer', 'ContatoController@postFooter')->name('contato-footer.post');
    Route::get('politica-de-privacidade', 'PoliticaDePrivacidadeController@index')->name('politica-de-privacidade');
    Route::post('busca', 'BuscaController@buscar')->name('busca'); //ok
    Route::get('busca/{linha}/categorias/{letra}', 'BuscaController@getCategorias')->name('getCategorias'); //ok
    Route::post('aceite-de-cookies', 'HomeController@postCookies')->name('aceite-de-cookies.post');

    // Auth Clientes
    Route::group([
        'prefix'    => 'cliente',
        'namespace' => 'Cliente'
    ], function () {
        Route::post('login', 'AuthController@login')->name('cliente.login');
        Route::get('logout', 'AuthController@logout')->name('cliente.logout');
        Route::post('cadastro', 'AuthController@create')->name('cliente.cadastro');
    });

    // Painel
    Route::group([
        'prefix'     => 'painel',
        'namespace'  => 'Painel',
        'middleware' => ['auth']
    ], function () {
        Route::get('/', 'PainelController@index')->name('painel');
        Route::post('ckeditor-upload', 'PainelController@imageUpload');
        Route::post('order', 'PainelController@order');
        Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

        Route::resource('home-banners', 'HomeBannersController');
        Route::resource('home-destaques', 'HomeDestaquesController', ['only' => ['index', 'update']]);
        Route::resource('home-diferenciais', 'HomeDiferenciaisController');
        Route::resource('marcas', 'MarcasController');
        Route::get('linhas', 'LinhasController@index')->name('painel.linhas.index');
        Route::resource('linhas.categorias', 'CategoriasController');
        Route::resource('produtos', 'ProdutosController');
        Route::get('produtos/linhas/{linha}/categorias', 'ProdutosController@getCategorias')->name('painel.produtos.linhas.categorias');
        Route::get('produtos/{produto}/desenho/delete', 'ProdutosController@deleteDesenho')->name('painel.produtos.desenho.delete');
        Route::get('produtos/{produto}/manual/delete', 'ProdutosController@deleteManual')->name('painel.produtos.manual.delete');
        Route::get('produtos/{produto}/variacoes/delete', 'ProdutosController@deleteImagem')->name('painel.produtos.variacoes.delete');
        Route::resource('clientes', 'ClientesController', ['only' => ['index', 'destroy']]);
        Route::get('clientes/export', 'ClientesController@export')->name('painel.clientes.export');
        Route::resource('empresa', 'EmpresaController', ['only' => ['index', 'update']]);
        Route::get('empresa/imagens/clear', [
            'as'   => 'painel.empresa.imagens.clear',
            'uses' => 'EmpresaImagensController@clear'
        ]);
        Route::resource('empresa/imagens', 'EmpresaImagensController', ['parameters' => ['imagens' => 'empresa_imagens']]);
        Route::resource('empresa/diferenciais', 'EmpresaDiferenciaisController');
        Route::resource('categorias-novidades', 'CategoriasNovidadesController');
        Route::resource('novidades', 'NovidadesController');
        Route::get('orcamentos/{orcamento}/toggle', ['as' => 'painel.orcamentos.toggle', 'uses' => 'OrcamentosController@toggle']);
        Route::resource('orcamentos', 'OrcamentosController');
        Route::resource('orcamentos-contatos', 'ContatosOrcamentosController');

        Route::get('newsletter', 'NewsletterController@index')->name('painel.newsletter.index');
        Route::get('newsletter/export', 'NewsletterController@export')->name('painel.newsletter.export');
        Route::get('contato/recebidos/{recebidos}/toggle', ['as' => 'painel.contato.recebidos.toggle', 'uses' => 'ContatosRecebidosController@toggle']);
        Route::resource('contato/recebidos', 'ContatosRecebidosController');
        Route::resource('contato', 'ContatoController');
        Route::get('aceite-de-cookies', 'AceiteDeCookiesController@index')->name('painel.aceite-de-cookies');
        Route::resource('politica-de-privacidade', 'PoliticaDePrivacidadeController', ['only' => ['index', 'update']]);
        Route::resource('configuracoes', 'ConfiguracoesController', ['only' => ['index', 'update']]);
        Route::resource('usuarios', 'UsuariosController');

        // Limpar caches
        Route::get('clear-cache', function () {
            $exitCode = Artisan::call('config:cache');
            $exitCode = Artisan::call('config:clear');
            $exitCode = Artisan::call('cache:clear');
            $exitCode = Artisan::call('route:clear');
            $exitCode = Artisan::call('view:clear');
            $exitCode = Artisan::call('clear-compiled');

            return 'DONE';
        });
    });

    // Auth
    Route::group([
        'prefix'    => 'painel',
        'namespace' => 'Auth'
    ], function () {
        Route::get('login', 'AuthController@showLoginForm')->name('auth');
        Route::post('login', 'AuthController@login')->name('login');
        Route::get('logout', 'AuthController@logout')->name('logout');
        Route::get('esqueci-minha-senha', 'PasswordController@sendEmail')->name('esqueci-minha-senha');
        Route::post('esqueci-minha-senha', 'PasswordController@sendResetLinkEmail')->name('esqueci-minha-senha.post');
        Route::get('reset/{token}', 'PasswordController@showResetForm')->name('password-reset');
        Route::post('reset', 'PasswordController@reset')->name('password-reset.post');
    });
});
